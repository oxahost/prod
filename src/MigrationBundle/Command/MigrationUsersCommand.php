<?php

namespace MigrationBundle\Command;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrationUsersCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'oro:migration:users';
    protected function configure()
    {
        $this
        ->setDescription('Migrate users from prestashop to orocommerce ')

        ->setHelp('This command allows you migrate from prestashop to orocommerce')
    ;
    }

    public function GenerateToken()
    {
        $token = '{
            "grant_type": "client_credentials",
            "client_id": "5YreI4Jfdq3_Gmkj1msKIi1QhN4fshzS",
            "client_secret": "8eFmLWHP1VUTqxWwkKLp-v1Zi0pJBz8-50ooOJGRACti1_xpXFH4PvXta5HYgJnV5Rd-MODWZQXYVut10_ADcz"
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://vtest.orotunisie.com/oauth2-token",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>$token,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Cookie: _csrf=SPEOl__eYbbevocafx09KWBs9pLHG1PhwNyCPofv500; PrestaShop-f4c2ae80a1b773da54f57fa40c14a3a3=def50200716ad4ea8b369642e4456852174e261ccefc82e3c4a18ab87d97c39beae7e81f753f5e8da5d79774e1d31b1cd3783de50bc65a29f03e7e9c6d55e82e6211c261999672a87c376ab000b44e9f5cabf7f874d90747fad34cd826e7985124fe39ade599100db3ae76b433c0ace615b97268b7fb79148ecce4163f086b006be32767a26f29ac9e88fa121cd8c650d3688dcf49bf7d0419642ff112945d38c6d63b; PHPSESSID=0heckros7o73p95i64d09d8kd9; PrestaShop-9c54a5fdea027e64c55fca395f76a2f4=def50200f5d6f6d9e19a9e02d181d29902feed01ed7ea0948fe3e361594b5578a2f553e5296f524fde0fda035df7f213ef414247785cdbc477a0331f10b8dc24f07c139c4980742c0c5a060eccf63b1d9b06921d6d762e6fb8ebdff342f9cdd75e8dfd140eb1ed"
          ),
        ));
        
        $orotoken = curl_exec($curl);
        curl_close($curl);
  
        $orotoken = json_decode($orotoken,true);
        $orotoken = $orotoken['access_token'];
        return $orotoken;
    }


    public function GetCustumerUsers(){

    $urlapi = "https://I4P1V1DR4MUFYFFEICU8HCIDUZ1Y6WQX@www.wamia.tn/api/";
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $urlapi."customers&display=full&output_format=JSON?filter[id]=[10000,20000]",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cookie: _csrf=SPEOl__eYbbevocafx09KWBs9pLHG1PhwNyCPofv500; PrestaShop-f4c2ae80a1b773da54f57fa40c14a3a3=def50200716ad4ea8b369642e4456852174e261ccefc82e3c4a18ab87d97c39beae7e81f753f5e8da5d79774e1d31b1cd3783de50bc65a29f03e7e9c6d55e82e6211c261999672a87c376ab000b44e9f5cabf7f874d90747fad34cd826e7985124fe39ade599100db3ae76b433c0ace615b97268b7fb79148ecce4163f086b006be32767a26f29ac9e88fa121cd8c650d3688dcf49bf7d0419642ff112945d38c6d63b; PHPSESSID=0heckros7o73p95i64d09d8kd9; PrestaShop-9c54a5fdea027e64c55fca395f76a2f4=def50200f5d6f6d9e19a9e02d181d29902feed01ed7ea0948fe3e361594b5578a2f553e5296f524fde0fda035df7f213ef414247785cdbc477a0331f10b8dc24f07c139c4980742c0c5a060eccf63b1d9b06921d6d762e6fb8ebdff342f9cdd75e8dfd140eb1ed"
      ),
    ));
    
    $users = curl_exec($curl);
    curl_close($curl);

    $users = json_decode($users,true);
    $users = $users['customers'];
    return $users;
    }


public function PostUsers($output, $token,$users){

    $em    = $this->getContainer()->get('doctrine')->getManager('default');
    foreach($users as $key=>$value ){
    $email = $value['email'];

    $firstname = $value['firstname'];
    $lastname = $value['lastname'];
    $enable = $value['active'];
    $password = $value['passwd'];
    $idgroup = $value['id_default_group'];

    $queryemail = "SELECT email FROM oro_customer_user where email ='$email' ";
    $statement = $em->getConnection()->prepare($queryemail);    
    $queryemail = $statement->execute();
    $queryemail = $statement->fetchAll();
    if(count($queryemail) == 0){

        if($enable== 1){

            $enable = true;
          }
          else {
            $enable = false;
          }
      
          $postusers = '{
            "data": {
              "type": "customerusers",
              "attributes": {
                "confirmed": true,
                "email": "'.$email.'",
                "firstName": "'.$firstname.'",
                "lastName": "'.$lastname.'",
                "enabled": "'.$enable.'",
                "password": "'.$password.'"
              },
              "relationships": {
                "roles": {
                  "data": [{
                    "type": "customeruserroles",
                    "id": "2"
                  }]
                },
                "customer": {
                  "data": {
                    "type": "customers",
                    "id": "customers_'.$firstname.'_'.$lastname.'"
                  }
                }
              }
            },
            "included": [{
              "type": "customers",
              "id": "customers_'.$firstname.'_'.$lastname.'",
              "attributes": {
                "name": "'.$firstname.' '.$lastname.'"
              },
              "relationships": {
                "children": {
                  "data": []
                },
                "group": {
                  "data": []
                },
                "users": {
                  "data": []
                }
              }
            }]
          
          }' ;
      
      
             /* $postarray[] = $post;*/
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://vtest.orotunisie.com/admin/api/customerusers",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $postusers,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/vnd.api+json",
            "Authorization: Bearer $token",
            "Cookie: _csrf=SPEOl__eYbbevocafx09KWBs9pLHG1PhwNyCPofv500; PrestaShop-f4c2ae80a1b773da54f57fa40c14a3a3=def50200716ad4ea8b369642e4456852174e261ccefc82e3c4a18ab87d97c39beae7e81f753f5e8da5d79774e1d31b1cd3783de50bc65a29f03e7e9c6d55e82e6211c261999672a87c376ab000b44e9f5cabf7f874d90747fad34cd826e7985124fe39ade599100db3ae76b433c0ace615b97268b7fb79148ecce4163f086b006be32767a26f29ac9e88fa121cd8c650d3688dcf49bf7d0419642ff112945d38c6d63b; PHPSESSID=0heckros7o73p95i64d09d8kd9; PrestaShop-9c54a5fdea027e64c55fca395f76a2f4=def50200f5d6f6d9e19a9e02d181d29902feed01ed7ea0948fe3e361594b5578a2f553e5296f524fde0fda035df7f213ef414247785cdbc477a0331f10b8dc24f07c139c4980742c0c5a060eccf63b1d9b06921d6d762e6fb8ebdff342f9cdd75e8dfd140eb1ed"
          ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
    }


 }

return $response;
}

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $token = $this->GenerateToken();
        $custumeruser = $this->GetCustumerUsers();
        $postcustomers = $this->PostUsers($output,$token,$custumeruser);
        $output->writeln('migration reussie');
    }
}