<?php

namespace MigrationBundle\Command;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrationCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'oro:migration:prestashop';
    private $container;

    protected function configure()
    {
        $this
        ->setDescription('Migrate data from prestashop to orocommerce ')

        ->setHelp('This command allows you migrate from prestashop to orocommerce')
    ;
    }

    public function GenerateToken()
    {
        $token = '{
            "grant_type": "client_credentials",
            "client_id": "5YreI4Jfdq3_Gmkj1msKIi1QhN4fshzS",
            "client_secret": "8eFmLWHP1VUTqxWwkKLp-v1Zi0pJBz8-50ooOJGRACti1_xpXFH4PvXta5HYgJnV5Rd-MODWZQXYVut10_ADcz"
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://vtest.orotunisie.com/oauth2-token",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>$token,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Cookie: _csrf=SPEOl__eYbbevocafx09KWBs9pLHG1PhwNyCPofv500; PrestaShop-f4c2ae80a1b773da54f57fa40c14a3a3=def50200716ad4ea8b369642e4456852174e261ccefc82e3c4a18ab87d97c39beae7e81f753f5e8da5d79774e1d31b1cd3783de50bc65a29f03e7e9c6d55e82e6211c261999672a87c376ab000b44e9f5cabf7f874d90747fad34cd826e7985124fe39ade599100db3ae76b433c0ace615b97268b7fb79148ecce4163f086b006be32767a26f29ac9e88fa121cd8c650d3688dcf49bf7d0419642ff112945d38c6d63b; PHPSESSID=0heckros7o73p95i64d09d8kd9; PrestaShop-9c54a5fdea027e64c55fca395f76a2f4=def50200f5d6f6d9e19a9e02d181d29902feed01ed7ea0948fe3e361594b5578a2f553e5296f524fde0fda035df7f213ef414247785cdbc477a0331f10b8dc24f07c139c4980742c0c5a060eccf63b1d9b06921d6d762e6fb8ebdff342f9cdd75e8dfd140eb1ed"
          ),
        ));
        
        $orotoken = curl_exec($curl);
        curl_close($curl);
  
        $orotoken = json_decode($orotoken,true);
        $orotoken = $orotoken['access_token'];
        return $orotoken;
    }


    public function GetCategories(){

    $urlapi = "https://I4P1V1DR4MUFYFFEICU8HCIDUZ1Y6WQX@www.wamia.tn/api/";
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $urlapi."categories&display=full&output_format=JSON?sort=[level_depth_ASC]&filter[id]=>[11]",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cookie: _csrf=SPEOl__eYbbevocafx09KWBs9pLHG1PhwNyCPofv500; PrestaShop-f4c2ae80a1b773da54f57fa40c14a3a3=def50200716ad4ea8b369642e4456852174e261ccefc82e3c4a18ab87d97c39beae7e81f753f5e8da5d79774e1d31b1cd3783de50bc65a29f03e7e9c6d55e82e6211c261999672a87c376ab000b44e9f5cabf7f874d90747fad34cd826e7985124fe39ade599100db3ae76b433c0ace615b97268b7fb79148ecce4163f086b006be32767a26f29ac9e88fa121cd8c650d3688dcf49bf7d0419642ff112945d38c6d63b; PHPSESSID=0heckros7o73p95i64d09d8kd9; PrestaShop-9c54a5fdea027e64c55fca395f76a2f4=def50200f5d6f6d9e19a9e02d181d29902feed01ed7ea0948fe3e361594b5578a2f553e5296f524fde0fda035df7f213ef414247785cdbc477a0331f10b8dc24f07c139c4980742c0c5a060eccf63b1d9b06921d6d762e6fb8ebdff342f9cdd75e8dfd140eb1ed"
      ),
    ));
    
    $prestashopres = curl_exec($curl);
    curl_close($curl);

    $prestashopres = json_decode($prestashopres,true);
    $prestashopres = $prestashopres['categories'];

    return $prestashopres;
    }

public function PostCategories($output, $token,$prestacategories){

    function file_get_contents_curl($url) { 
        $ch = curl_init(); 
      
        curl_setopt($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_URL, $url); 
      
        $data = curl_exec($ch); 
        curl_close($ch); 
      
        return $data; 
    } 


    foreach($prestacategories as $key=>$value ){

    $urlapi = "https://I4P1V1DR4MUFYFFEICU8HCIDUZ1Y6WQX@www.wamia.tn/api/";
    $em    = $this->getContainer()->get('doctrine')->getManager('default');

    $name = $value['name']['0']['value'];
    $dateadd = $value['date_add'];
    $dateadd =  date("Y-m-d\TH:i:s.000\Z", strtotime($dateadd));
    $idparent = $value['id_parent'];
    $dateupdate = $value['date_upd'];
    $dateupdate =  date("Y-m-d\TH:i:s.000\Z", strtotime($dateupdate));
    $description = $value['description']['0']['value'];
    if(strlen($description) > 0){
        $description= str_replace('"',"'",$description);
        $description = str_replace( array("\n", "\r"), array( '', ''), $description );        }
      else{

        $description = null;
      }
    $level = $value['level_depth'];
    $root = $value['is_root_category'];
    $id = $value['id'];



   /* $image =  $urlapi.'images/categories/'.$id;
    $image = file_get_contents_curl($image);
    $image = base64_encode($image);*/
    $description = str_replace("\r",'', $description);



    $querycatalogue = 'SELECT id_presta FROM oro_synchron where id_presta ='.$id.'';
    $statement = $em->getConnection()->prepare($querycatalogue);    
    $querycatalogue = $statement->execute();
    $querycatalogue = $statement->fetchAll();
    if(count($querycatalogue) == 0){

        $RAW_QUERY=  'INSERT INTO oro_synchron (id_presta,id_oro,id_parent_presta,id_parent_oro) VALUES ('.$id.',NULL,'.$idparent.',NULL)';
        $statement = $em->getConnection()->prepare($RAW_QUERY);    
        $statement->execute();
    
        if ($level == 2){
            $post = '{
                "data": {
                    "type": "categories",
                    "id": "'.$id.'",
                    "attributes": {
                        "availability_date": null,
                        "level": "'.$level.'",
                        "root": "'.$root.'",
                        "createdAt": "'.$dateadd.'",
                        "updatedAt": "'.$dateupdate.'"
                    },
                    "relationships": {
                        "titles": {
                            "data": [
                                {
                                    "type": "localizedfallbackvalues",
                                    "id": "'.$name.'"
                                }
                            ]
                        },
                        "parentCategory": {
                            "data": {
                                "type": "categories",
                                "id": "1"
                            }
                        },
                        "childCategories": {
                            "data": null
                        },
                        "shortDescriptions": {
                            "data": [
                                {
                                "type": "localizedfallbackvalues",
                                "id": "description_'.$id.'"
                            }
                         ]
                        },
                        "longDescriptions": {
                            "data": []
                        },
                        "slugPrototypes": {
                            "data": []
                        },
                        "organization": {
                            "data": {
                                "type": "organizations",
                                "id": "1"
                            }
                        },
                        "products": {
                            "data": null
                        },
                        "metaTitles": {
                            "data": []
                        },
                        "largeImage": {
                            "data": []
                        },
                        "smallImage": {
                            "data": []
                        }
                    }
                },
              "included": [
                        {
                            "type": "localizedfallbackvalues",
                            "id": "'.$name.'",
                        "attributes": {
                            "wysiwyg": null,
                            "wysiwyg_style": null,
                            "wysiwyg_properties": null,
                            "fallback": null,
                            "string": "'.$name.'",
                            "text": null
                        },
                        "relationships": {
                            "localization": {
                                "data": null
                            }
                        }
                        },
                        {
                            "type": "localizedfallbackvalues",
                            "id": "description_'.$id.'",
                        "attributes": {
                            "wysiwyg": null,
                            "wysiwyg_style": null,
                            "wysiwyg_properties": null,
                            "fallback": null,
                            "string": null,
                            "text": "'.$description.'"
                        },
                        "relationships": {
                            "localization": {
                                "data": null
                            }
                        }
                    }
              ]
            }';
       /* $postarray[] = $post;*/
       $curl = curl_init();
       curl_setopt_array($curl, array(
         CURLOPT_URL => "http://vtest.orotunisie.com/admin/api/categories",
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_ENCODING => "",
         CURLOPT_MAXREDIRS => 10,
         CURLOPT_TIMEOUT => 0,
         CURLOPT_FOLLOWLOCATION => true,
         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
         CURLOPT_CUSTOMREQUEST => "POST",
         CURLOPT_POSTFIELDS => $post,
         CURLOPT_HTTPHEADER => array(
           "Content-Type: application/vnd.api+json",
           "Authorization: Bearer $token",
           "Cookie: _csrf=SPEOl__eYbbevocafx09KWBs9pLHG1PhwNyCPofv500; PrestaShop-f4c2ae80a1b773da54f57fa40c14a3a3=def50200716ad4ea8b369642e4456852174e261ccefc82e3c4a18ab87d97c39beae7e81f753f5e8da5d79774e1d31b1cd3783de50bc65a29f03e7e9c6d55e82e6211c261999672a87c376ab000b44e9f5cabf7f874d90747fad34cd826e7985124fe39ade599100db3ae76b433c0ace615b97268b7fb79148ecce4163f086b006be32767a26f29ac9e88fa121cd8c650d3688dcf49bf7d0419642ff112945d38c6d63b; PHPSESSID=0heckros7o73p95i64d09d8kd9; PrestaShop-9c54a5fdea027e64c55fca395f76a2f4=def50200f5d6f6d9e19a9e02d181d29902feed01ed7ea0948fe3e361594b5578a2f553e5296f524fde0fda035df7f213ef414247785cdbc477a0331f10b8dc24f07c139c4980742c0c5a060eccf63b1d9b06921d6d762e6fb8ebdff342f9cdd75e8dfd140eb1ed"
         ),
       ));
       
       $response = curl_exec($curl);
       curl_close($curl);
    
       $response = json_decode($response,true);
       $response = $response['data'];
       $idresponse =$response['id'];
       $RAW_UPDATE= 'UPDATE oro_synchron
       SET id_oro='.$idresponse.', id_parent_oro=1
       WHERE id_presta='.$id.' ';
       $statement = $em->getConnection()->prepare($RAW_UPDATE);    
       $statement->execute();
        }
        else{
            $idparentoro = 'SELECT id_oro FROM oro_synchron where id_presta ='.$idparent.'';
            $statement = $em->getConnection()->prepare($idparentoro);    
            $idparentoro = $statement->execute();
            $idparentoro = $statement->fetchAll();
            $idparentoro = $idparentoro[0]['id_oro'];
    
            $post1 = '{
                "data": {
                    "type": "categories",
                    "id": "'.$id.'",
                    "attributes": {
                        "availability_date": null,
                        "level": "'.$level.'",
                        "root": "'.$root.'",
                        "createdAt": "'.$dateadd.'",
                        "updatedAt": "'.$dateupdate.'"
                    },
                    "relationships": {
                        "titles": {
                            "data": [
                                {
                                    "type": "localizedfallbackvalues",
                                    "id": "'.$name.'"
                                }
                            ]
                        },
                        "parentCategory": {
                            "data": {
                                "type": "categories",
                                "id": "'.$idparentoro.'"
                            }
                        },
                        "childCategories": {
                            "data": null
                        },
                        "shortDescriptions": {
                            "data": [
                                {
                                "type": "localizedfallbackvalues",
                                "id": "description_'.$id.'"
                            }
                         ]
                        },
                        "longDescriptions": {
                            "data": []
                        },
                        "slugPrototypes": {
                            "data": []
                        },
                        "organization": {
                            "data": {
                                "type": "organizations",
                                "id": "1"
                            }
                        },
                        "products": {
                            "data": null
                        },
                        "metaTitles": {
                            "data": []
                        },
                        "largeImage": {
                            "data": []
                        },
                        "smallImage": {
                            "data": []
                        }
                    }
                },
              "included": [
                        {
                            "type": "localizedfallbackvalues",
                            "id": "'.$name.'",
                        "attributes": {
                            "wysiwyg": null,
                            "wysiwyg_style": null,
                            "wysiwyg_properties": null,
                            "fallback": null,
                            "string": "'.$name.'",
                            "text": null
                        },
                        "relationships": {
                            "localization": {
                                "data": null
                            }
                        }
                        },
                        {
                            "type": "localizedfallbackvalues",
                            "id": "description_'.$id.'",
                        "attributes": {
                            "wysiwyg": null,
                            "wysiwyg_style": null,
                            "wysiwyg_properties": null,
                            "fallback": null,
                            "string": null,
                            "text": "new description"
                        },
                        "relationships": {
                            "localization": {
                                "data": null
                            }
                        } 
                    } 
              ]
            }';
    
       /* $postarray[] = $post;*/
      $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://vtest.orotunisie.com/admin/api/categories",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $post1,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/vnd.api+json",
            "Authorization: Bearer $token",
            "Cookie: _csrf=SPEOl__eYbbevocafx09KWBs9pLHG1PhwNyCPofv500; PrestaShop-f4c2ae80a1b773da54f57fa40c14a3a3=def50200716ad4ea8b369642e4456852174e261ccefc82e3c4a18ab87d97c39beae7e81f753f5e8da5d79774e1d31b1cd3783de50bc65a29f03e7e9c6d55e82e6211c261999672a87c376ab000b44e9f5cabf7f874d90747fad34cd826e7985124fe39ade599100db3ae76b433c0ace615b97268b7fb79148ecce4163f086b006be32767a26f29ac9e88fa121cd8c650d3688dcf49bf7d0419642ff112945d38c6d63b; PHPSESSID=0heckros7o73p95i64d09d8kd9; PrestaShop-9c54a5fdea027e64c55fca395f76a2f4=def50200f5d6f6d9e19a9e02d181d29902feed01ed7ea0948fe3e361594b5578a2f553e5296f524fde0fda035df7f213ef414247785cdbc477a0331f10b8dc24f07c139c4980742c0c5a060eccf63b1d9b06921d6d762e6fb8ebdff342f9cdd75e8dfd140eb1ed"
          ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
    
        $response = json_decode($response,true);
        $response = $response['data'];
    
        $idresponse =$response['id'];
    
        $RAW_UPDATE= 'UPDATE oro_synchron
        SET id_oro='.$idresponse.', id_parent_oro='.$idparentoro.' WHERE id_presta='.$id.' ';
    
     $statement = $em->getConnection()->prepare($RAW_UPDATE);    
     $statement->execute();
     }


    }

}
return $response;
}


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $token = $this->GenerateToken();
        $categories = $this->GetCategories();
        $postcategories = $this->PostCategories($output,$token,$categories);
        $output->writeln('migration reussie');
    }
}