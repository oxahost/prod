<?php

namespace Ibnab\Bundle\PlusMinusBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ibnab_plus_minus');
        SettingsBuilder::append(
            $rootNode,
            array(
                'enable'   => ['value' => 1]
            )
        );
        return $treeBuilder;
    }

    
}
