<?php

namespace Ibnab\Bundle\ImanBundle\Migrations\Data\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gaufrette\Adapter\Local;
use Gaufrette\Filesystem;
use Oro\Bundle\AttachmentBundle\Entity\File as AttachmentFile;
use Ibnab\Bundle\SliderBundle\ContentWidget\SliderContentWidgetType;
use Oro\Bundle\CMSBundle\Entity\ContentBlock;
use Oro\Bundle\CMSBundle\Entity\ContentWidget;
use Oro\Bundle\CMSBundle\Entity\ImageSlide;
use Oro\Bundle\CMSBundle\Entity\TextContentVariant;
use Oro\Bundle\DigitalAssetBundle\Entity\DigitalAsset;
use Oro\Bundle\LocaleBundle\Entity\LocalizedFallbackValue;
use Oro\Bundle\UserBundle\DataFixtures\UserUtilityTrait;
use Oro\Bundle\UserBundle\Entity\User;
use Oro\Bundle\UserBundle\Migrations\Data\ORM\LoadAdminUserData;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Ibnab\Bundle\SliderBundle\Entity\Slider;
use Ibnab\Bundle\SliderBundle\Entity\Banner;

/**
 * Adds content block with Image Slider content widget in it.
 */
class LoadWidgetwithBlockContent extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;
    use UserUtilityTrait;

    /** @var string */
    private const HOME_PAGE_SLIDER_ALIAS_IMAN = 'home-page-slider-iman';
    private const HOME_PAGE_SLIDER_ALIAS_IMAN_MODERN = 'home-page-slider-iman-modern';
    private const HOME_PAGE_TOP_BANNER1_ALIAS_IMAN_MODERN = 'home-page-top-banner1-iman-modern';
    private const HOME_PAGE_TOP_BANNER2_ALIAS_IMAN_MODERN = 'home-page-top-banner2-iman-modern';
    
    
    private const HOME_PAGE_SLIDER_ALIAS = 'slider-home';
    private const HOME_PAGE_SLIDER_ALIAS_SECOND = 'slider-home-second';
    private const HOME_PAGE_SLIDER_ALIAS_MODERN_TOP_BANNER1 = 'banner-top1-modernA';
    private const HOME_PAGE_SLIDER_ALIAS_MODERN_TOP_BANNER2 = 'banner-top2-modernA';
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadAdminUserData::class
        ];
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = $this->getFirstUser($manager);
        $slider = $manager->getRepository(Slider::class)
            ->findOneBy(['title' => self::HOME_PAGE_SLIDER_ALIAS]);

        $widget = new ContentWidget();
        $widget->setWidgetType(SliderContentWidgetType::getName());
        $widget->setName(self::HOME_PAGE_SLIDER_ALIAS_IMAN);
        $widget->setOrganization($user->getOrganization());
        if($slider){
        $widget->setSettings(
            [
                'slider' => $slider,
                'banner' => null,
                'isBanner' => false
            ]
        );
        }
        $manager->persist($widget);
        $manager->flush();
        $this->updateOrCreateContentBlock(
            $manager,
            $user,
            '<div data-title="home-page-slider" data-type="ibnab_widget_easy_slider" class="content-widget content-placeholder">
                {{ widget("home-page-slider-iman") }}
            </div>',
            self::HOME_PAGE_SLIDER_ALIAS_IMAN
        );
        $slider1 = $manager->getRepository(Slider::class)
            ->findOneBy(['title' => self::HOME_PAGE_SLIDER_ALIAS_SECOND]);
        $widget = new ContentWidget();
        $widget->setWidgetType(SliderContentWidgetType::getName());
        $widget->setName(self::HOME_PAGE_SLIDER_ALIAS_IMAN_MODERN);
        $widget->setOrganization($user->getOrganization());
        if($slider1){
        $widget->setSettings(
            [
                'slider' => $slider1,
                'banner' => null,
                'isBanner' => false
            ]
        );
        }
        $manager->persist($widget);
        
        
        $banner1 = $manager->getRepository(Banner::class)
            ->findOneBy(['title' => self::HOME_PAGE_SLIDER_ALIAS_MODERN_TOP_BANNER1]);
        $widget2 = new ContentWidget();
        $widget2->setWidgetType(SliderContentWidgetType::getName());
        $widget2->setName(self::HOME_PAGE_TOP_BANNER1_ALIAS_IMAN_MODERN);
        $widget2->setOrganization($user->getOrganization());
        if($banner1){
        $widget2->setSettings(
            [
                'slider' => null,
                'banner' => $banner1,
                'isBanner' => true
            ]
        );
        }
        $manager->persist($widget2);
        
        $banner2 = $manager->getRepository(Banner::class)
            ->findOneBy(['title' => self::HOME_PAGE_SLIDER_ALIAS_MODERN_TOP_BANNER2]);
        $widget3 = new ContentWidget();
        $widget3->setWidgetType(SliderContentWidgetType::getName());
        $widget3->setName(self::HOME_PAGE_TOP_BANNER2_ALIAS_IMAN_MODERN);
        $widget3->setOrganization($user->getOrganization());
        if($banner2){
        $widget3->setSettings(
            [
                'slider' => null,
                'banner' => $banner2,
                'isBanner' => true
            ]
        );
        }
        $manager->persist($widget3);
        
        
        $manager->flush();
        $html = file_get_contents(__DIR__.'/data/iman-modern-slider-adsbanner.html');
        $this->updateOrCreateContentBlock(
            $manager,
            $user,
            $html,
            self::HOME_PAGE_SLIDER_ALIAS_IMAN_MODERN
        );
    }

    /**
     * @param ObjectManager $manager
     * @param User $user
     * @param string $content
     * @return ContentBlock|null
     */
    private function updateOrCreateContentBlock(ObjectManager $manager, User $user, string $content,string $alias): void
    {
        $contentBlock = $manager->getRepository(ContentBlock::class)
            ->findOneBy(['alias' => $alias]);

        //if (!$contentBlock instanceof ContentBlock) {
            $title = new LocalizedFallbackValue();
            $title->setString($alias);
            $manager->persist($title);

            $variant = new TextContentVariant();
            $variant->setDefault(true);
            $variant->setContent($content);
            $manager->persist($variant);

            $contentBlock = new ContentBlock();
            $contentBlock->setOrganization($user->getOrganization());
            $contentBlock->setOwner($user->getOwner());
            $contentBlock->setAlias($alias);
            $contentBlock->addTitle($title);
            $contentBlock->addContentVariant($variant);
            $manager->persist($contentBlock);
       /* } else {
            $html = file_get_contents(__DIR__ . '/data/frontpage_slider.html');

            foreach ($contentBlock->getContentVariants() as $contentVariant) {
                if ($contentVariant->getContent() === $html) {
                    $contentVariant->setContent($content);
                    break;
                }
            }
        }*/

        $manager->flush();
    }

}
