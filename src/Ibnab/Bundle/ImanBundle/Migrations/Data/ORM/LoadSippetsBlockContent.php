<?php

namespace Ibnab\Bundle\ImanBundle\Migrations\Data\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gaufrette\Adapter\Local;
use Gaufrette\Filesystem;
use Oro\Bundle\AttachmentBundle\Entity\File as AttachmentFile;
use Ibnab\Bundle\SliderBundle\ContentWidget\SliderContentWidgetType;
use Oro\Bundle\CMSBundle\Entity\ContentBlock;
use Oro\Bundle\CMSBundle\Entity\ContentWidget;
use Oro\Bundle\CMSBundle\Entity\ImageSlide;
use Oro\Bundle\CMSBundle\Entity\TextContentVariant;
use Oro\Bundle\DigitalAssetBundle\Entity\DigitalAsset;
use Oro\Bundle\LocaleBundle\Entity\LocalizedFallbackValue;
use Oro\Bundle\UserBundle\DataFixtures\UserUtilityTrait;
use Oro\Bundle\UserBundle\Entity\User;
use Oro\Bundle\UserBundle\Migrations\Data\ORM\LoadAdminUserData;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Ibnab\Bundle\SliderBundle\Entity\Banner;

/**
 * Load Html Snippets.
 */
class LoadSippetsBlockContent extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;
    use UserUtilityTrait;

    /** @var string */
    private const OUR_SERVICES_ALIAS_IMAN = 'iman-our-services';
    private const HOME_BANNER_ADS2_ALIAS_IMAN = 'home-banners-ads2';
    private const SOCIAL_MEDIA_BOTTOM = 'social-media-bottom';

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadAdminUserData::class
        ];
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = $this->getFirstUser($manager);
        
        
        $banners = ['banner-home-ads1' => 'banner1','banner-home-ads2' => '400-ads2.png', 'banner-home-ads3' => '400-ads3.png'];
        foreach($banners as $key => $banner){
        $$banner = $manager->getRepository(Banner::class)
            ->findOneBy(['title' => $key]);
        $$key = new ContentWidget();
        $$key->setWidgetType(SliderContentWidgetType::getName());
        $$key->setName($key);
        $$key->setOrganization($user->getOrganization());
        if($$banner){
        $$key->setSettings(
            [
                'slider' => null,
                'banner' => $$banner,
                'isBanner' => true
            ]
        );
        }
        $manager->persist($$key);
        }
        $manager->flush();
        $html = file_get_contents(__DIR__.'/data/iman-collection-adsbanner1.html');
        $this->updateOrCreateContentBlock(
            $manager,
            $user,
            $html,
            self::HOME_BANNER_ADS2_ALIAS_IMAN
        );        
        
        $html = file_get_contents(__DIR__.'/data/iman-our-services.html');
        $this->updateOrCreateContentBlock(
            $manager,
            $user,
            $html,
            self::OUR_SERVICES_ALIAS_IMAN
        );
        $html = file_get_contents(__DIR__.'/data/social-media-bottom.html');
        $this->updateOrCreateContentBlock(
            $manager,
            $user,
            $html,
            self::SOCIAL_MEDIA_BOTTOM
        );
    }

    /**
     * @param ObjectManager $manager
     * @param User $user
     * @param string $content
     * @return ContentBlock|null
     */
    private function updateOrCreateContentBlock(ObjectManager $manager, User $user, string $content,string $alias): void
    {
        $contentBlock = $manager->getRepository(ContentBlock::class)
            ->findOneBy(['alias' => $alias]);

        //if (!$contentBlock instanceof ContentBlock) {
            $title = new LocalizedFallbackValue();
            $title->setString($alias);
            $manager->persist($title);

            $variant = new TextContentVariant();
            $variant->setDefault(true);
            $variant->setContent($content);
            $manager->persist($variant);

            $contentBlock = new ContentBlock();
            $contentBlock->setOrganization($user->getOrganization());
            $contentBlock->setOwner($user->getOwner());
            $contentBlock->setAlias($alias);
            $contentBlock->addTitle($title);
            $contentBlock->addContentVariant($variant);
            $manager->persist($contentBlock);
            $manager->flush();
    }

}
