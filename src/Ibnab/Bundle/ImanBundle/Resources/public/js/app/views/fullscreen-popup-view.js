define(function(require) {
    'use strict';

    var FullscreenPopupView;
    var template = require('tpl-loader!ibnabiman/templates/fullscreen-popup/fullscreen-popup.html');
    var footerTemplate = require('tpl-loader!orofrontend/templates/fullscreen-popup/fullscreen-popup-footer.html');
    var headerTemplate = require('tpl-loader!orofrontend/templates/fullscreen-popup/fullscreen-popup-header.html');
    var BaseView = require('oroui/js/app/views/base/view');
    var tools = require('oroui/js/tools');
    var mediator = require('oroui/js/mediator');
    var scrollHelper = require('oroui/js/tools/scroll-helper');
    var _ = require('underscore');
    var $ = require('jquery');

    FullscreenPopupView = BaseView.extend({
        /**
         * @property
         */
        keepElement: true,

        /**
         * @property
         */
        optionNames: BaseView.prototype.optionNames.concat([
            'template', 'templateSelector', 'templateData',
            'popupLabel', 'popupCloseOnLabel',
            'popupCloseButton', 'popupIcon', 'popupBadge',
            'stopEventsPropagation', 'stopEventsList'
        ]),

        sections: ['header', 'content', 'footer'],

        sectionOptionVariants: ['', 'Selector', 'View', 'Element', 'Template'],

        header: {
            Template: headerTemplate,
            options: {
                templateData: {}
            }
        },

        footer: {
            Template: footerTemplate
        },

        /**
         * @property
         */
        template: template,

        /**
         * @property
         */
        popupLabel: _.__('Back'),

        /**
         * @property
         */
        popupCloseOnLabel: true,

        /**
         * @property
         */
        popupCloseButton: true,

        /**
         * @property
         */
        popupIcon: false,

        /**
         * @property
         */
        popupBadge: false,

        /**
         * @property
         */
        previousClass: null,

        events: {
            click: 'show'
        },

        /**
         * @property
         */
        stopEventsPropagation: true,

        /**
         * @property
         */
        stopEventsList: 'mousedown focusin',

        /**
         * @property
         */
        $popup: null,

        /**
         * @inheritDoc
         */
        constructor: function FullscreenPopupView() {
            FullscreenPopupView.__super__.constructor.apply(this, arguments);
        },

        /**
         * @inheritDoc
         */
        initialize: function(options) {
            _.each(this.sections, this._initializeSection.bind(this, options));
            return FullscreenPopupView.__super__.initialize.apply(this, arguments);
        },

        /**
         * @inheritDoc
         */
        dispose: function() {
            if (this.disposed) {
                return;
            }
            this.close();
            FullscreenPopupView.__super__.dispose.apply(this, arguments);
        },

        show: function() {
            this.close();
            this.$popup = $(this.getTemplateFunction()(this.getTemplateData()));

            this.$popup.appendTo($('body'));

            var promises = _.map(this.sections, this.showSection, this);
            $.when.apply($, promises).then(this._onShow.bind(this));
        },

        showSection: function(section) {
            var deferred = $.Deferred();
            this[section].$el = this.$popup.find('[data-role="' + section + '"]');
            if (false === this._eachSectionVariant(section, '_renderSection', deferred)) {
                deferred.resolve();
            }
            return deferred.promise();
        },

        /**
         * @inheritDoc
         */
        getTemplateData: function() {
            var data = FullscreenPopupView.__super__.getTemplateData.apply(this, arguments);
            data = _.extend({}, data, {
                close: this.popupCloseButton
            });
            return data;
        },

        /**
         * @param {String} title
         */
        setPopupTitle: function(title) {
            if (this.$popup) {
                this.$popup.find('[data-role="title"]').html(title);
            }
        },

        close: function() {
            if (!this.$popup) {
                return;
            }

            scrollHelper.enableBodyTouchScroll();

            _.each(this.sections, this.closeSection, this);

            this.$popup.find('[data-scroll="true"]').off('touchstart');
            this.$popup.remove();
            delete this.$popup;

            this.trigger('close');
        },

        closeSection: function(section) {
            this._eachSectionVariant(section, '_closeSection');
        },

        _eachSectionVariant: function(section, callback) {
            var args = _.rest(arguments, 2);
            var sectionOptions = this[section];
            var variant;
            for (var i = 0; i < this.sectionOptionVariants.length; i++) {
                variant = this.sectionOptionVariants[i];
                var func = callback + variant;
                var variantValue = sectionOptions[variant];
                if (this[func] && variantValue !== null) {
                    return this[func].apply(this, args.concat([
                        section,
                        sectionOptions,
                        variant,
                        variantValue
                    ]));
                }
            }
        },

        _initializeSection: function(options, section) {
            var sectionOptions = this[section] = _.extend({}, this[section] || {});
            sectionOptions.options = _.extend({}, sectionOptions.options || {}, options[section + 'Options'] || {});
            sectionOptions.attr = options[section + 'Attributes'] || {};

            this.sectionOptionVariants = _.map(this.sectionOptionVariants, function(variant) {
                var value = options[section + variant];
                variant = variant || 'Content';
                sectionOptions[variant] = value !== undefined ? value : (sectionOptions[variant] || null);

                return variant;
            }, this);

            if (section === 'header' && _.isObject(sectionOptions.options.templateData)) {
                sectionOptions.options.templateData = _.extend({
                    label: this.popupLabel,
                    closeOnLabel: this.popupCloseOnLabel,
                    icon: this.popupIcon,
                    badge: this.popupBadge
                }, sectionOptions.options.templateData);
            }
        },

        _onShow: function() {
            this._initPopupEvents();
            mediator.trigger('layout:reposition');
            if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
             || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
               scrollHelper.disableBodyTouchScroll();
            }
            this.trigger('show');
        },

        _renderSectionContent: function(deferred, section, sectionOptions, option, content) {
            sectionOptions.$el.html(content);
            deferred.resolve();
        },

        _renderSectionElement: function(deferred, section, sectionOptions, option, element) {
            var $element = $(element);
            sectionOptions.$placeholder = $('<div/>');

            this._savePreviousClasses($element);
            $element.after(sectionOptions.$placeholder)
                .attr(sectionOptions.attr);
            sectionOptions.$el.append($element);

            deferred.resolve();
        },

        _renderSectionSelector: function(deferred, section, sectionOptions, option, selector) {
            return this._renderSectionContent(deferred, section, sectionOptions, option, $(selector).html());
        },

        _renderSectionView: function(deferred, section, sectionOptions, option, View) {
            if (_.isString(View)) {
                tools.loadModules(View, _.bind(function(View) {
                    sectionOptions.View = View;
                    this._renderSectionView(deferred, section, sectionOptions, option, View);
                }, this));
            } else {
                this.subview(section, new View(
                    _.extend(this[section].options, {
                        el: sectionOptions.$el.get()
                    })
                ));
                deferred.resolve();
            }
        },

        _renderSectionTemplate: function(deferred, section, sectionOptions, option, template) {
            var templateData = sectionOptions.options.templateData;
            if (!templateData) {
                return false;
            }
            return this._renderSectionContent(deferred, section, sectionOptions, option, template(templateData));
        },

        _closeSectionElement: function(section, sectionOptions, option, element) {
            if (!sectionOptions.$placeholder) {
                return;
            }
            var $element = $(element);

            sectionOptions.$placeholder
                .after($element)
                .remove();

            $element.removeAttr(_.keys(sectionOptions.attr).join(' '));
            this._setPreviousClasses($element);
        },

        _closeSectionView: function(section, sectionOptions, option, View) {
            this.removeSubview(section);
        },

        _initPopupEvents: function() {
            this.$popup
                .on('click', '[data-role="close"]', _.bind(this.close, this))
                .on('touchstart', '[data-scroll="true"]', _.bind(scrollHelper.removeIOSRubberEffect, this));

            if (this.stopEventsPropagation) {
                this.$popup.on(this.stopEventsList, function(e) {
                    e.stopPropagation();
                });
            }
        },

        /**
         * @param {jQuery} $el
         */
        _savePreviousClasses: function($el) {
            this.previousClass = $el.attr('class');
        },

        /**
         * @param {jQuery} $el
         */
        _setPreviousClasses: function($el) {
            $el.attr('class', this.previousClass);
        }
    });

    return FullscreenPopupView;
});
