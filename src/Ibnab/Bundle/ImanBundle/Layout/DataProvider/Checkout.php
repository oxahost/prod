<?php

namespace Ibnab\Bundle\ImanBundle\Layout\DataProvider;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Oro\Bundle\ActionBundle\Button\ButtonInterface;
use Oro\Bundle\ActionBundle\Button\ButtonSearchContext;
use Oro\Bundle\ActionBundle\Provider\ButtonProvider;
use Oro\Bundle\ActionBundle\Provider\ButtonSearchContextProvider;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ActionBundle\Layout\DataProvider\LayoutButtonProvider;
/**
 * Provides payment context by checkout
 */
class Checkout extends LayoutButtonProvider
{


    /** @var ContainerInterface */
    protected $container;
    /** @var ButtonProvider */
    protected $buttonProvider;

    /** @var DoctrineHelper */
    protected $doctrineHelper;

    /** @var ButtonSearchContextProvider */
    protected $contextProvider;
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container,
        ButtonProvider $buttonProvider,
        DoctrineHelper $doctrineHelper,
        ButtonSearchContextProvider $contextProvider) {
        $this->container = $container;
        $this->buttonProvider = $buttonProvider;
        $this->doctrineHelper = $doctrineHelper;
        $this->contextProvider = $contextProvider;
        parent::__construct($buttonProvider,$doctrineHelper,$contextProvider);
    }
    public function getByGroup($entity = null, $datagrid = null, $group = null)
    {
        $buttonContext =   $this->prepareButtonSearchContext($entity, $datagrid, $group)->setRouteName('oro_shopping_list_frontend_view');
        return $this->buttonProvider->findAvailable($buttonContext);
    }
    /**
     * @param object $entity
     * @param string|null $datagrid
     * @param string|null $group
     *
     * @return ButtonSearchContext
     */
    private function prepareButtonSearchContext($entity = null, $datagrid = null, $group = null)
    {
        $buttonSearchContext = $this->contextProvider->getButtonSearchContext()
            ->setGroup($group)
            ->setDatagrid($datagrid);
        if (is_object($entity)) {
            $entityClass = $this->doctrineHelper->getEntityClass($entity);
            $entityId = null;
            if (!$this->doctrineHelper->isNewEntity($entity)) {
                $entityId = $this->doctrineHelper->getSingleEntityIdentifier($entity);
            }
            $buttonSearchContext->setEntity($entityClass, $entityId);
        }
        $buttonSearchContext->setRouteName('oro_shopping_list_frontend_view');
        return $buttonSearchContext;
    }
    public function getShoppingList()
    { 
      return $this->container->get('oro_shopping_list.manager.current_shopping_list')->getCurrent();
    }

}
