<?php

namespace Ibnab\Bundle\NewsletterBundle\Form\Type;

use Doctrine\Persistence\ManagerRegistry;
use Oro\Bundle\SecurityBundle\ORM\Walker\AclHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Oro\Bundle\ConsentBundle\Entity\Consent;
use Oro\Bundle\CMSBundle\Entity\Page;

class NewsletterConfigType extends AbstractType
{
    /** @var ManagerRegistry */
    private $registry;


    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'placeholder' => 'oro.newletter.form.newsletter_config_choice.placeholder',
        ]);
        $resolver->setRequired('entityClass');
        $resolver->setNormalizer(
            'choices',
            function (OptionsResolver $options) {
                if($options['entityClass'] == 'Page'){
                $page= [];
                $results =  $this->registry->getManagerForClass(Page::class)
                    ->getRepository(Page::class)
                    ->findAll();
                foreach($results as $result){
                    $page[$result->getId()] = (string) $result->getDefaultTitle() .' - id: '.$result->getId();
                }
                return array_flip($page);
                }
                if($options['entityClass'] == 'Consent'){
                $consent= [];
                $results = $this->registry->getManagerForClass(Consent::class)
                    ->getRepository(Consent::class)
                    ->findAll();
                
                foreach($results as $result){
                    $names = $result->getNames();
                    $currentName = 'consent';
                    foreach($names as $name){
                       $currentName =  $name;
                       break;
                    }
                    $consent[$result->getId()] = (string) $currentName .' - id: '.$result->getId();
                }
                return array_flip($consent);
                }
                return null;
            }
        );
        $resolver->setAllowedTypes('entityClass', ['null', 'string']);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
