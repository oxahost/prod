<?php

namespace Ibnab\Bundle\NewsletterBundle\Layout\DataProvider;

use Ibnab\Bundle\NewsletterBundle\Provider\ConfigurationProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LayoutProvider
{

    /** @var ContainerInterface */
    protected $container;
    
    protected $configurationProvider;
    
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->configurationProvider = $container->get(ConfigurationProvider::class);
    }
    /**
     *
     * @return int
     */
    public function isEnable() {
        return $this->configurationProvider->getEnable();
    } 
}
