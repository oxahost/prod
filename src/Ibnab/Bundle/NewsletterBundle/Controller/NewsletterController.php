<?php

namespace Ibnab\Bundle\NewsletterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Oro\Bundle\CustomerBundle\Security\CustomerUserProvider;
use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Oro\Bundle\ConsentBundle\Entity\Consent;
use Oro\Bundle\ConsentBundle\Entity\ConsentAcceptance;
use Oro\Bundle\CMSBundle\Entity\Page;
//use Oro\Bundle\CMSBundle\Entity\Repository\PageRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Ibnab\Bundle\NewsletterBundle\Provider\ConfigurationProvider;

class NewsletterController extends AbstractController {

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * @Route("/get-email", name="ibnab_newsletter_get_email", options= {"expose"= true})
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function getEmailAction(Request $request) {

        $this->tokenStorage = $this->get("security.token_storage");
        $subsEmail = $request->query->get('email');
        $configProvider = $this->get(ConfigurationProvider::class);
        $result['status'] = 0;
        if($configProvider->getEnable()){
        $user = $this->getLoggedUser();
        if (!$user instanceOf CustomerUser && !is_null($subsEmail)) {
            $subsEmail = strtolower($subsEmail);
            $customerUser = $this->getDoctrine()->getRepository(CustomerUser::class);
            $user = $customerUser->findOneBy(['emailLowercase' => $subsEmail]);
        }

        if ($user instanceOf CustomerUser && !is_null($user)) {
            
            //$subsEmail = $request->query->get('email');
            $consentRepository = $this->getDoctrine()->getRepository(Consent::class);
            $consentAcceptanceRepository = $this->getDoctrine()->getRepository(ConsentAcceptance::class);
            $pageRepository = $this->getDoctrine()->getRepository(Page::class);
            $consent = $consentRepository->find(2);
            $page = $pageRepository->find(14);
            $existConsentAcceptance = $consentAcceptanceRepository->findBy(['landingPage' => $page, 'consent' => $consent, 'customerUser' => $user]);
            if (!$existConsentAcceptance) {
                $consentAcceptance = new ConsentAcceptance();
                $consentAcceptance->setCustomerUser($user);
                $consentAcceptance->setLandingPage($configProvider->getPage());
                $consentAcceptance->setConsent($configProvider->getConsent());
                $em = $this->getDoctrine()->getManager();
                $em->persist($consentAcceptance);
                $em->flush();
                // registered
                $result['status'] = 1;
            }else{
                // exist
                $result['status'] = 2;
            }
        }else{
           $lead = $configProvider->getLead();
           //$result['status'] = $lead.'ok';
           if($lead == 0){
              $result['status'] = 3; 
           }
        }
        }
        return new JsonResponse($result);

    }

    /**
     * @return mixed
     */
    protected function getLoggedUser() {
        $token = $this->tokenStorage->getToken();

        return $token ? $token->getUser() : null;
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
        return array_merge(
            parent::getSubscribedServices(),
            [
                ConfigurationProvider::class,
            ]
        );
    }
}
