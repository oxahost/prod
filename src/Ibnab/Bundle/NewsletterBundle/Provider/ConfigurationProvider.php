<?php

namespace Ibnab\Bundle\NewsletterBundle\Provider;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;

class ConfigurationProvider
{
    const ENABLE_FIELD = 'ibnab_newsletter.enable';
    const PAGE_FIELD = 'ibnab_newsletter.page';
    const CONSENT_FIELD = 'ibnab_newsletter.consent';
    const LEAD_FIELD = 'ibnab_newsletter.lead';
    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }

    /**
     * @return string
     */
    public function getEnable()
    {       
        return $this->configManager->get(self::ENABLE_FIELD);    
    }
    public function getPage()
    {       
        return $this->configManager->get(self::PAGE_FIELD);    
    }
    public function getConsent()
    {       
        return $this->configManager->get(self::CONSENT_FIELD);    
    }
    public function getLead()
    {       
        return $this->configManager->get(self::LEAD_FIELD);    
    } 
}
