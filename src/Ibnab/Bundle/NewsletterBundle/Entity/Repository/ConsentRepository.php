<?php

namespace Ibnab\Bundle\NewsletterBundle\Entity\Repository;

use Doctrine\ORM\QueryBuilder;
use Oro\Bundle\ConsentBundle\Entity\Consent;
use Oro\Bundle\ConsentBundle\Entity\Repository\ConsentRepository as ParentConsentRepository;

class ConsentRepository extends ParentConsentRepository
{
    public function getConsents()
    {
        return $this->getEntityManager()
                    ->createQuery("SELECT c FROM OroConsentBundle:Consent c ")
                    ->getResult();
    }
    

}
