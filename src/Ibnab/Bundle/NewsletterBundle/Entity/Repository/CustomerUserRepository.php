<?php

namespace Ibnab\Bundle\NewsletterBundle\Entity\Repository;

use Doctrine\ORM\QueryBuilder;
use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Oro\Bundle\CustomerBundle\Entity\Repository\CustomerUserRepository as ParentCustomerUserRepository;

class CustomerUserRepository extends ParentCustomerUserRepository
{
   
    public function getCustomerUsers()
    {
        return $this->getEntityManager()
            ->createQuery("SELECT u FROM OroCustomerBundle:CustomerUser u")
            ->getResult();
    }
    
}
