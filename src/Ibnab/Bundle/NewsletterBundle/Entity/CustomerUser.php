<?php

namespace Ibnab\Bundle\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\CustomerBundle\Entity\CustomerUser as ParentCustomerUser;

/**
 * @ORM\Entity(repositoryClass="Ibnab\Bundle\NewsletterBundle\Entity\Repository\CustomerUserRepository")
 */
class CustomerUser extends ParentCustomerUser
{
    
}
