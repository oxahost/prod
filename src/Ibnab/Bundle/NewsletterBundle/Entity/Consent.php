<?php

namespace Ibnab\Bundle\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\ConsentBundle\Entity\Consent as ParentConsent;

/**
 * @ORM\Entity(repositoryClass="Ibnab\Bundle\NewsletterBundle\Entity\Repository\ConsentRepository")
 */
class Consent extends ParentConsent
{
    
}
