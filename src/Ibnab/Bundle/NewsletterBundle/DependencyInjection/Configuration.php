<?php

namespace Ibnab\Bundle\NewsletterBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;
/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ibnab_newsletter');
        SettingsBuilder::append(
            $rootNode,
            array(
                'enable'   => ['value' => 1],
                'page'   => ['value' => 14],
                'consent'   => ['value' => 2],
                'lead'   => ['value' => 0],
            )
        );
        return $treeBuilder;
    }

    
}
