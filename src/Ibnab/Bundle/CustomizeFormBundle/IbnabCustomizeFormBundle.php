<?php

namespace Ibnab\Bundle\CustomizeFormBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IbnabCustomizeFormBundle extends Bundle
{
    public function getParent()
    {
        return 'OroFormBundle';
    }
}
