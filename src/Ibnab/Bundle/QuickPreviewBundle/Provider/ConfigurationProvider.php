<?php

namespace Ibnab\Bundle\QuickPreviewBundle\Provider;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;

class ConfigurationProvider
{
    const ENABLE_FIELD = 'ibnab_quick_preview.enable';
    const ENABLEGRID_FIELD = 'ibnab_quick_preview.enablegrid';
    const ENABLEFEATURE_FIELD = 'ibnab_quick_preview.enablefeature';
    const ENABLENEW_FIELD = 'ibnab_quick_preview.enablenew';
    const ENABLERELATED_FIELD = 'ibnab_quick_preview.enablerelated';
    const ENABLEUPSELL_FIELD = 'ibnab_quick_preview.enableupsell';
    const ENABLETOPSELL_FIELD = 'ibnab_quick_preview.enabletopsell';
   
    const SHOWSKU_FIELD = 'ibnab_quick_preview.showsku';
    const SHOWBRAND_FIELD = 'ibnab_quick_preview.showbrand';
    const SHOWDECRIPTION_FIELD = 'ibnab_quick_preview.showdecription';
    const SHOWMEDIA_FIELD = 'ibnab_quick_preview.showmedia';
    const SHOWSPECIFICATION_FIELD = 'ibnab_quick_preview.showspecification';
    const SHOWRELATEDPRODUCTS_FIELD = 'ibnab_quick_preview.showrelatedproducts';
    const SHOWUPSELLPRODUCTS_FIELD = 'ibnab_quick_preview.showupsellproducts';
    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }

    /**
     * @return string
     */
    public function getEnable()
    {       
        return $this->configManager->get(self::ENABLE_FIELD);    
    }
    public function getEnableGrid()
    {       
        return $this->configManager->get(self::ENABLEGRID_FIELD);    
    }
    public function getEnableFeature()
    {       
        return $this->configManager->get(self::ENABLEFEATURE_FIELD);    
    }
    public function getEnableNew()
    {       
        return $this->configManager->get(self::ENABLENEW_FIELD);    
    }
    
    public function getEnableRelated()
    {       
        return $this->configManager->get(self::ENABLERELATED_FIELD);    
    }
    public function getEnableUpsell()
    {       
        return $this->configManager->get(self::ENABLEUPSELL_FIELD);    
    }
    public function getEnableTopsell()
    {       
        return $this->configManager->get(self::ENABLETOPSELL_FIELD);    
    }

    
    public function getShowSku()
    {       
        return $this->configManager->get(self::SHOWSKU_FIELD);    
    }  
    public function getShowBrand()
    {       
        return $this->configManager->get(self::SHOWBRAND_FIELD);    
    }  
    public function getShowDecription()
    {       
        return $this->configManager->get(self::SHOWDECRIPTION_FIELD);    
    }  
    public function getShowMedia()
    {       
        return $this->configManager->get(self::SHOWMEDIA_FIELD);    
    }  
    public function getShowSpecification()
    {       
        return $this->configManager->get(self::SHOWSPECIFICATION_FIELD);    
    }  
    public function getShowRelatedproducts()
    {       
        return $this->configManager->get(self::SHOWRELATEDPRODUCTS_FIELD);    
    }  
    public function getShowUpsellproducts()
    {       
        return $this->configManager->get(self::SHOWUPSELLPRODUCTS_FIELD);    
    }  
}
