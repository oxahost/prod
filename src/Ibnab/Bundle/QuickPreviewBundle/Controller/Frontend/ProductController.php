<?php
namespace Ibnab\Bundle\QuickPreviewBundle\Controller\Frontend;
use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Oro\Bundle\PricingBundle\Form\Extension\PriceAttributesProductFormExtension;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Oro\Bundle\ProductBundle\Controller\Frontend\ProductController as ParentProductController;
/**
 * The controller for the product view and search functionality.
 */
class ProductController extends AbstractController
{


    /**
     * View list of products
     *
     * @Route("/view/{id}", name="ibnab_quickpreview_frontend_product_view", requirements={"id"="\d+"}, options= {"expose" = true})
     * @Layout(vars={"product_type", "attribute_family", "page_template"})
     * @Acl(
     *      id="oro_product_frontend_view",
     *      type="entity",
     *      class="OroProductBundle:Product",
     *      permission="VIEW",
     *      group_name="commerce"
     * )
     *
     * @param Request $request
     * @param Product $product
     *
     * @return array
     */
    public function viewQuickAction(Request $request, Product $product)
    {
          $productController = $this->get(ParentProductController::class);
          return $productController->viewAction($request,$product);
    }
}
