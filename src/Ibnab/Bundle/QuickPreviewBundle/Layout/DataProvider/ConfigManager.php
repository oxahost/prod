<?php

namespace Ibnab\Bundle\QuickPreviewBundle\Layout\DataProvider;

use Ibnab\Bundle\QuickPreviewBundle\Provider\ConfigurationProvider;
use Symfony\Component\HttpFoundation\RequestStack;

class ConfigManager
{
    /**
     * @var ConfigManager
     */
    protected $configProvider;
    protected $requestStack;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigurationProvider $configProvider,RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->configProvider = $configProvider;
    }
    public function getEnable()
    {       
        return $this->configProvider->getEnable();    
    }
    public function getEnableGrid()
    {       
        return $this->configProvider->getEnableGrid();    
    }
    public function getEnableFeature()
    {       
        return $this->configProvider->getEnableFeature();    
    }
    public function getEnableNew()
    {       
        return $this->configProvider->getEnableNew();    
    }
    public function getEnableRelated()
    {       
        return $this->configProvider->getEnableRelated();    
    }
    public function getEnableUpsell()
    {       
        return $this->configProvider->getEnableUpsell();   
    }
    public function getEnableTopsell()
    {       
        return $this->configProvider->getEnableTopsell();   
    }
    public function getIsQuickView()
    {       
       $request = $this->requestStack->getCurrentRequest();   
       $type = $request->query->get("type");
       if($type == "quick-view-popup"){
           return 1;
       }
       return 0;
    }  
    
    public function getShowSku()
    {       
        return $this->getIsQuickView() && $this->configProvider->getShowSku();    
    }  
    public function getShowDecription()
    {       
        return $this->getIsQuickView() && $this->configProvider->getShowDecription();    
    }  
    public function getShowMedia()
    {       
        return $this->getIsQuickView() && $this->configProvider->getShowMedia();    
    }  
    public function getShowSpecification()
    {       
        return $this->getIsQuickView() && $this->configProvider->getShowSpecification();    
    }  
    public function getShowRelatedproducts()
    {       
        return $this->getIsQuickView() && $this->configProvider->getShowRelatedproducts();    
    }  
    public function getShowUpsellproducts()
    {       
        return $this->configProvider->getShowUpsellproducts();    
    }
    public function getShowBrand()
    {    
        return $this->configProvider->getShowBrand(); 
    }
}
