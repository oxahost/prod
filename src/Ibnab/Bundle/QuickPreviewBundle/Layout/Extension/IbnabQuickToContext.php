<?php

namespace Ibnab\Bundle\QuickPreviewBundle\Layout\Extension;

use Oro\Component\Layout\ContextConfiguratorInterface;
use Oro\Component\Layout\ContextInterface;
use Ibnab\Bundle\QuickPreviewBundle\Layout\DataProvider\ConfigManager;

class IbnabQuickToContext implements ContextConfiguratorInterface
{
    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }
    /**
     * {@inheritdoc}
     */
    public function configureContext(ContextInterface $context)
    {
        $context->getResolver()
            ->setRequired(['ibnab_quickview'])
            ->setAllowedTypes('ibnab_quickview', ['numeric']);
        $context->set('ibnab_quickview', $this->configManager->getIsQuickView());
    }

}
