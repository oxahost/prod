<?php

namespace Ibnab\Bundle\QuickPreviewBundle\Layout\Extension\Theme;

use Oro\Component\Layout\ContextAwareInterface;
use Oro\Component\Layout\ContextInterface;
use Oro\Component\Layout\Extension\Theme\PathProvider\PathProviderInterface;

class IbnabQuickPathProvider implements PathProviderInterface, ContextAwareInterface {

    /** @var ContextInterface */
    protected $context;

    /**
     * {@inheritdoc}
     */
    public function setContext(ContextInterface $context) {
        $this->context = $context;
    }

    /**
     * {@inheritdoc}
     */
    public function getPaths(array $existingPaths) {

        $routeName = $this->context->getOr('route_name');
        $paths = [];
        
        if ($routeName && $routeName == "ibnab_quickpreview_frontend_product_view") {
            $routeName = 'oro_product_frontend_product_view';
            foreach ($existingPaths as $path) {
                $paths[] = $path;
                $paths[] = implode(self::DELIMITER, [$path, $routeName]);
            }
            return $paths;
        }

        return $existingPaths;
    }

}
