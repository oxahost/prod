<?php

namespace Ibnab\Bundle\QuickPreviewBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ibnab_quick_preview');
        SettingsBuilder::append(
            $rootNode,
            array(
                'enable'   => ['value' => 1],
                'enablegrid'   => ['value' => 1],
                'enablefeature'   => ['value' => 1],
                'enablenew'   => ['value' => 1],
                'enablerelated'   => ['value' => 1],
                'enableupsell'   => ['value' => 1],
                'enabletopsell'   => ['value' => 1],
                
                'showsku'   => ['value' => 1],
                'showbrand'   => ['value' => 1],
                'showdecription'   => ['value' => 1],
                'showmedia'   => ['value' => 1],
                'showspecification'   => ['value' => 1],
                'showrelatedproducts'   => ['value' => 0],
                'showupsellproducts'   => ['value' => 0],
            )
        );
        return $treeBuilder;
    }

    
}
