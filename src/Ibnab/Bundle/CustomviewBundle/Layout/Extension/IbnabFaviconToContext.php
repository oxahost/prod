<?php

namespace Ibnab\Bundle\CustomviewBundle\Layout\Extension;

use Oro\Component\Layout\ContextConfiguratorInterface;
use Oro\Component\Layout\ContextInterface;
use Ibnab\Bundle\CustomviewBundle\Provider\ConfigurationProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Oro\Bundle\ThemeBundle\Model\ThemeRegistry;
use Symfony\Component\Asset\Packages as AssetHelper;


class IbnabFaviconToContext implements ContextConfiguratorInterface
{
    const Favicon_FIELD = 'ibnab_customview.favicon';
    const IncludeCSS_FIELD = 'ibnab_customview.color_enable';

    /**
     * @var ConfigManager
     */
    protected $configurationProvider;
    /** @var ContainerInterface */
    protected $container;
    /** @var ThemeRegistry */
    private $themeRegistry;
    /**
     * @var AssetHelper
     */
    private $assetHelper;    
    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigurationProvider $configurationProvider,ThemeRegistry $themeRegistry, AssetHelper $assetHelper )
    {
        $this->configurationProvider = $configurationProvider;
        $this->themeRegistry = $themeRegistry;
        $this->assetHelper = $assetHelper;
    }
    /**
     * {@inheritdoc}
     */
   public function configureContext(ContextInterface $context)
    {
        $favicon = $this->configurationProvider->getFavicon();
        if ($favicon == "" || is_null($favicon)) {
         $activeTheme = $this->themeRegistry->getActiveTheme();
        if ($activeTheme) {
            $favicon = $this->assetHelper->getUrl($activeTheme->getIcon()); 
        }
        }
        $context->getResolver()
            ->setRequired(['ibnab_favicon'])
            ->setAllowedTypes('ibnab_favicon', ['string', 'null']);
        $context->set('ibnab_favicon', $favicon);
        $includeCSS = $this->configurationProvider->getIncludeCSS();
        $context->getResolver()
            ->setRequired(['ibnab_includecss'])
            ->setAllowedTypes('ibnab_includecss', ['int','string', 'null']);
        $context->set('ibnab_includecss', $includeCSS);
        $themeStyle = $this->configurationProvider->getThemeStyle();
        $context->getResolver()
            ->setRequired(['theme_style'])
            ->setAllowedTypes('theme_style', ['int','string', 'null']);
        $context->set('theme_style', $themeStyle);
    }

}
