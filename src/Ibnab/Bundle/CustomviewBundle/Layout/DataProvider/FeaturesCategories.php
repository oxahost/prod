<?php

namespace Ibnab\Bundle\CustomviewBundle\Layout\DataProvider;


use Oro\Component\Layout\ContextInterface;
use Ibnab\Bundle\CustomviewBundle\Provider\ConfigurationProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Oro\Bundle\CatalogBundle\Layout\DataProvider\CategoriesProductsProvider;
use Oro\Bundle\CatalogBundle\Layout\DataProvider\FeaturedCategoriesProvider;

class FeaturesCategories
{
    /**
     * @var ConfigManager
     */
    protected $configurationProvider;
    /** @var ContainerInterface */
    protected $container;
    
    /** @var CategoriesProductsProvider */
    protected $categoriesProductsProvider;
    /** @var FeaturedCategoriesProvider */
    protected $featuredCategoriesProvider;
    
    
    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigurationProvider $configurationProvider,ContainerInterface $container,CategoriesProductsProvider $categoriesProductsProvider,FeaturedCategoriesProvider $featuredCategoriesProvider)
    {
        $this->configurationProvider = $configurationProvider;
        $this->container = $container;
        $this->categoriesProductsProvider = $categoriesProductsProvider;
        $this->featuredCategoriesProvider = $featuredCategoriesProvider;
    }
    /**
     *
     * @return int
     */
    public function isEnable() {
        return $this->configurationProvider->getIFC();
    } 
    /**
     *
     * @return int
     */
    public function getIncludeCSS()
    {
        return $this->configurationProvider->getIncludeCSS();
    }
    /**
     *
     * @return int
     */
    public function isFMEnable() {
        return $this->configurationProvider->getIFM();
    }  
    /**
     *
     * @return int
     */
    public function isFPEnable() {
        return $this->configurationProvider->getIFP();
    }  
    /**
     *
     * @return int
     */
    public function isTPEnable() {
        return $this->configurationProvider->getITP();
    }
    /**
     *
     * @return int
     */
    public function isNPEnable() {
        return $this->configurationProvider->getINP();
    }  
    public function getAllFC() {
        $allFC = $this->configurationProvider->getFC();
        $allArrayFC = array_map('intval', explode(",",$allFC));
        $result = $this->featuredCategoriesProvider->getAll($allArrayFC);
        return $result;
    }    
    public function getAllFCP() {
        $allFC = $this->configurationProvider->getFC();
        $allArrayFC = array_map('intval', explode(",",$allFC));
        $result = $this->categoriesProductsProvider->getCountByCategories($allArrayFC);
        return $result;
    }
    public function isSHEnable()
    {
        return $this->configurationProvider->getSH();
    }
    public function getFT()
    {
        return $this->configurationProvider->getFT();
    }
    /**
     * @return string
     */
    public function isUseWidget()
    {
        return $this->configurationProvider->isUseWidget();
    }
}
