<?php
namespace Ibnab\Bundle\CustomviewBundle\Helper;

use Ibnab\Bundle\CustomviewBundle\Provider\ConfigurationProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
/**
 * Helper service to collect product image custom information
 */
class AssetsBuilder{

    /** @var ContainerInterface */
    protected $container;    
    /**
     * @var ConfigManager
     */
    protected $configProvider;
    
    public function __construct(
       ConfigurationProvider $configProvider, ContainerInterface $container
    ) {
        $this->configProvider = $configProvider;
        $this->container = $container;

    }
    public function updateCss(){
            $cssTemplate = ".container.testimonial .text .testimonial_fa,.easy-slider__info .btn--info,.button--info,.btn--info,.cart-widget__icon,.shopping-list__item-indicator,.checkout-navigation__icon--current,.checkout__title-icon,.container.testimonial .view-all .btn--info{border-color:".$this->configProvider->getButtonColor()." ;background-color:".$this->configProvider->getButtonColor()."}";
            $cssTemplate .= ".middlebar__right.forShopping .search-widget__submit{border-color:".$this->configProvider->getConfig('ibnab_customview.button_search')." ;background-color:".$this->configProvider->getConfig('ibnab_customview.button_search')."}";
            $cssTemplate .= ".middlebar__right.forShopping .f-icon{color:".$this->configProvider->getConfig('ibnab_customview.top_user')."}";
            
            $cssTemplate .= ".main-menu .main-menu__link{color:".$this->configProvider->getConfig('ibnab_customview.mainmenu_baselink')."}";
            $cssTemplate .= ".main-menu .main-menu__item:hover .main-menu__link{color:".$this->configProvider->getConfig('ibnab_customview.mainmenu_baselink_hover').";border-top: 2px solid ".$this->configProvider->getConfig('ibnab_customview.mainmenu_baselink_hover').";}";

            $cssTemplate .= ".topbar .topbar-navigation .topbar-navigation__link{color:".$this->configProvider->getConfig('ibnab_customview.topnav_link')."}";

            
            $cssTemplate .= ".main-menu-column__item .main-menu-column__link{color:".$this->configProvider->getConfig('ibnab_customview.mainmenu_columnlink')."}";
            $cssTemplate .= ".main-menu-column__item .main-menu-column__link:hover{color:".$this->configProvider->getConfig('ibnab_customview.mainmenu_columnlink_hover')."}";
            $cssTemplate .= ".main-menu-column__title{color:".$this->configProvider->getConfig('ibnab_customview.mainmenu_title')."}";
            $cssTemplate .= ".main-menu-column__subitem .main-menu-column__link{color:".$this->configProvider->getConfig('ibnab_customview.mainmenu_subcolumnlink')."}";
            $cssTemplate .= ".main-menu-column__subitem .main-menu-column__link:hover{color:".$this->configProvider->getConfig('ibnab_customview.mainmenu_subcolumnlink_hover')."}";
            
            $cssTemplate .= ".middlebar__right.forShopping .cart-widget__content .f-icon,.middlebar__right.forShopping .cart-widget__content .cart-widget__list-trigger,.middlebar__right.forShopping .cart-widget__content .fa-angle-down{color:".$this->configProvider->getConfig('ibnab_customview.top_cart')."}";
            $cssTemplate .= ".add-product-from-view-component .button--info,.add-product-from-view-component .btn--info,.product-add-to-shopping-list-container .btn--info,.product-add-to-shopping-list-container .button{border-color:".$this->configProvider->getAddButtonColor().";background-color:".$this->configProvider->getAddButtonColor()."}";
            $cssTemplate .= ".product-item__sku-value,.topbar-navigation__link span,.catalog-switcher .btn.active, .catalog-switcher .uploader .action.active, .uploader .catalog-switcher .action.active,.container.testimonial h1,.page-footer .information-nav__title{color:".$this->configProvider->getLinkColor()."}";
            $cssTemplate .= ".button--info:hover,.btn--info:hover,.easy-slider .slick-arrow:hover {border-color:".$this->configProvider->getButtonHoverColor().";background-color:".$this->configProvider->getButtonHoverColor()." !important;color:#fff;border-color:".$this->configProvider->getButtonHoverColor()." !important}";
            $cssTemplate .= ".button--info:focus,.btn--info:focus{border-color:".$this->configProvider->getButtonHoverColor().";background-color:".$this->configProvider->getButtonHoverColor().";color:#fff}";
            $cssTemplate .= ".quick-access__icon--dark{background-color:".$this->configProvider->getQuickFormColor()."}";
            $cssTemplate .= ".quick-access__icon{background-color:".$this->configProvider->getQuickQuoteColor()."}";
            $cssTemplate .= ".quick-access__icon--light{background-color:".$this->configProvider->getQuickOrderColor()."}";
            $cssTemplate .= ".main-menu-outer{background-color:".$this->configProvider->getMenuColor()."}";
            $cssTemplate .= ".main-menu .main-menu__item:hover .main-menu__link, .main-menu .main-menu__item.active .main-menu__link{background-color:".$this->configProvider->getMenuHoverColor().";}";
            $cssTemplate .= ".shopping-list-widget .show-cart-details-checkout .btn-group.checkout-widget .btn--info{background-color:".$this->configProvider->getConfig('ibnab_customview.top_createorder_background')." !important; color:".$this->configProvider->getConfig('ibnab_customview.top_createorder_color')." !important;border: 0px;}";
            $cssTemplate .= ".shopping-list-widget .show-cart-details-checkout .btn-group.checkout-widget .btn--info:hover{background-color:".$this->configProvider->getConfig('ibnab_customview.top_createorder_background_hover')." !important; color:".$this->configProvider->getConfig('ibnab_customview.top_createorder_color_hover')." !important;border: 0px;}";
            $cssTemplate .= ".shopping-list-widget .show-cart-details-checkout .btn-group .btn--info{background-color:".$this->configProvider->getConfig('ibnab_customview.top_viewcart_background')." !important; color:".$this->configProvider->getConfig('ibnab_customview.top_viewcart_color')." !important;border: 0px;}";
            $cssTemplate .= ".shopping-list-widget .show-cart-details-checkout .btn-group .btn--info:hover{background-color:".$this->configProvider->getConfig('ibnab_customview.top_viewcart_background_hover')." !important; color:".$this->configProvider->getConfig('ibnab_customview.top_viewcart_color_hover')." !important;border: 0px;}";

            $cssTemplate .= ".easy-slider .slick-arrow{background-color:".$this->configProvider->getConfig('ibnab_customview.slidernav_backgroundcolor')." !important; color:".$this->configProvider->getConfig('ibnab_customview.slidernav_color')." !important;border: 0px;}";
            $cssTemplate .= ".easy-slider .slick-arrow:hover{background-color:".$this->configProvider->getConfig('ibnab_customview.slidernav_backgroundcolor_hover')." !important; color:".$this->configProvider->getConfig('ibnab_customview.slidernav_color_hover')." !important;border: 0px;}";
            $cssTemplate .= ".easy-slider .slick-dots li.slick-active button{background-color:".$this->configProvider->getConfig('ibnab_customview.slidernav_dotactive_color')." !important; border-color:".$this->configProvider->getConfig('ibnab_customview.slidernav_dotactive_background')." !important;}";
            $cssTemplate .= ".easy-slider .slick-dots button{border-color:".$this->configProvider->getConfig('ibnab_customview.slidernav_dotactive_background')." !important;}";

            $cssTemplate .= ".container.testimonial .back-stars{color:".$this->configProvider->getConfig('ibnab_customview.testimonial_starts_backgroundcolor')." !important;}";
            $cssTemplate .= ".container.testimonial .front-stars{color:".$this->configProvider->getConfig('ibnab_customview.testimonial_starts_filledbackgroundcolor')." !important;}";
                        
            $cssTemplate .= ".page-footer .information-nav__title{color:".$this->configProvider->getConfig('ibnab_customview.footer_title')."}";
            $cssTemplate .= ".page-footer .information-nav__item-content,.page-footer .information-nav__item{color:".$this->configProvider->getConfig('ibnab_customview.footer_link')."}";
            $cssTemplate .= ".page-footer .information-nav__item-content:hover,.page-footer .information-nav__item:hover{color:".$this->configProvider->getConfig('ibnab_customview.footer_link_hover')."}";
            $cssTemplate .= ".page-footer .copyright{color:".$this->configProvider->getConfig('ibnab_customview.footer_copyright')."}";

            $cssTemplate .= ".embedded-list__label:before{background-color:".$this->configProvider->getConfig('ibnab_customview.block_topborder')."; height:".$this->configProvider->getConfig('ibnab_customview.block_height')."px;display: none;}";
            $cssTemplate .= ".page-content .oro-datagrid{border-top: ".$this->configProvider->getConfig('ibnab_customview.block_height')."px solid ".$this->configProvider->getConfig('ibnab_customview.block_topborder')."; !important;}";
            $cssTemplate .= ".product-sticker--text-new_arrival{background-color:".$this->configProvider->getConfig('ibnab_customview.block_productsticker_backgroundcolor')." !important; color:".$this->configProvider->getConfig('ibnab_customview.block_productsticker_color')."}";
            $cssTemplate .= ".product-quick-preview-product{background-color:".$this->configProvider->getConfig('ibnab_customview.block_productquick_backgroundcolor')." !important; color:".$this->configProvider->getConfig('ibnab_customview.block_productquick_color')." !important}";
            $cssTemplate .= ".product-quick-preview-product:hover{background-color:".$this->configProvider->getConfig('ibnab_customview.block_productquick_backgroundcolor_hover')." !important; color:".$this->configProvider->getConfig('ibnab_customview.block_productquick_color_hover')." !important}";
            $cssTemplate .= ".view-product-gallery{background-color:".$this->configProvider->getConfig('ibnab_customview.block_imagequick_backgroundcolor')." !important; color:".$this->configProvider->getConfig('ibnab_customview.block_imagequick_color')." !important}";
            $cssTemplate .= ".view-product-gallery:hover{background-color:".$this->configProvider->getConfig('ibnab_customview.block_imagequick_backgroundcolor_hover')." !important; color:".$this->configProvider->getConfig('ibnab_customview.block_imagequick_color_hover')." !important}";
            $cssTemplate .= ".product-item__sku-value{color:".$this->configProvider->getConfig('ibnab_customview.block_productsku_color')."}";
            $cssTemplate .= ".product-price__value{color:".$this->configProvider->getConfig('ibnab_customview.block_productprice_color')."}";
            $cssTemplate .= ".input-group-btn.qty-plus .btn-number,.input-group-btn.qty-minus .btn-number{background-color:".$this->configProvider->getConfig('ibnab_customview.block_plusminus_backgroundcolor')." !important; color:".$this->configProvider->getConfig('ibnab_customview.block_plusminus_color')." !important}";
            $cssTemplate .= ".input-group-btn.qty-plus .btn-number:hover,.input-group-btn.qty-minus .btn-number:hover{background-color:".$this->configProvider->getConfig('ibnab_customview.block_plusminus_backgroundcolor_hover')." !important; color:".$this->configProvider->getConfig('ibnab_customview.block_plusminus_color_hover')." !important}";
            $cssTemplate .= ".button--info, .add-product-from-view-component .btn--info, .product-add-to-shopping-list-container .btn--info{background-color:".$this->configProvider->getConfig('ibnab_customview.block_addtocart_backgroundcolor')." !important; color:".$this->configProvider->getConfig('ibnab_customview.block_addtocart_color')." !important;border: 0px;}";
            $cssTemplate .= ".button--info, .add-product-from-view-component .btn--info:hover, .product-add-to-shopping-list-container .btn--info:hover{background-color:".$this->configProvider->getConfig('ibnab_customview.block_addtocart_backgroundcolor_hover')." !important; color:".$this->configProvider->getConfig('ibnab_customview.block_addtocart_color_hover')." !important;border: 0px;}";    

            $fs = new Filesystem();
            $fs->remove($this->container->get('kernel')->getProjectDir() .'/public/css/layout/'.$this->configProvider->getFT().'/cs.css');            
            $fs->dumpFile($this->container->get('kernel')->getProjectDir() .'/public/css/layout/'.$this->configProvider->getFT().'/cs.css', $cssTemplate);        
    }
    public function updateFavIcon(){
            $favicon = $this->configProvider->getFavicon();
            try{
            if ($favicon != "" || !is_null($favicon)) {
                $fs = new Filesystem();
                $publicPathRoot = $this->container->get('kernel')->getProjectDir() . '/public/favicon.ico';
                $publicPathFrontend = $this->container->get('kernel')->getProjectDir() . '/public/bundles/orofrontend/default';
                //$publicPathVendor = $this->container->get('kernel')->getProjectDir() . '/vendor/oro/customer-portal/src/Oro/Bundle/FrontendBundle/Resources/public/default';
                $pathVarFavZip = $this->container->get('kernel')->getProjectDir() . '/var/' . $this->configProvider->getAttachmentDir() . '/' . $favicon;
                //$fs->copy($this->container->get('kernel')->getProjectDir().'/var/'.$this->configProvider->getAttachmentDir().'/'.$favicon, $this->container->get('kernel')->getProjectDir() .'/public/'.$favicon, true);
                //$result = $this->unzip($pathVarFavZip, $publicPathVendor);
                if ($fs->exists($publicPathFrontend)) {
                    $result = $this->unzip($pathVarFavZip, $publicPathFrontend);
                    if ($fs->exists($publicPathFrontend . '/favicons/favicon.ico')) {
                        $fs->copy($publicPathFrontend . '/favicons/favicon.ico', $publicPathRoot, true);
                        $fs->copy($publicPathFrontend . '/favicons/favicon.ico', $publicPathFrontend . '/images/favicon.ico', true);
                        //$fs->copy($publicPathVendor . '/favicons/favicon.ico', $publicPathVendor . '/images/favicon.ico', true);
                    }
                }
            }    
            }catch(\Exception $e){
                
            }
    }
    /**
     * @param string $file
     * @param string $path
     *
     * @return bool
     * @throws \RuntimeException
     */
    protected function unzip($file, $path, $open = \ZipArchive::OVERWRITE) {
        $zip = new \ZipArchive();
        $res = $zip->open($file);

        $zipErrors = [
            \ZipArchive::ER_EXISTS => 'File already exists.',
            \ZipArchive::ER_INCONS => 'Zip archive inconsistent.',
            \ZipArchive::ER_INVAL => 'Invalid argument.',
            \ZipArchive::ER_MEMORY => 'Malloc failure.',
            \ZipArchive::ER_NOENT => 'No such file.',
            \ZipArchive::ER_NOZIP => 'Not a zip archive.',
            \ZipArchive::ER_OPEN => 'Can\'t open file.',
            \ZipArchive::ER_READ => 'Read error.',
            \ZipArchive::ER_SEEK => 'Seek error.',
        ];

        if ($res !== true) {
            throw new \RuntimeException($zipErrors[$res], $res);
        }

        $isExtracted = $zip->extractTo($path);
        if (!$isExtracted) {
            throw new \RuntimeException(sprintf('Pack %s can\'t be extracted', $file));
        }

        $isClosed = $zip->close();
        if (!$isClosed) {
            throw new \RuntimeException(sprintf('Pack %s can\'t be closed', $file));
        }

        return true;
    }
}
