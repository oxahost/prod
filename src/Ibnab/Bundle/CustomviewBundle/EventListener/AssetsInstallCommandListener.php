<?php

namespace Ibnab\Bundle\CustomviewBundle\EventListener;

use Symfony\Bundle\FrameworkBundle\Command\AssetsInstallCommand;
use Symfony\Component\Console\Command\Command;
use Ibnab\Bundle\CustomviewBundle\Provider\ConfigurationProvider;
use Ibnab\Bundle\CustomviewBundle\Helper\AssetsBuilder;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;

class AssetsInstallCommandListener
{
    
    /** @var AssetsBuilder */
    private $assetsBuilder;
    
    /**
     * @var ConfigManager
     */
    protected $configProvider;
    /**
     * @param Filesystem $filesystem
     * @param string     $kernelProjectDir
     */
    public function __construct(ConfigurationProvider $configProvider,AssetsBuilder $assetsBuilder)
    {
        $this->configProvider = $configProvider;
        $this->assetsBuilder = $assetsBuilder;
    }


    /**
     * @param ConsoleTerminateEvent $event
     */
    public function afterExecute(ConsoleTerminateEvent $event)
    {
        if ($this->isAssetsInstallCommand($event->getCommand())) {
           $this->assetsBuilder->updateCss();
           $this->assetsBuilder->updateFavIcon();
        }
    }

    /**
     * @param Command $command
     *
     * @return bool
     */
    private function isAssetsInstallCommand(Command $command)
    {
        return $command instanceof AssetsInstallCommand;
    }
}
