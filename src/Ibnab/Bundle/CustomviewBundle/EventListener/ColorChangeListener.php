<?php

namespace Ibnab\Bundle\CustomviewBundle\EventListener;

use Oro\Bundle\ConfigBundle\Event\ConfigUpdateEvent;
use Ibnab\Bundle\CustomviewBundle\Provider\ConfigurationProvider;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Ibnab\Bundle\CustomviewBundle\Helper\AssetsBuilder;


/**
 * Saves the date when the config cache was changed.
 */
class ColorChangeListener {

    /**
     * @var ConfigManager
     */
    protected $configProvider;

    /** @var ContainerInterface */
    protected $container;
    
    /** @var AssetsBuilder */
    private $assetsBuilder;
    
    const css = [ConfigurationProvider::Button_FIELD];

    /**
     * @param ConfigManager $configProvider
     */
    public function __construct(ConfigurationProvider $configProvider, ContainerInterface $container,AssetsBuilder $assetsBuilder) {
        $this->configProvider = $configProvider;
        $this->container = $container;
        $this->assetsBuilder = $assetsBuilder;
    }

    /**
     * @param ConfigUpdateEvent $event
     */
    public function onConfigUpdate(ConfigUpdateEvent $event) {
        $changeSet = $event->getChangeSet();
        /* if(!empty($changeSet) && (isset($changeSet[ConfigurationProvider::Logo_FIELD]) || isset($changeSet[ConfigurationProvider::LogoHeight_FIELD]) || isset($changeSet[ConfigurationProvider::Logowidth_FIELD]) || isset($changeSet[ConfigurationProvider::Button_FIELD]) || isset($changeSet[ConfigurationProvider::Buttonhover_FIELD]) ||  isset($changeSet[ConfigurationProvider::Menu_FIELD]) || isset($changeSet[ConfigurationProvider::Menuhover_FIELD]) || isset($changeSet[ConfigurationProvider::Quickform_FIELD]) || isset($changeSet[ConfigurationProvider::Quickorder_FIELD]) || isset($changeSet[ConfigurationProvider::Quickquote_FIELD]) )){
          $cssTemplate = ".button--info,.btn--info,.cart-widget__icon,.shopping-list__item-indicator,.checkout-navigation__icon--current,.checkout__title-icon{border-color:".$this->configProvider->getButtonColor().";background-color:".$this->configProvider->getButtonColor()."}";
          $cssTemplate .= ".product-item__sku-value,.main-menu-column__title,.topbar-navigation__link span,.catalog-switcher .btn.active, .catalog-switcher .uploader .action.active, .uploader .catalog-switcher .action.active{color:".$this->configProvider->getButtonColor()."}";
          $cssTemplate .= ".button--info:hover,.btn--info:hover{border-color:".$this->configProvider->getButtonHoverColor().";background-color:".$this->configProvider->getButtonHoverColor().";color:#fff}";
          $cssTemplate .= ".button--info:focus,.btn--info:focus{border-color:".$this->configProvider->getButtonHoverColor().";background-color:".$this->configProvider->getButtonHoverColor().";color:#fff}";
          $cssTemplate .= ".quick-access__icon--dark{background-color:".$this->configProvider->getQuickFormColor()."}";
          $cssTemplate .= ".quick-access__icon{background-color:".$this->configProvider->getQuickQuoteColor()."}";
          $cssTemplate .= ".quick-access__icon--light{background-color:".$this->configProvider->getQuickOrderColor()."}";
          $cssTemplate .= ".logo{width: ".$this->configProvider->getLogoWidth()."px;height: ".$this->configProvider->getLogoHeight()."px;}";
          $fs = new Filesystem();
          $fs->remove($this->container->get('kernel')->getRootDir().'/../public/css/layout/default/custom_view_color.css');
          $fs->dumpFile($this->container->get('kernel')->getRootDir().'/../public/css/layout/default/custom_view_color.css', $cssTemplate);
          } */

        if (!empty($changeSet) && (isset($changeSet[ConfigurationProvider::Favicon_FIELD]))) {
          $this->assetsBuilder->updateFavIcon();
        }
        if (!empty($changeSet)) {
           $this->assetsBuilder->updateCss();
        }
    }



}
