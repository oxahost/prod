<?php

namespace Ibnab\Bundle\CustomviewBundle\Condition;

use Oro\Bundle\RFPBundle\Form\Extension\RequestDataStorageExtension;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Oro\Bundle\ShoppingListBundle\Condition\RfpAllowed as ParentRfpAllowed;
use Ibnab\Bundle\CustomviewBundle\Provider\ConfigurationProvider;

/**
 * Check if at least one of products can be added to RFP
 * Usage:
 * @rfp_allowed: items
 */
class RfpAllowed extends ParentRfpAllowed {

    /**
     * @var RequestDataStorageExtension
     */
    protected $requestDataStorageExtension;

    /** @var ContainerInterface */
    protected $container;

    /**
     * @var ConfigManager
     */
    protected $configurationProvider;

    public function __construct(ContainerInterface $container, ConfigurationProvider $configurationProvider, RequestDataStorageExtension $requestDataStorageExtension) {
        $this->configurationProvider = $configurationProvider;
        $this->requestDataStorageExtension = $requestDataStorageExtension;
        $this->container = $container;
        parent::__construct($requestDataStorageExtension);
    }

    protected function getUser() {
        $token = $this->container->get('security.token_storage')->getToken();
        return $token->getUser();
    }

    /**
     * {@inheritdoc}
     */
    protected function isConditionAllowed($context) {
        $parentResult = parent::isConditionAllowed($context);
        if ($parentResult) {
            $user = $this->getUser();
            $RFQCG = $this->configurationProvider->getRFQCG();
            if (is_object($user)) {
                if (!is_null($user->getCustomer()->getGroup())) {
                    $group = $user->getCustomer()->getGroup()->getId();
                    if (in_array($group, $RFQCG)) {
                        return $parentResult;
                    }
                }
            }
        }
        return false;
    }

}
