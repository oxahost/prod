<?php

namespace Ibnab\Bundle\CustomviewBundle\Twig;

use Oro\Bundle\ThemeBundle\Model\ThemeRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Ibnab\Bundle\CustomviewBundle\Provider\ConfigurationProvider;
use Oro\Bundle\ThemeBundle\Model\ThemeRegistry;

use Oro\Bundle\ThemeBundle\Twig\ThemeExtension as ParentThemeExtension;

class ThemeExtension extends ParentThemeExtension
{
    const NAME = 'oro_theme';

    /** @var ThemeRegistry */
    private $themeRegistry;
    /**
     * @var ConfigManager
     */
    protected $configProvider;
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ThemeRegistry $themeRegistry,ConfigurationProvider $configProvider)
    {
        $this->themeRegistry = $themeRegistry;
        $this->configProvider = $configProvider;
    }


    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('oro_theme_logo', [$this, 'getThemeLogo']),
            new \Twig_SimpleFunction('oro_theme_icon', [$this, 'getThemeIcon']),
        ];
    }

    /**
     * Get theme logo
     *
     * @return string
     */
    public function getThemeLogo()
    {
        $result = '';
        $activeTheme = $this->themeRegistry->getActiveTheme();
        if ($activeTheme) {
            $result = $activeTheme->getLogo();
        }
        return $result;
    }

    /**
     * Get theme icon
     *
     * @return string
     */
    public function getThemeIcon()
    {
        
        $favicon = $this->configProvider->getFavicon();
        if($favicon  == "") {
        $result = '';
        $activeTheme = $this->themeRegistry->getActiveTheme();
        if ($activeTheme) {
            $result = $activeTheme->getIcon();
        }
        }else{
            $result = $favicon;
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::NAME;
    }
}
