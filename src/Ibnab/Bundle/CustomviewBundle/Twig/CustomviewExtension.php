<?php

namespace Ibnab\Bundle\CustomviewBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Oro\Bundle\ProductBundle\Layout\DataProvider\ProductFormAvailabilityProvider;
use Oro\Bundle\ProductBundle\Entity\Product;

class CustomviewExtension extends \Twig_Extension
{

    /** @var ContainerInterface */
    protected $container;
    protected $productFormAvailabilityProvider;

    /** @var array */
    protected $configCache = [];

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container,ProductFormAvailabilityProvider $productFormAvailabilityProvider)
    {
        $this->container = $container;
        $this->productFormAvailabilityProvider = $productFormAvailabilityProvider;
    }

    /**
     * @return EventDispatcherInterface
     */
    protected function getDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }
    /**
     * @return EventDispatcherInterface
     */
    protected function getEntityManager()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        return $em;
    }
    protected function getConfigurationProvider() {
        
        return $this->container->get('ibnab_customview.provider.configuration');
    }
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_customview_logo', [$this, 'getCustomviewLogo']),
            new \Twig_SimpleFunction('get_customview_logowidth', [$this, 'getCustomviewLogoWidth']),
            new \Twig_SimpleFunction('get_customview_logoheight', [$this, 'getCustomviewLogoHeight']),
            new \Twig_SimpleFunction('get_customview_bottoncolor', [$this, 'getCustomviewButtonColor']),
            new \Twig_SimpleFunction('get_customview_bottonhovercolor', [$this, 'getCustomviewButtonHoverColor']),
            new \Twig_SimpleFunction('get_customview_quickformcolor', [$this, 'getCustomviewQuickFormColor']),
            new \Twig_SimpleFunction('get_customview_quickordercolor', [$this, 'getCustomviewQuickOrderColor']),
            new \Twig_SimpleFunction('get_customview_quickquotecolor', [$this, 'getCustomviewQuickQuoteColor']),
            new \Twig_SimpleFunction('get_customview_menucolor', [$this, 'getCustomviewMenuColor']),
            new \Twig_SimpleFunction('get_customview_menuhovercolor', [$this, 'getCustomviewMenuHoverColor']),
            new \Twig_SimpleFunction('get_available_matrix_form_type', [$this, 'getAvailableMatrixFormType']),



        ];
    }
    public function getAvailableMatrixFormType(Product $product, $productView = "")
    {
        return $this->productFormAvailabilityProvider->getAvailableMatrixFormType($product,$productView);     
    }
    /**
     * @param int $id
     *
     * @return file
     */
    public function getCustomviewLogo()
    {
        return $this->getConfigurationProvider()->getLogo();      
    }
    public function getCustomviewLogoWidth()
    {
        return $this->getConfigurationProvider()->getLogoWidth();      
    }
    public function getCustomviewLogoHeight()
    {
        return $this->getConfigurationProvider()->getLogoHeight();      
    }
    public function getCustomviewButtonColor()
    {
        return $this->getConfigurationProvider()->getButtonColor();      
    }
    public function getCustomviewButtonHoverColor()
    {
        return $this->getConfigurationProvider()->getButtonHoverColor();      
    }
    public function getCustomviewQuickFormColor()
    {
        return $this->getConfigurationProvider()->getQuickFormColor();      
    }
    public function getCustomviewQuickOrderColor()
    {
        return $this->getConfigurationProvider()->getQuickOrderColor();      
    }
    public function getCustomviewQuickQuoteColor()
    {
        return $this->getConfigurationProvider()->getQuickQuoteColor();      
    }
    public function getCustomviewMenuColor()
    {
        return $this->getConfigurationProvider()->getMenuColor();      
    }
    public function getCustomviewMenuHoverColor()
    {
        return $this->getConfigurationProvider()->getMenuHoverColor();      
    }
}
