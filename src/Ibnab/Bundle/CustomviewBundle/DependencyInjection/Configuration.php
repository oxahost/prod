<?php

namespace Ibnab\Bundle\CustomviewBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;
/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ibnab_customview');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        SettingsBuilder::append(
            $rootNode,
            array(
                'favicon'   => ['value' => null],
                'placeholder'   => ['value' => null],
                'logo'   => ['value' => null],
                'theme_style'   => ['value' => 'basic'],
                'width'   => ['value' => 100],
                'height'   => ['value' => 40],
                'widget_system' => ['value' => 1],
                
                
                'color_enable'   => ['value' => 0],
                'button'   => ['value' => '#000000'],
                'button_search'   => ['value' => '#e0493e'],
                
                'mainmenu_baselink'   => ['value' => '#000'],
                'mainmenu_baselink_hover'   => ['value' => '#e0493e'],
                'mainmenu_columnlink'   => ['value' => '#626365'],
                'mainmenu_columnlink_hover'   => ['value' => '#626365'],
                'mainmenu_title'   => ['value' => '#e0493e'],
                'mainmenu_subcolumnlink'   => ['value' => '#626365'],
                'mainmenu_subcolumnlink_hover'   => ['value' => '#626365'],               
                'topnav_link'   => ['value' => '#e0493e'],               
                'top_user'   => ['value' => '#000'],
                'top_cart'   => ['value' => '#000'],
                'menu'   => ['value' => '#ffffff'],
                'menu_hover'   => ['value' => '#f5f5f5'],
                'quick_form'   => ['value' => '#e0493e'],
                'quick_quote'   => ['value' => '#e0493e'],
                'quick_order'   => ['value' => '#e0493e'],
                
                'top_createorder_color'   => ['value' => '#fff'],
                'top_createorder_background'   => ['value' => '#e0493e'],
                'top_createorder_color_hover'   => ['value' => '#fff'],
                'top_createorder_background_hover'   => ['value' => '#e0493e'],
                'top_viewcart_color'   => ['value' => '#fff'],
                'top_viewcart_background'   => ['value' => '#000'],
                'top_viewcart_color_hover'   => ['value' => '#fff'],
                'top_viewcart_background_hover'   => ['value' => '#000'],
                
                'footer_title'   => ['value' => '#e0493e'],
                'footer_link'   => ['value' => '#000'],
                'footer_link_hover'   => ['value' => '#000'],
                'footer_copyright'   => ['value' => '#e0493e'],
                
                'block_topborder'   => ['value' => '#000'],
                'block_height'   => ['value' => 2],
                
                'block_productsticker_color'   => ['value' => '#fff'],
                'block_productsticker_backgroundcolor'   => ['value' => '#e0493e'],
                'block_productquick_color'   => ['value' => '#000'],
                'block_productquick_backgroundcolor'   => ['value' => '#fff'],
                'block_productquick_color_hover'   => ['value' => '#fff'],
                'block_productquick_backgroundcolor_hover'   => ['value' => '#000'],
                'block_imagequick_color'   => ['value' => '#000'],
                'block_imagequick_backgroundcolor'   => ['value' => '#fff'],
                'block_imagequick_color_hover'   => ['value' => '#fff'],
                'block_imagequick_backgroundcolor_hover'   => ['value' => '#000'],
                'block_productsku_color'   => ['value' => '#e0493e'],
                'block_productprice_color'   => ['value' => '#0081d0'],
                'block_plusminus_color'   => ['value' => '#fff'],
                'block_plusminus_backgroundcolor'   => ['value' => '#000'],
                'block_plusminus_color_hover'   => ['value' => '#000'],
                'block_plusminus_backgroundcolor_hover'   => ['value' => '#fff'],
                'block_addtocart_color'   => ['value' => '#fff'],
                'block_addtocart_backgroundcolor'   => ['value' => '#000'],
                'block_addtocart_color_hover'   => ['value' => '#fff'],
                'block_addtocart_backgroundcolor_hover'   => ['value' => '#000'],   

                'slidernav_color'   => ['value' => '#000'],
                'slidernav_backgroundcolor'   => ['value' => '#fff'],
                'slidernav_color_hover'   => ['value' => '#fff'],
                'slidernav_backgroundcolor_hover'   => ['value' => '#000'],
                
                'slidernav_dotactive_color'   => ['value' => '#fff'],
                'slidernav_dotactive_background'   => ['value' => '#000'],

                'testimonial_starts_filledbackgroundcolor'   => ['value' => '#626365'],
                'testimonial_starts_backgroundcolor'   => ['value' => '#d7d7d7'],
                
                'button_hover'   => ['value' => '#010101'],
                'link'   => ['value' => '#e0493e'],
                'addbutton'   => ['value' => '#000000'],
                'features_categories'   => ['value' => '2,3,4,14,7,13,11,12'],
                'features_categories_enable'   => ['value' => 1],
                'features_menu_enable'   => ['value' => 1],
                'features_products_enable'   => ['value' => 1],
                'new_products_enable'   => ['value' => 1],
                'topselling_products_enable'   => ['value' => 1],
                'slider_home_enable'   => ['value' => 1],
                'rfq_customers_group'   => ['value' => []],
            )
        );
        return $treeBuilder;
    }

    
}
