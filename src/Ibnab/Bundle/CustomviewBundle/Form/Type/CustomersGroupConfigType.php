<?php

namespace Ibnab\Bundle\CustomviewBundle\Form\Type;

use Oro\Bundle\FormBundle\Form\Type\OroChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomersGroupConfigType extends AbstractType
{
    /** @var ContainerInterface */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return EventDispatcherInterface
     */
    protected function getEntityManager() {
        $em = $this->container->get('doctrine.orm.entity_manager');
        return $em;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $em = $this->getEntityManager();
        $customersGroupRepository = $em->getRepository('Oro\Bundle\CustomerBundle\Entity\CustomerGroup')->findAll();
        $customerGroupArray = [];
        foreach($customersGroupRepository as $customerGroup){
            $customerGroupArray[$customerGroup->getName()] = $customerGroup->getId();
        }
        $resolver->setDefaults([
            'choices'  => $customerGroupArray,
            'configs'  => [
            ],
            'multiple' => true
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return OroChoiceType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
