<?php

namespace Ibnab\Bundle\CustomviewBundle\Provider;
use Oro\Bundle\AttachmentBundle\Entity\File;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\LayoutBundle\Provider\CustomImageFilterProviderInterface;
use Oro\Bundle\ProductBundle\DependencyInjection\Configuration;
class ConfigurationProvider
{
    const Logo_FIELD = 'ibnab_customview.logo';
    const Favicon_FIELD = 'ibnab_customview.favicon';
    const UseWidget_FIELD = 'ibnab_customview.widget_system';
    const ThemeStyle_FIELD = 'ibnab_customview.theme_style';
    const Placeholder_FIELD = 'ibnab_customview.placeholder';
    const Logowidth_FIELD = 'ibnab_customview.width';
    const LogoHeight_FIELD = 'ibnab_customview.height';   
    const Link_FIELD = 'ibnab_customview.link';
    const Button_FIELD = 'ibnab_customview.button';
    const IncludeCSS_FIELD = 'ibnab_customview.color_enable';
    const AddButton_FIELD = 'ibnab_customview.addbutton';
    const Buttonhover_FIELD = 'ibnab_customview.button_hover';
    const Quickform_FIELD = 'ibnab_customview.quick_form';
    const Quickquote_FIELD = 'ibnab_customview.quick_quote';
    const Quickorder_FIELD = 'ibnab_customview.quick_order';
    const Menu_FIELD = 'ibnab_customview.menu';
    const Menuhover_FIELD = 'ibnab_customview.menu_hover';
    const FC_FIELD = 'ibnab_customview.features_categories';
    const IFM_FIELD = 'ibnab_customview.features_menu_enable';
    const IFC_FIELD = 'ibnab_customview.features_categories_enable';
    const IFP_FIELD = 'ibnab_customview.features_products_enable';
    const INP_FIELD = 'ibnab_customview.new_products_enable';
    const ITP_FIELD = 'ibnab_customview.topselling_products_enable';
    const RFQCG_FIELD = 'ibnab_customview.rfq_customers_group';
    const SH_FIELD = 'ibnab_customview.slider_home_enable';
    const FT_FIELD = 'oro_frontend.frontend_theme';
    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager, DoctrineHelper $doctrineHelper, $attachmentDir)
    {
        $this->configManager = $configManager;
        $this->doctrineHelper = $doctrineHelper;
        $this->attachmentDir = $attachmentDir;
    }

    /**
     * @return string
     */
    public function getLogo()
    {       
        $imageId = $this->configManager->get(self::Logo_FIELD);
        $image = null;
        if ($imageId && $image = $this->doctrineHelper->getEntityRepositoryForClass(File::class)->find($imageId)) {
            /** @var File $image */
            return $image; 
        }
        return $image;       
    }
    public function getAttachmentDir(){
        return $this->attachmentDir;
    }
    /**
     * @return string
     */
    public function getFavicon()
    {       
        $fileId = $this->configManager->get(self::Favicon_FIELD);
        $file = null;
        $em = $this->doctrineHelper->getEntityManager(File::class);
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT filename FROM oro_attachment_file WHERE id = :id");
        $statement->bindValue('id', $fileId);
        $statement->execute();
        $results = $statement->fetchAll();
        if ($fileId && isset($results[0]['filename'])) {
            /** @var File $image */
            return $results[0]['filename'];
        }
        return $file;     
    }
    /**
     * @return string
     */
    public function getPlaceholder()
    {       
        $fileId = $this->configManager->get(self::Placeholder_FIELD);
        $file = null;
        if ($fileId && $file = $this->doctrineHelper->getEntityRepositoryForClass(File::class)->find($fileId)) {
            /** @var File $image */
            return $file->getFilename(); 
        }
        return $file;       
    }
    /**
     * @return string
     */
    public function getLogoWidth()
    {
        return $this->configManager->get(self::Logowidth_FIELD);
    }
    /**
     * @return string
     */
    public function isUseWidget()
    {
        return $this->configManager->get(self::UseWidget_FIELD);
    }
    /**
     * @return string
     */
    public function getLogoHeight()
    {
        return $this->configManager->get(self::LogoHeight_FIELD);
    }
    /**
     * @return string
     */
    public function getButtonColor()
    {
        return $this->configManager->get(self::Button_FIELD);
    }
    /**
     * @return string
     */
    public function getAddButtonColor()
    {
        return $this->configManager->get(self::AddButton_FIELD);
    }
    /**
     * @return string
     */
    public function getLinkColor()
    {
        return $this->configManager->get(self::Link_FIELD);
    }
    /**
     * @return string
     */
    public function getButtonHoverColor()
    {
        return $this->configManager->get(self::Buttonhover_FIELD);
    }
    /**
     * @return string
     */
    public function getQuickFormColor()
    {
        return $this->configManager->get(self::Quickform_FIELD);
    }
    /**
     * @return string
     */
    public function getQuickOrderColor()
    {
        return $this->configManager->get(self::Quickorder_FIELD);
    }
    /**
     * @return string
     */
    public function getQuickQuoteColor()
    {
        return $this->configManager->get(self::Quickquote_FIELD);
    }
    /**
     * @return string
     */
    public function getMenuColor()
    {
        return $this->configManager->get(self::Menu_FIELD);
    }
    /**
     * @return string
     */
    public function getMenuHoverColor()
    {
        return $this->configManager->get(self::Menuhover_FIELD);
    }
    /**
     * @return string
     */
    public function getFC()
    {
        return $this->configManager->get(self::FC_FIELD);
    }
    /**
     * @return string
     */
    public function getIncludeCSS()
    {
        return $this->configManager->get(self::IncludeCSS_FIELD);
    }
    /**
     * @return string
     */
    public function getThemeStyle()
    {
        return $this->configManager->get(self::ThemeStyle_FIELD);
    }
    /**
     * @return string
     */
    public function getIFM()
    {
        return $this->configManager->get(self::IFM_FIELD);
    }
    /**
     * @return int
     */
    public function getIFC()
    {
        return $this->configManager->get(self::IFC_FIELD);
    }
    /**
     * @return int
     */
    public function getIFP()
    {
        return $this->configManager->get(self::IFP_FIELD);
    }
    /**
     * @return int
     */
    public function getINP()
    {
        return $this->configManager->get(self::INP_FIELD);
    }
    /**
     * @return int
     */
    public function getITP()
    {
        return $this->configManager->get(self::ITP_FIELD);
    }
    /**
     * @return int
     */
    public function getSH()
    {
        return $this->configManager->get(self::SH_FIELD);
    }
    /**
     * @return int
     */
    public function getRFQCG()
    {
        return $this->configManager->get(self::RFQCG_FIELD);
    }
    /**
     * @return int
     */
    public function getFT()
    {
        return $this->configManager->get(self::FT_FIELD);
    }
    /**
     * @return string
     */
    public function getConfig($name)
    {
        return $this->configManager->get($name);
    }
}
