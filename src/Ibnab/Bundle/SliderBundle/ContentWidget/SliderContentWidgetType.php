<?php
namespace Ibnab\Bundle\SliderBundle\ContentWidget;

use Oro\Bundle\CMSBundle\ContentWidget\ContentWidgetTypeInterface;
use Oro\Bundle\CMSBundle\ContentWidget\AbstractContentWidgetType;
use Oro\Bundle\CMSBundle\Entity\ContentWidget;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Valid;
use Oro\Bundle\LayoutBundle\Layout\LayoutContextHolder;
use Oro\Bundle\LayoutBundle\Layout\LayoutManager;
use Twig\Environment;
use Ibnab\Bundle\SliderBundle\Form\Type\SliderSelectType;
use Ibnab\Bundle\SliderBundle\Form\Type\BannerSelectType;

class SliderContentWidgetType implements ContentWidgetTypeInterface
{
    /** @var LayoutManager */
    private $layoutManager;
    /** @var LayoutContextHolder */
    private $contextHolder;

    /**
     * @param LayoutContextHolder $contextHolder
     * @param ConfigManager       $configManager
     * @param bool                $isDebug
     */
    public function __construct(LayoutContextHolder $contextHolder,LayoutManager $layoutManager
        )
    {
        $this->contextHolder = $contextHolder;
        $this->layoutManager = $layoutManager;

    }
    public static function getName(): string
    {
        return 'ibnab_widget_easy_slider';
    }

    public function getLabel(): string
    {
        return 'ibnab.slider.widget.label';
    }
    /**
     * {@inheritdoc}
     */
    public function getBackOfficeViewSubBlocks(ContentWidget $contentWidget, Environment $twig): array
    {
        
        $data = $this->getWidgetData($contentWidget);

        return [
            [
                'title' => 'oro.cms.contentwidget.sections.slider_options.label',
                'subblocks' => [
                    [
                        'data' => [
                            $twig->render('@IbnabSlider/ContentWidget/view.html.twig', $data),
                        ]
                    ],
                ]
            ]
        ];
    }
    public function getSettingsForm(ContentWidget $contentWidget, FormFactoryInterface $formFactory): ?FormInterface
    {
        return $formFactory->create(FormType::class)
            ->add('slider', SliderSelectType::class, array(
                    'required' => false,
                    'label' => 'ibnab.slider.entity_label',
                    'create_enabled' => true,
            )
           )->add('banner', BannerSelectType::class, array(
                    'required' => false,
                    'label' => 'ibnab.slider.banner.entity_label',
                    'create_enabled' => true,
            )
           )->add('isBanner', CheckboxType::class, array(
                    'label' => 'ibnab.slider.isbanner.label',
                    'required' => false,
                    'constraints' => [
                        new Type('boolean'),
                    ]
                )) ;
    }

    /**
     * {@inheritdoc}
     */
    public function getWidgetData(ContentWidget $contentWidget): array
    {
        $context = $this->contextHolder->getContext();
        $settings = $contentWidget->getSettings();
        return array_merge(
            $settings,
            [ ]         
        );
    }

    /**
     * {@inheritdoc}
     */
    public function isInline(): bool
    {
        return false;
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultTemplate(ContentWidget $contentWidget, Environment $twig): string
    {
        return "";
    }
}
