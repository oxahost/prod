<?php

namespace Ibnab\Bundle\SliderBundle\Command;

use Oro\Bundle\BatchBundle\ORM\Query\BufferedIdentityQueryResultIterator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RebuildImagesCommand extends ContainerAwareCommand {

    const COMMAND_NAME = 'slider:image:rebuild-all';
    const BATCH_SIZE = 1000;

    /**
     * {@inheritdoc}
     */
    protected function configure() {
        $this
                ->setName(self::COMMAND_NAME)
                ->setDescription(
                        <<<DESC
Rebuild all banners cached images
DESC
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $iterator = $this->getBannersImagesIterator();
        $entitiesProcessed = 0;

        $cacheBuilder = $this->getContainer()->get('ibnab_slider.banner.cachebuilder.helper');
        foreach ($iterator as $entity) {
            $entity = $cacheBuilder->rebuild($entity);
            $this->saveEntity($entity);
            $entitiesProcessed++;
        }

        $output->writeln(sprintf('%d banner image(s) has rebuild.', $entitiesProcessed));
    }

    /**
     * @param object $entity
     */
    protected function saveEntity($entity) {
        $doctrineHelper = $this->getContainer()->get('oro_entity.doctrine_helper');
        $manager = $doctrineHelper->getEntityManager($entity);
        $manager->persist($entity);

        // flush entity with related entities
        $manager->flush();
    }

    /**
     * @return EventDispatcherInterface
     */
    protected function getEventDispatcher() {
        return $this->getContainer()->get('event_dispatcher');
    }

    /**
     * @return BufferedIdentityQueryResultIterator
     */
    protected function getBannersImagesIterator() {

        $doctrineHelper = $this->getContainer()->get('oro_entity.doctrine_helper');
        $className = $this->getContainer()->getParameter('ibnab_slider.banner.entity.class');
        $banners = $doctrineHelper->getEntityRepositoryForClass($className)->findAll(); 
        return $banners;
    }

}
