<?php

namespace Ibnab\Bundle\SliderBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Ibnab\Bundle\SliderBundle\Form\Type\SliderType;
use Ibnab\Bundle\SliderBundle\Entity\Slider;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Route("/ibnabslider/manage")
 */
class SliderController extends Controller
{
    /**
     * @Acl(
     *      id="ibnab_slider_manage_index",
     *      type="entity",
     *      class="IbnabSliderBundle:Slider",
     *      permission="VIEW"
     * )
     * @Method("GET")
     * @Route("/index", name="ibnab_slider_manage_index")
     * @Template()
     */
    public function indexAction()
    {
      return array();
    }

    /**
     * @param Slider $slider
     * @return array
     * @Acl(
     *      id="ibnab_slider_manage_view",
     *      type="entity",
     *      class="IbnabSliderBundle:Slider",
     *      permission="VIEW"
     * )
     * @Method("GET")
     * @Route("/view/{id}", name="ibnab_slider_manage_view")
     * @Template()
     */
    public function viewAction(Slider $slider)
    {
        return array();
    }
    
    /**
     * @param Slider $slider
     * @return array
     *
     * @Route("/update/{id}", name="ibnab_slider_manage_update", requirements={"id"="\d+"}))
     * @Acl(
     *      id="ibnab_slider_manage_update",
     *      type="entity",
     *      class="IbnabSliderBundle:Slider",
     *      permission="EDIT"
     * )
     * @Template("IbnabSliderBundle:Slider:update.html.twig")
     */
    public function updateAction(Slider $slider)
    {
        return $this->update($slider);
    }
    
    /**
     * @param Slider $slider
     * @return array
     *
     * @Route("/create", name="ibnab_slider_manage_create")
     * @Acl(
     *      id="ibnab_slider_manage_create",
     *      type="entity",
     *      class="IbnabSliderBundle:Slider",
     *      permission="EDIT"
     * )
     * @Template("IbnabSliderBundle:Slider:update.html.twig")
     */
    public function createAction()
    {
        return $this->update(new Slider());
    }
    /**
     * @param Slider $slider
     * @return array
     *
     * @Route("/banners/{id}", name="ibnab_slider_manage_banners", requirements={"id"="\d+"}))
     * @Acl(
     *      id="ibnab_slider_manage_banners",
     *      type="entity",
     *      class="IbnabSliderBundle:Slider",
     *      permission="EDIT"
     * )
     * @Template
     */
    public function bannersAction($id)
    {
        return ['id' => $id];
    }  
    /**
     * @param Slider $slider
     * @return array
     */
    protected function update(Slider $slider,$routeName = 'ibnab_slider_manage_update')
    {
        return $this->get('ibnab_slider.form.handler.manage')->handleUpdate(
            $slider,
            $this->createForm(SliderType::class, $slider),
            function (Slider $slider)  use ($routeName) {
                return [
                    'route' => $routeName,
                    'parameters' => ['id' => $slider->getId()]
                ];
            },
            function (Slider $slider) {
                return [
                    'route' => 'ibnab_slider_manage_view',
                    'parameters' => ['id' => $slider->getId()]
                ];
            },
            $this->get('translator')->trans('ibnab_slider.manage.saved.message')
        );
    }
    /**
     * @Route("/delete/{id}", name="ibnab_slider_manage_delete", requirements={"id"="\d+"}, defaults={"id"=0}))
     * @param int $id
     *
     * @Acl(
     *      id="ibnab_slider_manage_delete",
     *      type="entity",
     *      class="IbnabSliderBundle:Slider",
     *      permission="DELETE"
     * )
     *
     * @return Response
     */
    public function deleteAction($id) {
        $em = $this->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Slider')->find($id);
        if (!$entity) {
            return new JsonResponse($this->get('translator')->trans('ibnab_slider.manage.error.message'), 403);
        }

        $em->remove($entity);
        $em->flush();

        return new JsonResponse($this->get('translator')->trans('ibnab.slider.manage.saved.message'), 204);
    }
    /**
     * Quick edit product form
     *
     * @Route("/related-banners-update/{id}", name="ibnab_slider_manage_related_banners_update", requirements={"id"="\d+"})
     * @Template
     * @Acl(
     *      id="ibnab_slider_manage_related_banners_update",
     *      type="entity",
     *      class="IbnabSliderBundle:Slider",
     *      permission="EDIT"
     * )
     * @param Slider $slider
     *
     * @return array|RedirectResponse
     */
    public function updateRelatedItemsAction(Slider $slider)
    {

        return $this->update($slider, 'ibnab_slider_manage_related_banners_update');
    }
    /**
     * @Route(
     *     "/get-possible-banners/{id}",
     *     name="ibnab_slider_manage_possible_banners",
     *     requirements={"id"="\d+"}
     * )
     * @Template(template="IbnabSliderBundle:Slider:selectRelatedBanners.html.twig")
     *
     * @param Slider $slider
     * @return array
     */
    public function getPossibleBannersAction(Slider $slider)
    {
        return ['slider' => $slider];
    }
}
