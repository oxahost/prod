<?php

namespace Ibnab\Bundle\SliderBundle\Controller;

use Symfony\Component\Security\Core\Util\ClassUtils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Symfony\Component\Routing\RouterInterface;
use Oro\Bundle\AttachmentBundle\Entity\Attachment;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ibnab\Bundle\SliderBundle\Form\Type\BannerType;
/**
 * @Route("/ibnabslider/banner")
 */
class BannerController extends Controller
{

    /**
     * @Acl(
     *      id="ibnab_slider_banner_index",
     *      type="entity",
     *      class="IbnabSliderBundle:Banner",
     *      permission="VIEW"
     * )
     * @Method("GET")
     * @Route("/index", name="ibnab_slider_banner_index")
     * @Template()
     */
    public function indexAction()
    {
      return array();
    }

    /**
     * @param Banner $banner
     * @return array
     * @Acl(
     *      id="ibnab_slider_banner_view",
     *      type="entity",
     *      class="IbnabSliderBundle:Banner",
     *      permission="VIEW"
     * )
     * @Method("GET")
     * @Template()
     * @Route("/view/{id}", name="ibnab_slider_banner_view")
     */
    public function viewAction(Banner $banner)
    {
        return array();
    }
    
    /**
     * @param Banner $banner
     * @return array
     *
     * @Route("/update/{id}", name="ibnab_slider_banner_update", requirements={"id"="\d+"}))
     * @Acl(
     *      id="ibnab_slider_banner_update",
     *      type="entity",
     *      class="IbnabSliderBundle:Banner",
     *      permission="EDIT"
     * )
     * @Template("IbnabSliderBundle:Banner:update.html.twig")
     */
    public function updateAction(Banner $banner)
    {
        return $this->update($banner);
    }
    
    /**
     * @param Banner $banner
     * @return array
     *
     * @Route("/create", name="ibnab_slider_banner_create")
     * @Acl(
     *      id="ibnab_slider_banner_create",
     *      type="entity",
     *      class="IbnabSliderBundle:Banner",
     *      permission="EDIT"
     * )
     * @Template("IbnabSliderBundle:Banner:update.html.twig")
     */
    public function createAction()
    {
        return $this->update(new Banner());
    }
    
    /**
     * @param Banner $banner
     * @return array
     */
    protected function update(Banner $banner)
    {
        return $this->get('ibnab_slider.form.handler.banner')->handleUpdate(
            $banner,
            $this->createForm(BannerType::class, $banner),
            function (Banner $banner) {
                return [
                    'route' => 'ibnab_slider_banner_update',
                    'parameters' => ['id' => $banner->getId()]
                ];
            },
            function (Banner $banner) {
                return [
                    'route' => 'ibnab_slider_banner_view',
                    'parameters' => ['id' => $banner->getId()]
                ];
            },
            $this->get('translator')->trans('ibnab.slider.manage.saved.message')
        );
    }
    /**
     * @Route("/delete/{id}", name="ibnab_slider_banner_delete", requirements={"id"="\d+"}, defaults={"id"=0}))
     * @param int $id
     *
     * @Acl(
     *      id="ibnab_slider_banner_delete",
     *      type="entity",
     *      class="IbnabSliderBundle:Banner",
     *      permission="DELETE"
     * )
     *
     * @return Response
     */
    public function deleteAction($id) {
        $em = $this->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Banner')->find($id);
        if (!$entity) {
            return new JsonResponse($this->get('translator')->trans('ibnab_slider.manage.error.message'), 403);
        }



        $em->remove($entity);
        $em->flush();

        return new JsonResponse($this->get('translator')->trans('ibnab_slider.manage.saved.message'), 204);
    }

}
