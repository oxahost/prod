<?php

namespace Ibnab\Bundle\SliderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;

use Oro\Bundle\OrganizationBundle\Entity\Organization;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\UserBundle\Entity\User;
use Ibnab\Bundle\SliderBundle\Model\ExtendBanner;
/**
 * Slider
 *
 * @ORM\Entity
 * @ORM\Table(name="ibnab_slider_banner",
 * indexes={@ORM\Index(name="ibnab_banner_title_idx", columns={"title"}),
 *         @ORM\Index(name="ibnab_banner_active_idx", columns={"is_active_banner"})})
 * @ORM\Entity(repositoryClass="Ibnab\Bundle\SliderBundle\Entity\Repository\BannerRepository")
 * @ORM\HasLifecycleCallbacks
 * @Config(
 *      defaultValues={
 *          "ownership"={
 *              "owner_type"="USER",
 *              "owner_field_name"="owner",
 *              "owner_column_name"="user_owner_id",
 *              "organization_field_name"="organization",
 *              "organization_column_name"="organization_id"
 *          },
 *          "entity"={
 *              "icon"="icon-phone-sign"
 *          },
 *          "security"={
 *              "type"="ACL",
 *              "group_name"=""
 *          }
 *      },
 *      routeName="ibnab_slider_banner_index",
 *      routeView="ibnab_slider_banner_view",
 *      routeUpdate="ibnab_slider_banner_update"
 * )
 * @JMS\ExclusionPolicy("ALL")
 */
class Banner extends ExtendBanner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $description;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="order_banner", type="integer", nullable=true)
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $order;
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active_banner", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $isActive = true;

    /**
     * @var string
     *
     * @ORM\Column(name="url_banner", type="text", nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $url;


    /**
     * @var string
     *
     * @ORM\Column(name="target_banner", type="string", length=240, nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $target;

    /**
     * @var string
     *
     * @ORM\Column(name="img_alt_banner", type="string", length=400, nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $imgAlt;
    
    /**
     * @var string
     *
     * @ORM\Column(name="big_cache", type="string", length=600, nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    
    protected $bigCache;
   
    /**
     * @var string
     *
     * @ORM\Column(name="medium_cache", type="string", length=600, nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    
    protected $mediumCache;
   
    /**
     * @var string
     *
     * @ORM\Column(name="small_cache", type="string", length=600, nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    
    protected $smallCache;

    /**
     * @ORM\Column(name="start", type="datetime", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     *  )
     *
     * @var \DateTime|null
     */
    protected $start;
    /**
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     *  )
     *
     * @var \DateTime|null
     */
    protected $finish;
    
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_owner_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $owner;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\OrganizationBundle\Entity\Organization")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @param $name
     * @param string $content
     * @param string $type
     * @param bool $isSystem
     * @internal param $entityName
     */
    public function __construct()
    {
    }
    public function getId()
    {
        return $this->id;
    }
    /**
     * Gets owning user
     *
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Sets owning user
     *
     * @param User $owningUser
     *
     * @return PDFTemplate
     */
    public function setOwner($owningUser)
    {
        $this->owner = $owningUser;

        return $this;
    }
    /**
     * Convert entity to string
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Set organization
     *
     * @param Organization $organization
     * @return PDFTemplate
     */
    public function setOrganization(Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Banner
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set description
     *
     * @param string $description
     *
     * @return Banner
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return Banner
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    
    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Banner
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Banner
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * Set target
     *
     * @param string $target
     *
     * @return Banner
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set imgAlt
     *
     * @param string $imgAlt
     *
     * @return Banner
     */
    public function setImgAlt($imgAlt)
    {
        $this->imgAlt = $imgAlt;

        return $this;
    }

    /**
     * Get imgAlt
     *
     * @return string
     */
    public function getImgAlt()
    {
        return $this->imgAlt;
    }
    /*
     *
     * @param string $bigCache
     *
     * @return Banner
     */
    public function setBigCache($bigCache)
    {
        $this->bigCache = $bigCache;

        return $this;
    }

    /**
     * Get bigCache
     *
     * @return string
     */
    public function getBigCache()
    {
        return $this->bigCache;
    }
    /*
     *
     * @param string $mediumCache
     *
     * @return Banner
     */
    public function setMediumCache($mediumCache)
    {
        $this->mediumCache = $mediumCache;

        return $this;
    }

    /**
     * Get mediumCache
     *
     * @return string
     */
    public function getMediumCache()
    {
        return $this->mediumCache;
    }
    /*
     *
     * @param string $smallCache
     *
     * @return Banner
     */
    public function setSmallCache($smallCache)
    {
        $this->smallCache = $smallCache;

        return $this;
    }

    /**
     * Get smallCache
     *
     * @return string
     */
    public function getSmallCache()
    {
        return $this->smallCache;
    }
    /**
     * Get start
     *
     * @return string
     */
    public function getStart()
    {
        return $this->start;
    }
    
    /**
     * Set start
     *
     * @param string $start
     *
     * @return Slider
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get finish
     *
     * @return string
     */
    public function getFinish()
    {
        return $this->finish;
    }
    
    /**
     * Set finish
     *
     * @param string $finish
     *
     * @return Slider
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }
}
