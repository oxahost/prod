<?php

namespace Ibnab\Bundle\SliderBundle\Entity\RelatedBanner;

use Doctrine\ORM\Mapping as ORM;

use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Ibnab\Bundle\SliderBundle\Entity\Slider;
/**
 * @ORM\Table(
 *     name="ibnab_slider_banner_two",
 *     indexes={
 *          @ORM\Index(name="ibnab_slider_banner_two_slider_id", columns={"slider_id"}),
 *          @ORM\Index(name="ibnab_slider_banner_two_banner_id", columns={"banner_id"})
 *     },
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="IDX_Uniquesliderbannerentity_two",
 *              columns={"slider_id", "banner_id"}
 *          )
 *     }
 * )
 * @ORM\Entity(repositoryClass="Ibnab\Bundle\SliderBundle\Entity\Repository\RelatedBanner\RelatedBannerRepository")
 */
class RelatedBanner
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="Ibnab\Bundle\SliderBundle\Entity\Slider")
     * @ORM\JoinColumn(name="slider_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $slider_id;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="Ibnab\Bundle\SliderBundle\Entity\Banner")
     * @ORM\JoinColumn(name="banner_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $banner_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Slider
     */
    public function getSliderId()
    {
        return $this->slider_id;
    }

    /**
     * @param Product $slider_id
     * @return $this
     */
    public function setSliderId(Slider $slider_id)
    {
        $this->slider_id = $slider_id;

        return $this;
    }

    /**
     * @return Banner
     */
    public function getBannerId()
    {
        return $this->banner_id;
    }

    /**
     * @param Banner $banner_id
     * @return $this
     */
    public function setBannerId(Banner $banner_id)
    {
        $this->banner_id = $banner_id;

        return $this;
    }
}
