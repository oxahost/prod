<?php

namespace Ibnab\Bundle\SliderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;

use Oro\Bundle\OrganizationBundle\Entity\Organization;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\UserBundle\Entity\User;

/**
 * Slider
 *
 * @ORM\Entity
 * @ORM\Table(name="ibnab_slider_slider",
 *      indexes={@ORM\Index(name="ibnab_slider_title_idx", columns={"title"})})
 * @ORM\Entity(repositoryClass="Ibnab\Bundle\SliderBundle\Entity\Repository\SliderRepository")
 * @ORM\HasLifecycleCallbacks
 * @Config(
 *      defaultValues={
 *          "ownership"={
 *              "owner_type"="USER",
 *              "owner_field_name"="owner",
 *              "owner_column_name"="user_owner_id",
 *              "organization_field_name"="organization",
 *              "organization_column_name"="organization_id"
 *          },
 *          "entity"={
 *              "icon"="icon-phone-sign"
 *          },
 *          "security"={
 *              "type"="ACL",
 *              "group_name"=""
 *          }
 *      },
 *      routeName="ibnab_slider_manage_index",
 *      routeView="ibnab_slider_manage_view",
 *      routeUpdate="ibnab_slider_manage_update"
 * )
 * @JMS\ExclusionPolicy("ALL")
 */
class Slider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $id;
   

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showt_title", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $showtTitle = false;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="dots", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $dots = true;    

    /**
     * @var boolean
     *
     * @ORM\Column(name="draggable", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $draggable = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="infinite", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $infinite = true; 

    /**
     * @var boolean
     *
     * @ORM\Column(name="focus_onSelect", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $focusOnSelect = false;  
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="mobile_first", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $mobileFirst = false; 
 
    /**
     * @var boolean
     *
     * @ORM\Column(name="pause_onFocus", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $pauseOnFocus = true; 

    /**
     * @var boolean
     *
     * @ORM\Column(name="pause_onHover", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $pauseOnHover  = true; 
 
    /**
     * @var boolean
     *
     * @ORM\Column(name="pause_onDotsHover", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $pauseOnDotsHover = false; 

    /**
     * @var boolean
     *
     * @ORM\Column(name="arrows", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $arrows  = true; 
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="variable_width", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $variableWidth = false;
 
    /**
     * @var boolean
     *
     * @ORM\Column(name="center_mode", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $centerMode = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="autoplay", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $autoplay = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="swipe", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $swipe = false;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="touch_move", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $touchMove = true;
 
    /**
     * @var boolean
     *
     * @ORM\Column(name="vertical_swiping", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $verticalSwiping = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rtl", type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $rtl = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="autoplay_speed", type="integer", nullable=true)
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $autoplaySpeed = 3000;

    /**
     * @var integer
     *
     * @ORM\Column(name="slides_perRow", type="integer", nullable=true)
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $slidesPerRow = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="initial_slide", type="integer", nullable=true)
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $initialSlide = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="speed", type="integer", nullable=true)
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $speed = 300;

    /**
     * @var integer
     *
     * @ORM\Column(name="slides_toShow", type="integer", nullable=true)
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $slidesToShow = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="slides_toScroll", type="integer", nullable=true)
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $slidesToScroll = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="center_padding", type="integer", nullable=true)
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    protected $centerPadding = 50;

    /**
     * @var string
     *
     * @ORM\Column(name="easing", type="string", length=45, nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $easing = 'linear';
    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=200, nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $location = '';
    /**
     * @var string
     *
     * @ORM\Column(name="respond_to", type="string", length=45, nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $respondTo = 'window';

    /**
     * @var string
     *
     * @ORM\Column(name="responsive", type="text", nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $responsive;
    
    /**
     * @var string
     *
     * @ORM\Column(name="lazy_load", type="string", length=20, nullable=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $lazy_load = 'ondemand';


    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_owner_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $owner;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\OrganizationBundle\Entity\Organization")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $organization;


    public function __construct()
    {
    }
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets owning user
     *
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Sets owning user
     *
     * @param User $owningUser
     *
     * @return PDFTemplate
     */
    public function setOwner($owningUser)
    {
        $this->owner = $owningUser;

        return $this;
    }
    /**
     * Convert entity to string
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Set organization
     *
     * @param Organization $organization
     * @return PDFTemplate
     */
    public function setOrganization(Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Slider
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set showtTitle
     *
     * @param boolean $showtTitle
     *
     * @return Slider
     */
    public function setShowtTitle($showtTitle)
    {
        $this->showtTitle = $showtTitle;

        return $this;
    }

    /**
     * Get showtTitle
     *
     * @return boolean
     */
    public function getShowtTitle()
    {
        return $this->showtTitle;
    }

    /**
     * Set location
     *
     * @param boolean $showtTitle
     *
     * @return Slider
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }
    
    /**
     * Set dots
     *
     * @param boolean $dots
     *
     * @return Slider
     */
    public function setDots($dots)
    {
        $this->dots = $dots;

        return $this;
    }

    /**
     * Get dots
     *
     * @return boolean
     */
    public function getDots()
    {
        return $this->dots;
    }

    /**
     * Set draggable
     *
     * @param boolean $draggable
     *
     * @return Slider
     */
    public function setDraggable($draggable)
    {
        $this->draggable = $draggable;

        return $this;
    }

    /**
     * Get draggable
     *
     * @return boolean
     */
    public function getDraggable()
    {
        return $this->draggable;
    }

    /**
     * Set infinite
     *
     * @param boolean $infinite
     *
     * @return Slider
     */
    public function setInfinite($infinite)
    {
        $this->infinite = $infinite;

        return $this;
    }

    /**
     * Get infinite
     *
     * @return boolean
     */
    public function getInfinite()
    {
        return $this->infinite;
    }

    /**
     * Set focusOnSelect
     *
     * @param boolean $focusOnSelect
     *
     * @return Slider
     */
    public function setFocusOnSelect($focusOnSelect)
    {
        $this->focusOnSelect = $focusOnSelect;

        return $this;
    }

    /**
     * Get focusOnSelect
     *
     * @return boolean
     */
    public function getFocusOnSelect()
    {
        return $this->focusOnSelect;
    }

    /**
     * Set mobileFirst
     *
     * @param boolean $mobileFirst
     *
     * @return Slider
     */
    public function setMobileFirst($mobileFirst)
    {
        $this->mobileFirst = $mobileFirst;

        return $this;
    }

    /**
     * Get mobileFirst
     *
     * @return boolean
     */
    public function getMobileFirst()
    {
        return $this->mobileFirst;
    }

    /**
     * Set pauseOnFocus
     *
     * @param boolean $pauseOnFocus
     *
     * @return Slider
     */
    public function setPauseOnFocus($pauseOnFocus)
    {
        $this->pauseOnFocus = $pauseOnFocus;

        return $this;
    }

    /**
     * Get pauseOnFocus
     *
     * @return boolean
     */
    public function getPauseOnFocus()
    {
        return $this->pauseOnFocus;
    }

    /**
     * Set pauseOnHover
     *
     * @param boolean $pauseOnHover
     *
     * @return Slider
     */
    public function setPauseOnHover($pauseOnHover)
    {
        $this->pauseOnHover = $pauseOnHover;

        return $this;
    }

    /**
     * Get pauseOnHover
     *
     * @return boolean
     */
    public function getPauseOnHover()
    {
        return $this->pauseOnHover;
    }

    /**
     * Set pauseOnDotsHover
     *
     * @param boolean $pauseOnDotsHover
     *
     * @return Slider
     */
    public function setPauseOnDotsHover($pauseOnDotsHover)
    {
        $this->pauseOnDotsHover = $pauseOnDotsHover;

        return $this;
    }

    /**
     * Get pauseOnDotsHover
     *
     * @return boolean
     */
    public function getPauseOnDotsHover()
    {
        return $this->pauseOnDotsHover;
    }

    /**
     * Set arrows
     *
     * @param boolean $arrows
     *
     * @return Slider
     */
    public function setArrows($arrows)
    {
        $this->arrows = $arrows;

        return $this;
    }

    /**
     * Get arrows
     *
     * @return boolean
     */
    public function getArrows()
    {
        return $this->arrows;
    }

    /**
     * Set variableWidth
     *
     * @param boolean $variableWidth
     *
     * @return Slider
     */
    public function setVariableWidth($variableWidth)
    {
        $this->variableWidth = $variableWidth;

        return $this;
    }

    /**
     * Get variableWidth
     *
     * @return boolean
     */
    public function getVariableWidth()
    {
        return $this->variableWidth;
    }

    /**
     * Set centerMode
     *
     * @param boolean $centerMode
     *
     * @return Slider
     */
    public function setCenterMode($centerMode)
    {
        $this->centerMode = $centerMode;

        return $this;
    }

    /**
     * Get centerMode
     *
     * @return boolean
     */
    public function getCenterMode()
    {
        return $this->centerMode;
    }

    /**
     * Set autoplay
     *
     * @param boolean $autoplay
     *
     * @return Slider
     */
    public function setAutoplay($autoplay)
    {
        $this->autoplay = $autoplay;

        return $this;
    }

    /**
     * Get autoplay
     *
     * @return boolean
     */
    public function getAutoplay()
    {
        return $this->autoplay;
    }

    /**
     * Set swipe
     *
     * @param boolean $swipe
     *
     * @return Slider
     */
    public function setSwipe($swipe)
    {
        $this->swipe = $swipe;

        return $this;
    }

    /**
     * Get swipe
     *
     * @return boolean
     */
    public function getSwipe()
    {
        return $this->swipe;
    }

    /**
     * Set touchMove
     *
     * @param boolean $touchMove
     *
     * @return Slider
     */
    public function setTouchMove($touchMove)
    {
        $this->touchMove = $touchMove;

        return $this;
    }

    /**
     * Get touchMove
     *
     * @return boolean
     */
    public function getTouchMove()
    {
        return $this->touchMove;
    }

    /**
     * Set verticalSwiping
     *
     * @param boolean $verticalSwiping
     *
     * @return Slider
     */
    public function setVerticalSwiping($verticalSwiping)
    {
        $this->verticalSwiping = $verticalSwiping;

        return $this;
    }

    /**
     * Get verticalSwiping
     *
     * @return boolean
     */
    public function getVerticalSwiping()
    {
        return $this->verticalSwiping;
    }

    /**
     * Set rtl
     *
     * @param boolean $rtl
     *
     * @return Slider
     */
    public function setRtl($rtl)
    {
        $this->rtl = $rtl;

        return $this;
    }

    /**
     * Get rtl
     *
     * @return boolean
     */
    public function getRtl()
    {
        return $this->rtl;
    }

    /**
     * Set autoplaySpeed
     *
     * @param integer $autoplaySpeed
     *
     * @return Slider
     */
    public function setAutoplaySpeed($autoplaySpeed)
    {
        $this->autoplaySpeed = $autoplaySpeed;

        return $this;
    }

    /**
     * Get autoplaySpeed
     *
     * @return integer
     */
    public function getAutoplaySpeed()
    {
        return $this->autoplaySpeed;
    }

    /**
     * Set slidesPerRow
     *
     * @param integer $slidesPerRow
     *
     * @return Slider
     */
    public function setSlidesPerRow($slidesPerRow)
    {
        $this->slidesPerRow = $slidesPerRow;

        return $this;
    }

    /**
     * Get slidesPerRow
     *
     * @return integer
     */
    public function getSlidesPerRow()
    {
        return $this->slidesPerRow;
    }

    /**
     * Set initialSlide
     *
     * @param integer $initialSlide
     *
     * @return Slider
     */
    public function setInitialSlide($initialSlide)
    {
        $this->initialSlide = $initialSlide;

        return $this;
    }

    /**
     * Get initialSlide
     *
     * @return integer
     */
    public function getInitialSlide()
    {
        return $this->initialSlide;
    }

    /**
     * Set speed
     *
     * @param integer $speed
     *
     * @return Slider
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return integer
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set slidesToShow
     *
     * @param integer $slidesToShow
     *
     * @return Slider
     */
    public function setSlidesToShow($slidesToShow)
    {
        $this->slidesToShow = $slidesToShow;

        return $this;
    }

    /**
     * Get slidesToShow
     *
     * @return integer
     */
    public function getSlidesToShow()
    {
        return $this->slidesToShow;
    }

    /**
     * Set slidesToScroll
     *
     * @param integer $slidesToScroll
     *
     * @return Slider
     */
    public function setSlidesToScroll($slidesToScroll)
    {
        $this->slidesToScroll = $slidesToScroll;

        return $this;
    }

    /**
     * Get slidesToScroll
     *
     * @return integer
     */
    public function getSlidesToScroll()
    {
        return $this->slidesToScroll;
    }

    /**
     * Set centerPadding
     *
     * @param integer $centerPadding
     *
     * @return Slider
     */
    public function setCenterPadding($centerPadding)
    {
        $this->centerPadding = $centerPadding;

        return $this;
    }

    /**
     * Get centerPadding
     *
     * @return integer
     */
    public function getCenterPadding()
    {
        return $this->centerPadding;
    }

    /**
     * Set easing
     *
     * @param string $easing
     *
     * @return Slider
     */
    public function setEasing($easing)
    {
        $this->easing = $easing;

        return $this;
    }

    /**
     * Get easing
     *
     * @return string
     */
    public function getEasing()
    {
        return $this->easing;
    }

    /**
     * Set respondTo
     *
     * @param string $respondTo
     *
     * @return Slider
     */
    public function setRespondTo($respondTo)
    {
        $this->respondTo = $respondTo;

        return $this;
    }

    /**
     * Get respondTo
     *
     * @return string
     */
    public function getRespondTo()
    {
        return $this->respondTo;
    }

    /**
     * Set responsive
     *
     * @param string $responsive
     *
     * @return Slider
     */
    public function setResponsive($responsive)
    {
        $this->responsive = $responsive;

        return $this;
    }

    /**
     * Get responsive
     *
     * @return string
     */
    public function getResponsive()
    {
        return $this->responsive;
    }

    /**
     * Set lazyLoad
     *
     * @param string $lazyLoad
     *
     * @return Slider
     */
    public function setLazyLoad($lazyLoad)
    {
        $this->lazy_load = $lazyLoad;

        return $this;
    }
    /**
     * Get lazyLoad
     *
     * @return string
     */
    public function getLazyLoad()
    {
        return $this->lazy_load;
    }
    

}
