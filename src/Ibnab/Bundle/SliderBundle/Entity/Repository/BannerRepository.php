<?php

namespace Ibnab\Bundle\SliderBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Oro\Bundle\OrganizationBundle\Entity\Organization;

class BannerRepository extends EntityRepository
{
    public function getBanners($ids)
    {
        $qb = $this->createQueryBuilder('ibnab_slider_banner');
 
        $qb->select('banners')
       ->from('IbnabSliderBundle:Banner', 'banners')
       ->where('(banners.start < :datestart) or (banners.start is NULL)')
       ->andWhere('(banners.finish > :datefinish) or (banners.finish is NULL)')
       ->andWhere('banners.isActive = true')
       ->andWhere("banners.id IN(:bannerIds)")
         ->setParameter('datestart', new \Datetime(date('d-m-Y')))
         ->setParameter('datefinish', new \Datetime(date('d-m-Y')))
         ->setParameter('bannerIds', array_values($ids))
       ->orderBy('banners.order', 'DESC');
 
        return $qb->getQuery()
               ->getResult();
    }
}
