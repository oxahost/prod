<?php

namespace Ibnab\Bundle\SliderBundle\Entity\Repository\RelatedBanner;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Ibnab\Bundle\SliderBundle\Entity\Slider;
use Ibnab\Bundle\SliderBundle\Entity\RelatedBanner\RelatedBanner;

class RelatedBannerRepository extends EntityRepository
{
    /**
     * @param Banner|int $bannerFrom
     * @param Banner|int $bannerTo
     * @return bool
     */
    public function exists($bannerFrom, $bannerTo)
    {
        return null !== $this->findOneBy(['slider_id' => $bannerFrom, 'banner_id' => $bannerTo]);
    }

    /**
     * @param int $id
     * @return int
     */
    public function countRelationsForBanner($id)
    {
        return (int) $this->createQueryBuilder('ibnab_slider_banner_two')
            ->select('COUNT(ibnab_slider_banner_two.id)')
            ->where('ibnab_slider_banner_two.slider_id = :id')
            ->setParameter(':id', $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param int $id
     * @return Banner[]
     */
    public function findRelated($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->from(RelatedBanner::class, 'rb')
            ->select('rb')
            ->where('rb.slider_id = :id')
            ->setParameter(':id', $id)
            ->orderBy('rb.id');

        return $qb->getQuery()->execute();
    }
}
