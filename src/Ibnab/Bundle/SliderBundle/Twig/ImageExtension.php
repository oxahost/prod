<?php

namespace Ibnab\Bundle\SliderBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class ImageExtension extends \Twig_Extension
{

    /** @var ContainerInterface */
    protected $container;

    /** @var array */
    protected $configCache = [];

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return EventDispatcherInterface
     */
    protected function getDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }
    /**
     * @return EventDispatcherInterface
     */
    protected function getEntityManager()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        return $em;
    }
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_image_banner', [$this, 'getImageBanner']),
            new \Twig_SimpleFunction('get_related_banners', [$this, 'getRelatedBanners']),
            new \Twig_SimpleFunction('get_banners', [$this, 'getBanners']),
            new \Twig_SimpleFunction('get_slider_by_locations', [$this, 'getSliderByLocation']),
            new \Twig_SimpleFunction('get_slider', [$this, 'getSlider'])
        ];
    }
    /**
     * @param string $location
     *
     * @return slider collection
     */
    public function getSliderByLocation($location)
    {
        $em = $this->getEntityManager();
        $banner = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Slider')->findBy(array('location' => $location));
        return $banner;
        
    }
    /**
     * @param int $id
     *
     * @return file
     */
    public function getImageBanner($id)
    {
        $em = $this->getEntityManager();
        $banner = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Banner')->find($id);
        return $banner;
        
    }
    /**
     * @param int $id
     *
     * @return file
     */
    public function getRelatedBanners($id)
    {
        $em = $this->getEntityManager();
        $related = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\RelatedBanner\RelatedBanner')->findRelated($id);

        $ids = [];
        foreach ($related as $relatedBanner) {
            $ids[] = $relatedBanner->getBannerId()->getId();
        }     
        return $ids;
        
    }
    /**
     * @param int $id
     *
     * @return Slider
     */
    public function getSlider($id)
    {
        $em = $this->getEntityManager();
        $slider = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Slider')->find($id);      
        return $slider;
        
    } 
    /**
     * @param int $id
     *
     * @return collection banner
     */
    public function getBanners($id)
    {
        $em = $this->getEntityManager();
        $banner_ids = $this->getRelatedBanners($id);
        //$banners= $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Banner')->findby( array('id' => $banner_ids,'isActive' => 1), array('order' => 'DESC'));      
        $banners = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Banner')->getBanners($banner_ids);      
        return $banners;
        
    }    
}
