<?php

namespace Ibnab\Bundle\SliderBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraint;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Oro\Bundle\AttachmentBundle\Form\Type\ImageType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Oro\Bundle\FormBundle\Form\Type\OroDateTimeType;


class BannerType extends AbstractType {

    const NAME = 'ibnab_slider_form_banner';
    /**
     * @var string
     */
    protected $bannerClassName;

    public function __construct($bannerClassName) {
        $this->bannerClassName = $bannerClassName;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', TextType::class, [
                    'label' => 'ibnab.slider.banner.title_label',
                    'required' => true
                        ]
                )
                ->add('description', TextareaType::class, [
                    'label' => 'ibnab.slider.banner.description_label',
                    'required' => false
                        ]
                )
                ->add('order', TextType::class, [
                    'label' => 'ibnab.slider.banner.order_label',
                    'required' => false
                        ]
                )->add('start',OroDateTimeType::class,
                [
                    'required' => false,
                    'label' => 'ibnab.slider.manage.start_label',
                ]
                )->add('finish',OroDateTimeType::class,
                [
                    'required' => false,
                    'label' => 'ibnab.slider.manage.finish_label',
                ]
                )->add('isActive', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.banner.isActive_label'])
                ->add('url', TextType::class, [
                    'label' => 'ibnab.slider.banner.url_label',
                    'required' => false
                ])
                ->add('imageSlider', ImageType::class, [
                    'label' => 'ibnab.slider.banner.image_label',
                    'required' => false
                ])
                ->add('imageSliderMed', ImageType::class, [
                    'label' => 'ibnab.slider.banner.imageMed_label',
                    'required' => false
                ])
                ->add('imageSliderSmall', ImageType::class, [
                    'label' => 'ibnab.slider.banner.imageSmall_label',
                    'required' => false
                ])
                ->add('target', ChoiceType::class, array(
                    'label' => 'ibnab.slider.banner.target_label',
                    'multiple' => false,
                    'choices' => array(
                        'ibnab.slider.banner.target.blank_label' => '_self',
                        'ibnab.slider.banner.target.self_label' => '_blank'
                    ),
                    'required' => true
                ))
                ->add('imgAlt', TextType::class, [
                    'label' => 'ibnab.slider.banner.imgAlt_label',
                    'required' => false
                ])
                ->add('owner')
                ->add('organization');
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => $this->bannerClassName,
            'intention' => 'ibnab_banner',
            'cascade_validation' => true,
            'validation_groups' => [Constraint::DEFAULT_GROUP, 'form']
        ));
    }


    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return self::NAME;
    }

}
