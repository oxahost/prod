<?php

namespace Ibnab\Bundle\SliderBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Length;
use Ibnab\Bundle\SliderBundle\Entity\Slider;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Oro\Bundle\FormBundle\Form\Type\OroDateTimeType;

class SliderType extends AbstractType {

    const NAME = 'ibnab_slider_form_manage';
    const SLIDER_LOCATION_DEFAULT = "";
    const HOMEPAGE_CONTENT_TOP = 'homepage-content-top';
    const HOMEPAGE_CONTENT_BOTTOM = 'homepage-content-bottom';
    const CONTENT_TOP = 'content-top';
    const CONTENT_BOTTOM = 'content-bottom';
    const SIDEBAR_ADDITIONAL_TOP = 'sidebar';
    const CATEGORY_CONTENT_TOP = 'category-content-top';
    const CATEGORY_CONTENT_BOTTOM = 'category-content-bottom';
    const CATEGORY_SIDEBAR_ADDITIONAL_TOP = 'category-sidebar';
    const PRODUCT_CONTENT_TOP = 'product-content-top';
    const PRODUCT_CONTENT_BOTTOM = 'product-content-bottom';
    const PRODUCT_SIDEBAR_ADDITIONAL_TOP = 'product-sidebar';
    const CART_CONTENT_TOP = 'cart-content-top';
    const CART_CONTENT_BOTTOM = 'cart-content-bottom';
    const CART_CONTENT_SIDEBAR = 'cart-sidebar';
    const CHECKOUT_CONTENT_TOP = 'checkout-content-top';
    const CHECKOUT_CONTENT_BOTTOM = 'checkout-content-bottom';
    const CHECKOUT_CONTENT_SIDEBAR = 'checkout-sidebar';
    const CUSTOMER_CONTENT_TOP = 'customer-content-top';
    const CUSTOMER_CONTENT_BOTTOM = 'customer-content-bottom';
    const CUSTOMER_SIDEBAR_ADDITIONAL_TOP = 'customer-sidebar';
    const HEADER_BOTTOM = 'header-bottom';
    /**
     * @var string
     */
    protected $sliderClassName;

    public function __construct($sliderClassName) {
        $this->sliderClassName = $sliderClassName;
    }
    public static function getSliderGridLocations()
    {
        return array_flip([
            self::SLIDER_LOCATION_DEFAULT => '--- No Location ---',
            self::HEADER_BOTTOM => 'Header Bottom',
            self::HOMEPAGE_CONTENT_TOP => 'Homepage Content Top',
            self::HOMEPAGE_CONTENT_BOTTOM => 'Homepage Content Bottom',
            self::CONTENT_TOP => 'Content Top',
            self::CONTENT_BOTTOM => 'Content Bottom',
            self::SIDEBAR_ADDITIONAL_TOP => 'Content Sidebar',
            self::CATEGORY_CONTENT_TOP => 'Category Content Top',
            self::CATEGORY_CONTENT_BOTTOM => 'Category Content Bottom',
            self::CATEGORY_SIDEBAR_ADDITIONAL_TOP => 'Category Sidebar',
            self::PRODUCT_CONTENT_TOP => 'Product Content Top',
            self::PRODUCT_CONTENT_BOTTOM => 'Product Content Bottom',
            self::PRODUCT_SIDEBAR_ADDITIONAL_TOP => 'Product Sidebar',
            self::CART_CONTENT_TOP => 'Shopping List Top',
            self::CART_CONTENT_BOTTOM => 'Shopping List Bottom',
            self::CART_CONTENT_SIDEBAR => 'Shopping List Sidebar',
            self::CHECKOUT_CONTENT_TOP => 'Checkout Content Top',
            self::CHECKOUT_CONTENT_BOTTOM => 'Checkout Content Bottom',
            self::CHECKOUT_CONTENT_SIDEBAR => 'Checkout Content Sidebar',
            self::CUSTOMER_CONTENT_TOP => 'Customer Content Top',
            self::CUSTOMER_CONTENT_BOTTOM => 'Customer Content Bottom',
            self::CUSTOMER_SIDEBAR_ADDITIONAL_TOP => 'Customer Sidebar',
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', TextType::class, [
                    'label' => 'ibnab.slider.manage.title_label',
                    'required' => true
                        ]
                )->add('showtTitle', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.showtTitle_label'])
                ->add('location', ChoiceType::class, array(
                    'label' => 'ibnab.slider.manage.location_label',
                    'multiple' => false,
                    'choices' => $this->getSliderGridLocations(),
                    'required' => true
                ))->add('dots', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.dots_label'])
                ->add('draggable', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.draggable_label'])
                ->add('infinite', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.infinite_label'])
                ->add('focusOnSelect', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.focusOnSelect_label'])
                ->add('mobileFirst', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.mobileFirst_label'])
                ->add('pauseOnFocus', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.pauseOnFocus_label'])
                ->add('pauseOnHover', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.pauseOnHover_label'])
                ->add('pauseOnDotsHover', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.pauseOnDotsHover_label'])
                ->add('arrows', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.arrows_label'])
                ->add('variableWidth', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.variableWidth_label'])
                ->add('centerMode', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.centerMode_label'])
                ->add('autoplay', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.autoplay_label'])
                ->add('swipe', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.swipe_label'])
                ->add('touchMove', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.touchMove_label'])
                ->add('verticalSwiping', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.verticalSwiping_label'])
                ->add('rtl', CheckboxType::class, ['required' => false, 'label' => 'ibnab.slider.manage.rtl_label'])
                ->add('autoplaySpeed', TextType::class, [
                    'label' => 'ibnab.slider.manage.autoplaySpeed_label',
                    'required' => false
                ])
                ->add('slidesPerRow', TextType::class, [
                    'label' => 'ibnab.slider.manage.slidesPerRow_label',
                    'required' => false
                ])
                ->add('initialSlide', TextType::class, [
                    'label' => 'ibnab.slider.manage.initialSlide_label',
                    'required' => false
                ])
                ->add('speed', TextType::class, [
                    'label' => 'ibnab.slider.manage.speed_label',
                    'required' => false
                ])
                ->add('slidesToShow', TextType::class, [
                    'label' => 'ibnab.slider.manage.slidesToShow_label',
                    'required' => false
                ])
                ->add('slidesToScroll', TextType::class, [
                    'label' => 'ibnab.slider.manage.slidesToScroll_label',
                    'required' => false
                ])
                ->add('centerPadding', TextType::class, [
                    'label' => 'ibnab.slider.manage.centerPadding_label',
                    'required' => false
                ])
                ->add('easing', ChoiceType::class, array(
                    'label' => 'ibnab.slider.manage.easing_label',
                    'multiple' => false,
                    'choices' => array(
                        'ibnab.slider.manage.easing.linear_label' => 'linear'
                    ),
                    'required' => true
                ))
                ->add('respondTo', ChoiceType::class, array(
                    'label' => 'ibnab.slider.manage.respondTo_label',
                    'multiple' => false,
                    'choices' => array(
                        'ibnab.slider.manage.respondTo.window_label' => 'window'
                    ),
                    'required' => true
                ))
                ->add('responsive', TextareaType::class, array(
                    'label' => 'ibnab.slider.manage.responsive_label',
                    'required' => false
                ))
                ->add('lazy_load', ChoiceType::class, array(
                    'label' => 'ibnab.slider.manage.lazyLoad_label',
                    'multiple' => false,
                    'choices' => array(
                        'ibnab.slider.manage.lazyLoad.ondemand_label' => 'ondemand'
                    ),
                    'required' => true
                ))
                ->add('owner')
                ->add('organization');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => $this->sliderClassName,
            'intention' => 'ibnab_slider',
            'cascade_validation' => true,
            'validation_groups' => [Constraint::DEFAULT_GROUP, 'form']
        ));
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return self::NAME;
    }

}
