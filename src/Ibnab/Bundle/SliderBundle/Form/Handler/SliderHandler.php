<?php

namespace Ibnab\Bundle\SliderBundle\Form\Handler;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormError;
use Oro\Bundle\FormBundle\Model\UpdateHandler;
use Ibnab\Bundle\SliderBundle\Entity\Slider;
use Ibnab\Bundle\SliderBundle\RelatedItem\AssignerStrategyInterface;
use Symfony\Component\Translation\TranslatorInterface;
class SliderHandler extends UpdateHandler
{
    protected $relatedBannerAssigner;
    /** @var TranslatorInterface */
    private $translator;

    /**
     * @param AssignerStrategyInterface $relatedBannerAssigner
     */
    public function setRelatedBannerAssigner(AssignerStrategyInterface $relatedBannerAssigner)
    {
        $this->relatedBannerAssigner = $relatedBannerAssigner;
    }
    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    /**
     * @param Coupon $entity
     * @return bool
     */
    public function handleRegister(Slider $entity)
    {
        if ($this->getCurrentRequest()->getMethod() === 'POST') {
            $manager = $this->doctrineHelper->getEntityManager($entity);

            $manager->persist($entity);
            $manager->flush();

            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    protected function saveForm(FormInterface $form, $data)
    {
        return parent::saveForm($form, $data) && $this->saveRelatedBanners($form, $data);
    }
    /**
     * @param FormInterface  $form
     * @param Product        $entity
     * @param array|callable $saveAndStayRoute
     * @param array|callable $saveAndCloseRoute
     * @param string         $saveMessage
     * @param null           $resultCallback
     * @return array|RedirectResponse
     */
    protected function processSave(
        FormInterface $form,
        $entity,
        $saveAndStayRoute,
        $saveAndCloseRoute,
        $saveMessage,
        $resultCallback = null
    ) {
        $result = parent::processSave(
            $form,
            $entity,
            $saveAndStayRoute,
            $saveAndCloseRoute,
            $saveMessage,
            $resultCallback
        );


        return $result;
    }
    /**
     * @param object $entity
     */
    protected function saveEntity($entity)
    {
        $manager = $this->doctrineHelper->getEntityManager($entity);
        $manager->persist($entity);

        // flush entity with related entities
        $manager->flush();
    }
    /**
     * @param FormInterface $form
     * @param Product       $entity
     * @return bool
     */
    private function saveRelatedBanners(FormInterface $form, Slider $entity)
    {
        //var_dump($this->getCurrentRequest()->request->all());die();
        if (!$form->has('appendRelated') && !$form->has('removeRelated')) {
            return true;
        }

        $appendRelatedFormItem = $form->get('appendRelated');
        $removeRelatedFormItem = $form->get('removeRelated');
        $appendRelated = $appendRelatedFormItem->getData();
        $removeRelated = $removeRelatedFormItem->getData();
        $this->relatedBannerAssigner->removeRelations($entity, $removeRelated);

        try {
            $this->relatedBannerAssigner->addRelations($entity, $appendRelated);
        } catch (\InvalidArgumentException $e) {
            $this->addFormError($appendRelatedFormItem, $e->getMessage());
        } catch (\LogicException $e) {
            $this->addFormError($appendRelatedFormItem, $e->getMessage());
        } catch (\OverflowException $e) {
            $this->addFormError($appendRelatedFormItem, $e->getMessage());
        }

        return $form->getErrors()->count() === 0;
    }

    /**
     * @param FormInterface $form
     * @param string $message
     */
    private function addFormError(FormInterface $form, $message)
    {
        $form->addError(
            new FormError(
                $this->translator->trans($message, [], 'validators')
            )
        );
    }
}
