<?php

namespace Ibnab\Bundle\SliderBundle\Form\Handler;

use Symfony\Component\Form\FormInterface;
use Oro\Bundle\FormBundle\Model\UpdateHandler;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Oro\Bundle\FormBundle\Form\Handler\RequestHandlerTrait;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\FormBundle\Form\Handler\FormHandler;
use Oro\Bundle\UIBundle\Route\Router;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Ibnab\Bundle\SliderBundle\Helper\CacheBuilder;

class BannerHandler extends UpdateHandler
{
use RequestHandlerTrait;

    const ORIGINAL = 'product_original';


    /**
     * @var CacheBuilder
     */
    private $cacheBuilderHelper;
    /**
     * @param RequestStack $requestStack
     * @param Session $session
     * @param Router $router
     * @param DoctrineHelper $doctrineHelper
     * @param FormHandler $formHandler
     */
    public function __construct(
    RequestStack $requestStack, 
    Session $session, 
    Router $router, 
    DoctrineHelper $doctrineHelper,
    FormHandler $formHandler, 
    CacheBuilder $cacheBuilderHelper
    ) {
        $this->cacheBuilderHelper = $cacheBuilderHelper;
        parent::__construct($requestStack, $session, $router, $doctrineHelper, $formHandler);
    }
    /**
     * @param Coupon $entity
     * @return bool
     */
    public function handleRegister(Banner $entity)
    {
        if ($this->getCurrentRequest()->getMethod()  === 'POST') {
            $manager = $this->doctrineHelper->getEntityManager($entity);

            $manager->persist($entity);
            $manager->flush();

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function saveForm(FormInterface $form, $entity)
    {
        if (!$entity instanceof Banner) {
            throw new \InvalidArgumentException('Banner expected');
        }

        $form->setData($entity);
        if (in_array($this->getCurrentRequest()->getMethod(), ['POST', 'PUT'], true)) {
            $request = $this->getCurrentRequest();
            $this->submitPostPutRequest($form, $request);
            if ($form->isValid()) {
                $this->saveEntity($entity);
                $entity = $this->cacheBuilderHelper->rebuild($entity);
                $this->saveEntity($entity);
                return true;
            }
        }

        return false;
    }

    /**
     * @param object $entity
     */
    protected function saveEntity($entity)
    {
        $manager = $this->doctrineHelper->getEntityManager($entity);
        $manager->persist($entity);
        $manager->flush();
    }



}
