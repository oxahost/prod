<?php

namespace Ibnab\Bundle\SliderBundle\RelatedItem;

use Ibnab\Bundle\SliderBundle\Entity\Slider;

interface AssignerStrategyInterface
{

    public function addRelations(Slider $slider, array $bannersTo);


    public function removeRelations(Slider $slider, array $bannersTo);
}
