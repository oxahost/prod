<?php

namespace Ibnab\Bundle\SliderBundle\RelatedItem\RelatedBanner;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Ibnab\Bundle\SliderBundle\Entity\Slider;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Ibnab\Bundle\SliderBundle\Entity\RelatedBanner\RelatedBanner;
use Ibnab\Bundle\SliderBundle\Entity\Repository\RelatedBanner\RelatedBannerRepository;
use Ibnab\Bundle\SliderBundle\RelatedItem\AssignerStrategyInterface;

class AssignerDatabaseStrategy implements AssignerStrategyInterface
{

    /**
     * @var DoctrineHelper
     */
    private $doctrineHelper;

    /**
     * @param DoctrineHelper                    $doctrineHelper
     */
    public function __construct(DoctrineHelper $doctrineHelper)
    {
        $this->doctrineHelper = $doctrineHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function addRelations(Slider $bannerFrom, array $bannersTo)
    {
        $bannersTo = $this->validateRelations($bannerFrom, $bannersTo);

        if (count($bannersTo) === 0) {
            return;
        }

        foreach ($bannersTo as $bannerTo) {
            $banner = $this->getBannersRepository()->find($bannerTo);
            $this->addRelation($bannerFrom, $banner);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function removeRelations(Slider $bannerFrom, array $bannersTo)
    {
        foreach ($bannersTo as $bannerTo) {
            $this->removeRelation($bannerFrom, $bannerTo);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param Banner $bannerFrom
     * @param Banner $bannerTo
     */
    private function removeRelation(Slider $bannerFrom, $bannerTo)
    {
        $persistedRelation = $this->getRelatedBannersRepository()
            ->findOneBy(['slider_id' => $bannerFrom->getId(), 'banner_id' => $bannerTo]);

        if ($persistedRelation === null) {
            return;
        }

        $this->getEntityManager()->remove($persistedRelation);
    }

    /**
     * @param Banner $bannerFrom
     * @param Banner $bannerTo
     * @return bool
     */
    private function relationAlreadyExists(Slider $bannerFrom, $bannerTo)
    {
        return $this->getRelatedBannersRepository()->exists($bannerFrom, $bannerTo);
    }

    /**
     * @param Banner $bannerFrom
     * @param Banner $bannerTo
     */
    private function addRelation(Slider $bannerFrom, Banner $bannerTo)
    {
        $relatedBanner = new RelatedBanner();
        $relatedBanner->setSliderId($bannerFrom)
            ->setBannerId($bannerTo);

        $this->getEntityManager()->persist($relatedBanner);
    }

    /**
     * @param Banner   $bannerFrom
     * @param Banner[] $bannersTo
     *
     * @throws \LogicException when functionality is disabled
     * @throws \OverflowException when user tries to add more banners that limit allows
     * @throws \InvalidArgumentException When user tries to add related banner to itself
     *
     * @return Banner[]
     */
    private function validateRelations(Slider $bannerFrom, array $bannersTo)
    {

        $newRelations = [];
        foreach ($bannersTo as $bannerTo) {
            $newRelations[] = $bannerTo;
        }

        if (count($newRelations) === 0) {
            return [];
        }

        $numberOfRelations = $this->getRelatedBannersRepository()->countRelationsForBanner($bannerFrom->getId());
        $numberOfRelations += count($newRelations);

        return $newRelations;
    }



    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->doctrineHelper->getEntityManager(RelatedBanner::class);
    }

    /**
     * @return RelatedBannerRepository|EntityRepository
     */
    private function getRelatedBannersRepository()
    {
        return $this->doctrineHelper->getEntityRepository(RelatedBanner::class);
    }
    /**
     * @return RelatedBannerRepository|EntityRepository
     */
    private function getBannersRepository()
    {
        return $this->doctrineHelper->getEntityRepository(Banner::class);
    }
}
