<?php

namespace Ibnab\Bundle\SliderBundle\Migrations\Data\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Ibnab\Bundle\SliderBundle\Entity\Slider;
use Oro\Bundle\UserBundle\DataFixtures\UserUtilityTrait;
use Oro\Bundle\UserBundle\Migrations\Data\ORM\LoadAdminUserData;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Ibnab\Bundle\SliderBundle\Entity\RelatedBanner\RelatedBanner;
class LoadHomePageSlider extends AbstractFixture implements
    ContainerAwareInterface,
    DependentFixtureInterface
{
    use UserUtilityTrait;
    
    
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadAdminUserData::class
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = $this->getFirstUser($manager);
        $locator = $this->container->get('file_locator');
          
        $banner = new Banner();
        $banner->setOrganization($user->getOrganization());
        $banner->setOwner($user->getOwner());
        $banner->setTitle('Banner 1 For Slider 1');
        $banner->setOrder(0);
        $banner->setIsActive(1);
        $banner->setUrl("#");
        $banner->setTarget("_blank");
        $banner->setImgAlt("Banner 1");
        $banner->setImageSlider($this->getImage($manager,$locator,'banner1_big.jpg'));
        $banner->setImageSliderMed($this->getImage($manager,$locator,'banner1_med.jpg'));
        $banner->setImageSliderSmall($this->getImage($manager,$locator,'banner1_small.jpg'));
        $html = file_get_contents(__DIR__.'/data/banner1.html');
        $banner->setDescription($html);
        $manager->persist($banner);
        $banner2 = new Banner();
        $banner2->setOrganization($user->getOrganization());
        $banner2->setOwner($user->getOwner());
        $banner2->setTitle('Banner 2 For Slider 1');
        $banner2->setOrder(0);
        $banner2->setIsActive(1);
        $banner2->setUrl("#");
        $banner2->setTarget("_blank");
        $banner2->setImgAlt("Banner 2");
        $banner2->setImageSlider($this->getImage($manager,$locator,'banner2_big.jpg'));
        $banner2->setImageSliderMed($this->getImage($manager,$locator,'banner2_med.jpg'));
        $banner2->setImageSliderSmall($this->getImage($manager,$locator,'banner2_small.jpg'));
        $html = file_get_contents(__DIR__.'/data/banner2.html');
        $banner2->setDescription($html);
        $manager->persist($banner2);
        
        $slider = new Slider();
        $slider->setTitle('slider-home');
        $slider->setLocation('');
        $slider->setOrganization($user->getOrganization());
        $slider->setOwner($user->getOwner());
        $manager->persist($slider);
        $relatedBanner = new RelatedBanner();
        $relatedBanner->setSliderId($slider);
        $relatedBanner->setBannerId($banner);
        $manager->persist($relatedBanner);
        $manager->flush($relatedBanner);
        $relatedBanner2 = new RelatedBanner();
        $relatedBanner2->setSliderId($slider);
        $relatedBanner2->setBannerId($banner2);
        $manager->persist($relatedBanner2);
        $manager->flush();
        //$imageCacheHelper = $this->container->get('ibnab_slider.banner.cachebuilder.helper');
        //$imageCacheHelper->rebuild($banner);
        //$imageCacheHelper->rebuild($banner2);
        
    }
    protected function getImage(ObjectManager $manager, FileLocator $locator, $name)
    {
        $image = null;

        //try {
            $imagePath = $locator->locate(sprintf('@IbnabSliderBundle/Migrations/Data/ORM/images/%s', $name));

            if (is_array($imagePath)) {
                $imagePath = current($imagePath);
            }

            $fileManager = $this->container->get('oro_attachment.file_manager');
            $image = $fileManager->createFileEntity($imagePath);
            $manager->persist($image);
            $manager->flush();


       //} catch (\Exception $e) {
            //image not found
       //}

        return $image;
    }
}
