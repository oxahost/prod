<?php

namespace Ibnab\Bundle\SliderBundle\Migrations\Data\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Oro\Bundle\UserBundle\DataFixtures\UserUtilityTrait;
use Oro\Bundle\UserBundle\Migrations\Data\ORM\LoadAdminUserData;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;



class LoadAdsBanners extends AbstractFixture implements
    ContainerAwareInterface,
    DependentFixtureInterface
{
    use UserUtilityTrait;
    
    
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadAdminUserData::class
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = $this->getFirstUser($manager);
        $locator = $this->container->get('file_locator');
        $banners = ['banner-home-ads1' => '400-ads1.png','banner-home-ads2' => '400-ads2.png', 'banner-home-ads3' => '400-ads3.png'];
        foreach($banners as $key=>$banner){
            
        ${"banner".$banner} = new Banner();
        ${"banner".$banner}->setOrganization($user->getOrganization());
        ${"banner".$banner}->setOwner($user->getOwner());
        ${"banner".$banner}->setTitle($key);
        ${"banner".$banner}->setOrder(0);
        ${"banner".$banner}->setIsActive(1);
        ${"banner".$banner}->setUrl("#");
        ${"banner".$banner}->setTarget("_blank");
        ${"banner".$banner}->setImgAlt("Banner Top 1 Modern A");
        ${"banner".$banner}->setImageSlider($this->getImage($manager,$locator,$banner));
        ${"banner".$banner}->setImageSliderMed($this->getImage($manager,$locator,$banner));
        ${"banner".$banner}->setImageSliderSmall($this->getImage($manager,$locator,$banner));
        //$html = file_get_contents(__DIR__.'/data/banner1.html');
        ${"banner".$banner}->setDescription($key);
        $manager->persist(${"banner".$banner});
        }

        
        
        $manager->flush();
        //$imageCacheHelper = $this->container->get('ibnab_slider.banner.cachebuilder.helper');
        //$imageCacheHelper->rebuild($banner);
        //$imageCacheHelper->rebuild($banner2);
        
    }
    protected function getImage(ObjectManager $manager, FileLocator $locator, $name)
    {
        $image = null;

        //try {
            $imagePath = $locator->locate(sprintf('@IbnabSliderBundle/Migrations/Data/ORM/images/CollectionBanner1/%s', $name));

            if (is_array($imagePath)) {
                $imagePath = current($imagePath);
            }

            $fileManager = $this->container->get('oro_attachment.file_manager');
            $image = $fileManager->createFileEntity($imagePath);
            $manager->persist($image);
            $manager->flush();


       //} catch (\Exception $e) {
            //image not found
       //}

        return $image;
    }
}
