<?php

namespace Ibnab\Bundle\SliderBundle\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;

use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;
use Oro\Bundle\AttachmentBundle\Migration\Extension\AttachmentExtension;
use Oro\Bundle\AttachmentBundle\Migration\Extension\AttachmentExtensionAwareInterface;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class IbnabSliderBundle implements Migration, AttachmentExtensionAwareInterface
{
    /** @var AttachmentExtension */
    protected $attachmentExtension;
    const MAX_IMAGE_SLIDE_MAIN_IMAGE_SIZE_IN_MB = 10;
    const MAX_IMAGE_SLIDE_MEDIUM_IMAGE_SIZE_IN_MB = 10;
    const MAX_IMAGE_SLIDE_SMALL_IMAGE_SIZE_IN_MB = 10;

    /**
     * {@inheritdoc}
     */
    public function setAttachmentExtension(AttachmentExtension $attachmentExtension)
    {
        $this->attachmentExtension = $attachmentExtension;
    }
    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        /** Tables generation **/
        $this->createSliderTable($schema);
        $this->createBannerTable($schema);
        $this->createSliderBannerTable($schema);
        self::addPicToBanner($schema, $this->attachmentExtension);
        
        //$this->createSliderLanguageTable($schema);
        /** Foreign keys generation **/
        //$this->addPDFTemplateTableForeignKeys($schema);
        //$this->addPDFTemplateTranslationTableForeignKeys($schema);
    }

    /**
     * @param Schema              $schema
     * @param AttachmentExtension $attachmentExtension
     */
    public static function addPicToBanner(Schema $schema, AttachmentExtension $attachmentExtension)
    {
        $attachmentExtension->addImageRelation(
            $schema,
            'ibnab_slider_banner',
            'imageSlider',
            ['attachment' => ['acl_protected' => false, 'use_dam' => true]],
            self::MAX_IMAGE_SLIDE_MAIN_IMAGE_SIZE_IN_MB
        );
        $attachmentExtension->addImageRelation(
            $schema,
            'ibnab_slider_banner',
            'imageSliderMed',
            ['attachment' => ['acl_protected' => false, 'use_dam' => true]],
            self::MAX_IMAGE_SLIDE_MEDIUM_IMAGE_SIZE_IN_MB
        );
        $attachmentExtension->addImageRelation(
            $schema,
            'ibnab_slider_banner',               
            'imageSliderSmall',
            ['attachment' => ['acl_protected' => false, 'use_dam' => true]],
            self::MAX_IMAGE_SLIDE_SMALL_IMAGE_SIZE_IN_MB
        );
    }
    /**
     * Create ibnab_slider_slider table
     *
     * @param Schema $schema
     */
    protected function createSliderTable(Schema $schema)
    {
        $table = $schema->createTable('ibnab_slider_slider');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('title', 'string',['length' => 255]);
        $table->addColumn('showt_title', 'boolean' , ['notnull' => false]);
        $table->addColumn('dots', 'boolean' , ['notnull' => false]);
        $table->addColumn('draggable', 'boolean' , ['notnull' => false]);
        $table->addColumn('infinite', 'boolean' , ['notnull' => false]);
        $table->addColumn('focus_onSelect', 'boolean' , ['notnull' => false]);
        
        $table->addColumn('mobile_first', 'boolean' , ['notnull' => false]);
        $table->addColumn('pause_onFocus', 'boolean' , ['notnull' => false]);
        $table->addColumn('pause_onHover', 'boolean' , ['notnull' => false]);
        $table->addColumn('pause_onDotsHover', 'boolean' , ['notnull' => false]);
        
        $table->addColumn('arrows', 'boolean' , ['notnull' => false]);
        $table->addColumn('variable_width', 'boolean' , ['notnull' => false]);
        $table->addColumn('center_mode', 'boolean' , ['notnull' => false]);
        $table->addColumn('autoplay', 'boolean' , ['notnull' => false]);
        $table->addColumn('swipe', 'boolean' , ['notnull' => false]);
        $table->addColumn('touch_move', 'boolean' , ['notnull' => false]);
        $table->addColumn('vertical_swiping', 'boolean' , ['notnull' => false]);
        $table->addColumn('rtl', 'boolean' , ['notnull' => false]);
        $table->addColumn('autoplay_speed', 'integer', ['notnull' => false ]);
        $table->addColumn('slides_perRow', 'integer', ['notnull' => false ]);
        $table->addColumn('initial_slide', 'integer', ['notnull' => false ]);
        $table->addColumn('speed', 'integer', ['notnull' => false ]);
        $table->addColumn('slides_toShow', 'integer', ['notnull' => false]);
        $table->addColumn('slides_toScroll', 'integer', ['notnull' => false]);
        $table->addColumn('center_padding', 'integer', ['notnull' => false]);
        
        $table->addColumn('easing', 'string', ['notnull' => false,'length' => 45]);
        $table->addColumn('respond_to', 'string', ['notnull' => false,'length' => 45]);
        $table->addColumn('responsive', 'string', ['notnull' => false]);
        $table->addColumn('lazy_load', 'string', ['length' => 20,'notnull' => false]);
        $table->addColumn('location', 'string', ['length' => 200,'notnull' => false]);

        $table->addColumn('user_owner_id', 'integer', []);
        $table->addColumn('organization_id', 'integer', []);

        
        $table->setPrimaryKey(['id']);
        $table->addIndex(['title'], 'ibnab_slider_title_idx', []);
        $table->addIndex(['user_owner_id'], 'IDX_ibnabEE2DB9449EB185F9', []);
        $table->addIndex(['organization_id'], 'IDX_ibnabEE2DB94432C8A3DE', []);
        
        $table = $schema->getTable('ibnab_slider_slider');
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            []
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['user_owner_id'],
            ['id'],
            []
        );
       
    }
    /**
     * Create ibnab_slider_banner table
     *
     * @param Schema $schema
     */
    protected function createBannerTable(Schema $schema)
    {
        $table = $schema->createTable('ibnab_slider_banner');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('title', 'string',['length' => 255]);
        $table->addColumn('description', 'text',['notnull' => false]);
        $table->addColumn('order_banner', 'integer', ['notnull' => false ]);
        $table->addColumn('is_active_banner', 'boolean' , ['notnull' => false]);
        $table->addColumn('url_banner', 'string', ['notnull' => false]);
         $table->addColumn('image_banner', 'string', ['notnull' => false]);
        $table->addColumn('target_banner', 'string', ['notnull' => false,'length' => 250]);
        $table->addColumn('img_alt_banner', 'string', ['notnull' => false,'length' => 400]);
        $table->addColumn('big_cache', 'string',['length' => 600,'notnull' => false]);
        $table->addColumn('medium_cache', 'string',['length' => 600,'notnull' => false]);
        $table->addColumn('small_cache', 'string',['length' => 600,'notnull' => false]);
        $table->addColumn('user_owner_id', 'integer', []);
        $table->addColumn('organization_id', 'integer', []);       
        
        $table->setPrimaryKey(['id']);
        $table->addIndex(['title'], 'ibnab_banner_title_idx', []);
        $table->addIndex(['is_active_banner'], 'ibnab_banner_active_idx', []);
        $table->addIndex(['user_owner_id'], 'IDX_ibnabEE2DB9449EB185F9', []);
        $table->addIndex(['organization_id'], 'IDX_ibnabEE2DB94432C8A3DE', []);
        $table = $schema->getTable('ibnab_slider_banner');
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            []
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['user_owner_id'],
            ['id'],
            []
        );  
         
    }
    /**
     * Create ibnab_slider_banner_tow table
     *
     * @param Schema $schema
     */
    protected function createSliderBannerTable(Schema $schema)
    {
        $table = $schema->createTable('ibnab_slider_banner_two');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('banner_id', 'integer');
        $table->addColumn('slider_id','integer');        
        $table->setPrimaryKey(['id']);      
        $table->addUniqueIndex(['banner_id','slider_id'], 'IDX_Uniquesliderbanner_two');
        $table->addIndex(['banner_id'], 'ibnab_slider_banner_banner_two_idx', []);
        $table->addIndex(['slider_id'], 'ibnab_slider_banner_slider_two_idx', []);
        
        $table = $schema->getTable('ibnab_slider_banner_two');
        $table->addForeignKeyConstraint(
            $schema->getTable('ibnab_slider_slider'),
            ['slider_id'],
            ['id'],
            []
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('ibnab_slider_banner'),
            ['banner_id'],
            ['id'],
            []
        );  
        
        
    }
    /**
     * Create ibnab_slider_language
     *
     * @param Schema $schema
     */
    protected function createSliderLanguageTable(Schema $schema)
    {
        $table = $schema->createTable('ibnab_slider_language');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('banner_id', 'integer');
        $table->addColumn('language_id','integer');       
        $table->setPrimaryKey(['id']);         
        $table->addUniqueIndex(['banner_id','language_id'], 'IDX_Uniquesliderlanguage');        
        $table->addIndex(['banner_id'], 'ibnab_slider_bannerid_language_idx', []);
        $table->addIndex(['language_id'], 'ibnab_slider_banner_language_idx', []);
        $table = $schema->getTable('ibnab_slider_language');
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_language'),
            ['id'],
            ['language_id'],
            []
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('ibnab_slider_banner'),
            ['id'],
            ['banner_id'],
            []
        );      
    }
}

