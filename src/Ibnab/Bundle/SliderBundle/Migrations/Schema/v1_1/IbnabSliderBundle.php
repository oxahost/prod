<?php

namespace Ibnab\Bundle\SliderBundle\Migrations\Schema\v1_1;

use Doctrine\DBAL\Schema\Schema;

use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;


/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class IbnabSliderBundle implements Migration
{

    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        //$table = $schema->getTable('ibnab_slider_slider');
        $tableBanner = $schema->getTable('ibnab_slider_banner');
        //$table->addColumn('start', 'datetime', ['notnull' => false, 'comment' => '(DC2Type:datetime)']);
        //$table->addColumn('finish', 'datetime', ['notnull' => false, 'comment' => '(DC2Type:datetime)']);
        $tableBanner->addColumn('start', 'datetime', ['notnull' => false, 'comment' => '(DC2Type:datetime)']);
        $tableBanner->addColumn('finish', 'datetime', ['notnull' => false, 'comment' => '(DC2Type:datetime)']);
    }

}

