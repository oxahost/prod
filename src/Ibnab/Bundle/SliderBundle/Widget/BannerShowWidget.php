<?php

namespace Ibnab\Bundle\SliderBundle\Widget;

use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Oro\Bundle\CMSBundle\Widget\WidgetInterface;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;

class BannerShowWidget implements WidgetInterface {

    /** @var UrlGeneratorInterface */
    protected $urlGenerator;

    /** @var RequestStack */
    protected $requestStack;

    /**
     * @var DoctrineHelper
     */
    private $doctrineHelper;

    /** @var \Twig_Environment */
    protected $twig;

    /**
     * @param FormFactoryInterface  $formFactory
     * @param UrlGeneratorInterface $urlGenerator
     * @param TwigRenderer          $twigRenderer
     * @param RequestStack          $requestStack
     */
    public function __construct(
    UrlGeneratorInterface $urlGenerator, RequestStack $requestStack, DoctrineHelper $doctrineHelper, \Twig_Environment $twig
    ) {
        $this->urlGenerator = $urlGenerator;
        $this->requestStack = $requestStack;
        $this->doctrineHelper = $doctrineHelper;
        $this->twig = $twig;
    }

    /**
     * {@inheritDoc}
     */
    public function render(array $options = []) {
        if (isset($options['id'])) {
            $banner_id = $options['id'];
            $banner = $this->doctrineHelper->getEntityRepository(Banner::class)->find($banner_id);
            if (!is_null($banner) && !is_null($banner->getId())) {
                $rendered = $this->twig->render(
                        '@IbnabSlider/Banner/frontendShow.html.twig', [
                        'banner' => $banner,
                        ]
                );
            } else {
                $rendered = 'The Banner with ID ' . $banner_id . ' don\'t exist';
            }
        } else {
            $rendered = 'Plz specifie ID Slider';
        }


        return $rendered;
    }

}
