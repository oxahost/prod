<?php

namespace Ibnab\Bundle\SliderBundle\Widget;

use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Oro\Bundle\CMSBundle\Widget\WidgetInterface;
use Ibnab\Bundle\SliderBundle\Entity\Slider;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;

class SliderShowWidget implements WidgetInterface {

    /** @var UrlGeneratorInterface */
    protected $urlGenerator;

    /** @var RequestStack */
    protected $requestStack;

    /**
     * @var DoctrineHelper
     */
    private $doctrineHelper;

    /** @var \Twig_Environment */
    protected $twig;

    /**
     * @param FormFactoryInterface  $formFactory
     * @param UrlGeneratorInterface $urlGenerator
     * @param TwigRenderer          $twigRenderer
     * @param RequestStack          $requestStack
     */
    public function __construct(
    UrlGeneratorInterface $urlGenerator, RequestStack $requestStack, DoctrineHelper $doctrineHelper, \Twig_Environment $twig
    ) {
        $this->urlGenerator = $urlGenerator;
        $this->requestStack = $requestStack;
        $this->doctrineHelper = $doctrineHelper;
        $this->twig = $twig;
    }

    /**
     * {@inheritDoc}
     */
    public function render(array $options = []) {
        if (isset($options['id'])) {
            $slider_id = $options['id'];
            $silder = $this->doctrineHelper->getEntityRepository(Slider::class)->find($slider_id);
            if (!is_null($silder) && !is_null($silder->getId())) {
                $rendered = $this->twig->render(
                        '@IbnabSlider/Slider/frontendShow.html.twig', [
                        'slider_id' => $silder->getId(),
                        ]
                );
            } else {
                $rendered = 'The Slider with ID ' . $slider_id . ' don\'t exist';
            }
        } else {
            $rendered = 'Plz specifie ID Slider';
        }


        return $rendered;
    }

}
