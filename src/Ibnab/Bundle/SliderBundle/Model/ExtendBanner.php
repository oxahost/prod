<?php

namespace Ibnab\Bundle\SliderBundle\Model;

use Oro\Bundle\AttachmentBundle\Entity\File;
use Ibnab\Bundle\SliderBundle\Entity\Banner;

/**
 * @method File getImageSlider()
 * @method Banner setImageSlider(File $image)
 * @method File getImageSliderMed()
 * @method Banner setImageSliderMed(File $image)
 * @method File getImageSliderSmall()
 * @method Banner setImageSliderSmall(File $image)
 */
class ExtendBanner
{
    protected $imageSlider;
    protected $imageSliderMed;
    protected $imageSliderSmall;
    /**
     * Constructor
     *
     * The real implementation of this method is auto generated.
     *
     * IMPORTANT: If the derived class has own constructor it must call parent constructor.
     */
    public function __construct()
    {
    }
}
