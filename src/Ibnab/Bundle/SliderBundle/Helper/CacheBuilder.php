<?php

namespace Ibnab\Bundle\SliderBundle\Helper;

use Oro\Bundle\AttachmentBundle\Manager\AttachmentManager;
use Oro\Bundle\AttachmentBundle\Manager\MediaCacheManagerRegistry;
use Oro\Bundle\AttachmentBundle\Manager\ImageResizeManager;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Oro\Bundle\AttachmentBundle\Provider\AttachmentFilterAwareUrlGenerator;

/**
 * Helper service to collect product image custom information
 */
class CacheBuilder {

    const ORIGINAL = 'original';

    /**
     * @var ImageResizer
     */
    protected $imageResizer;

    /**
     * @var MediaCacheManager
     */
    private $mediaCacheManager;

    /**
     * @var AttachmentManager
     */
    private $attachmentManager;

    public function __construct(
    ImageResizeManager $imageResizer, MediaCacheManagerRegistry $mediaCacheManager, AttachmentManager $attachmentManager, AttachmentFilterAwareUrlGenerator $urlGenerator
    ) {
        $this->imageResizer = $imageResizer;
        $this->mediaCacheManager = $mediaCacheManager;
        $this->attachmentManager = $attachmentManager;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param Banner $entity
     * @return array
     */
    public function rebuild(Banner $entity) {
        $bigCache = $entity->getImageSlider();
        $mediumCache = $entity->getImageSliderMed();
        $smallCache = $entity->getImageSliderSmall();
        if ($bigCache) {
            //$imagePath = $this->attachmentManager->getFilteredImageUrl($bigCache, self::ORIGINAL);
            //for new version
            $imagePath = $this->getFilteredImageUrl($bigCache, self::ORIGINAL);
            $filteredImage = $this->imageResizer->applyFilter($bigCache, self::ORIGINAL,true);
            if ($filteredImage) {
                $this->mediaCacheManager->getManagerForFile($bigCache)->writeToStorage($filteredImage->getContent(), $imagePath);
            }
            $entity->setBigCache(str_replace('/index_dev.php', '', $imagePath));
        } else {
            $entity->setBigCache('');
        }
        if ($mediumCache) {
            //$imagePath = $this->attachmentManager->getFilteredImageUrl($mediumCache, self::ORIGINAL);
            //for new version
            $imagePath = $this->getFilteredImageUrl($mediumCache, self::ORIGINAL);
            $filteredImage = $this->imageResizer->applyFilter($mediumCache, self::ORIGINAL,true);
            if ($filteredImage) {
                $this->mediaCacheManager->getManagerForFile($mediumCache)->writeToStorage($filteredImage->getContent(), $imagePath);
            }
            $entity->setMediumCache(str_replace('/index_dev.php', '', $imagePath));
        } else {
            $entity->setMediumCache('');
        }
        if ($smallCache) {
            //$imagePath = $this->attachmentManager->getFilteredImageUrl($smallCache, self::ORIGINAL);
            //for new version
            $imagePath = $this->getFilteredImageUrl($smallCache, self::ORIGINAL);
            $filteredImage = $this->imageResizer->applyFilter($smallCache, self::ORIGINAL,true);
            if ($filteredImage) {
                $this->mediaCacheManager->getManagerForFile($smallCache)->writeToStorage($filteredImage->getContent(), $imagePath);
            }
            $entity->setSmallCache(str_replace('/index_dev.php', '', $imagePath));
        } else {
            $entity->setSmallCache('');
        }
        return $entity;
    }

    protected function getFilteredImageUrl($file, $filterName) {
        if ($file != null) {
            return $this->urlGenerator->generate(
                            'oro_frontend_attachment_filter_image', [
                        'filter' => $filterName,
                        'id' => $file->getId(),
                        'filename' => $file->getFilename(),
                            ], UrlGeneratorInterface::ABSOLUTE_PATH
            );
        }
        return null;
    }

}
