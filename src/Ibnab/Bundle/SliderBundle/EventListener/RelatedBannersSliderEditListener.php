<?php

namespace Ibnab\Bundle\SliderBundle\EventListener;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Oro\Bundle\FormBundle\Form\Type\EntityIdentifierType;
use Oro\Bundle\FormBundle\Event\FormHandler\FormProcessEvent;
use Ibnab\Bundle\SliderBundle\Entity\Banner;
use Oro\Bundle\ProductBundle\RelatedItem\AbstractRelatedItemConfigProvider;
use Oro\Bundle\UIBundle\View\ScrollData;
use Oro\Bundle\UIBundle\Event\BeforeListRenderEvent;

class RelatedBannersSliderEditListener
{
    const RELATED_ITEMS_ID = 'relatedItems';

    /** @var int */
    const BLOCK_PRIORITY = 10;

    /** @var TranslatorInterface */
    private $translator;

    /** @var AbstractRelatedItemConfigProvider */
    private $configProvider;

    /** @var AuthorizationCheckerInterface */
    private $authorizationChecker;

    /**
     * @param TranslatorInterface               $translator
     * @param AbstractRelatedItemConfigProvider $configProvider
     * @param AuthorizationCheckerInterface     $authorizationChecker
     */
    public function __construct(
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->translator = $translator;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param BeforeListRenderEvent $event
     */
    public function onBannerEdit(BeforeListRenderEvent $event)
    {
        /*
        if ($this->authorizationChecker->isGranted('ibnab_slider_banner_related_banners_update')
        ) {
            return;
        }*/

        $twigEnv = $event->getEnvironment();
        $relatedBannersTemplate = $twigEnv->render(
            '@IbnabSlider/Slider/RelatedBanners/relatedBanners.html.twig',
            [
                'form' => $event->getFormView(),
                'entity' => $event->getEntity()
            ]
        );
        $this->addEditPageBlock($event->getScrollData(), $relatedBannersTemplate);
    }

    /**
     * @param FormProcessEvent $event
     */
    public function onFormDataSet(FormProcessEvent $event)
    {
        //if ($this->authorizationChecker->isGranted('ibnab_slider_manage_update')) {
            $event->getForm()->add(
                'appendRelated',
                EntityIdentifierType::class,
                [
                    'class' => Banner::class,
                    'required' => false,
                    'mapped' => false,
                    'multiple' => true,
                ]
            );
            $event->getForm()->add(
                'removeRelated',
                EntityIdentifierType::class,
                [
                    'class' => Banner::class,
                    'required' => false,
                    'mapped' => false,
                    'multiple' => true,
                ]
            );
        //} else {
          //  $event->getForm()->remove('appendRelated');
          //  $event->getForm()->remove('removeRelated');
       // }
    }

    /**
     * @param ScrollData $scrollData
     * @param string $relatedBannerssForm
     */
    private function addEditPageBlock(ScrollData $scrollData, $relatedBannerssForm)
    {
        $scrollData->addNamedBlock(
            self::RELATED_ITEMS_ID,
            $this->translator->trans('ibnab.slider.sections.relatedBanners'),
            self::BLOCK_PRIORITY
        );
        $subBlock = $scrollData->addSubBlock(self::RELATED_ITEMS_ID);
        $scrollData->addSubBlockData(self::RELATED_ITEMS_ID, $subBlock, $relatedBannerssForm, 'relatedItems');
    }
}
