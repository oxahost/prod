<?php

namespace Ibnab\Bundle\SliderBundle\Layout\DataProvider;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

class LocationProvider {

    /** @var ContainerInterface */
    protected $container;
    /** @var LoggerInterface */
    protected $logger;
    /** @var array */
    protected $configCache = [];
    protected static $sliderByLoaction = [];
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container,LoggerInterface $logger) {
        $this->container = $container;
        $this->logger = $logger;
    }

    protected function getEntityManager() {
        $em = $this->container->get('doctrine.orm.entity_manager');
        return $em;
    }
    public function getIsLocation($location = null){
        $result = $this->getSliderByLocation();
        if(!is_null($location)){
           if(isset($result[$location])){
               return 1;
           } 
        }
        return 0;
    }
    public function getSidlerLocation($location = null){
        $result = $this->getSliderByLocation();
        if(!is_null($location)){
           if(isset($result[$location])){
               
               return $result[$location];
           } 
        }
        return [];
    }
    public function getSidlerById($id = null){
        if(!is_null($id )){
        $em = $this->getEntityManager();
        $slider = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Slider')->find($id); 
        }else{
            return null;
        }
        return $slider;
    }
    public function getBannerById($id = null){
        if(!is_null($id )){
        $em = $this->getEntityManager();
        $banner = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Banner')->find($id);
        }else{
            return null;
        }
        return $banner;
    }
    /**
     * @param Product $product
     *
     * @return array
     */
    public function getSliderByLocation() {
        
        if (array_key_exists('sliderByLocation', self::$sliderByLoaction)) {
            return self::$sliderByLoaction['sliderByLocation'];
        }
        $this->logger->notice('count sliderLocation time');
        $em = $this->getEntityManager();
        $newSliderCollection = [];
        $sliders = $em->getRepository('Ibnab\Bundle\SliderBundle\Entity\Slider')->findAll();      
        foreach($sliders as $slider){
            $newSliderCollection[$slider->getLocation()][] = $slider;
        }
        self::$sliderByLoaction['sliderByLocation']= $newSliderCollection;
        return self::$sliderByLoaction['sliderByLocation'];
    }

}
