<?php

namespace Ibnab\Bundle\SliderBundle\Layout\DataProvider;


use Symfony\Component\DependencyInjection\ContainerInterface;

class SliderProvider {

    /** @var ContainerInterface */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }


}
