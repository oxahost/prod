<?php

namespace Ibnab\Bundle\InternWidgetBundle\Migrations\Data\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gaufrette\Adapter\Local;
use Gaufrette\Filesystem;
use Oro\Bundle\AttachmentBundle\Entity\File as AttachmentFile;
use Ibnab\Bundle\InternWidgetBundle\ContentWidget\ProductsCarouselsContentWidgetType;
use Oro\Bundle\CMSBundle\Entity\ContentBlock;
use Oro\Bundle\CMSBundle\Entity\ContentWidget;
use Oro\Bundle\CMSBundle\Entity\ImageSlide;
use Oro\Bundle\CMSBundle\Entity\TextContentVariant;
use Oro\Bundle\DigitalAssetBundle\Entity\DigitalAsset;
use Oro\Bundle\LocaleBundle\Entity\LocalizedFallbackValue;
use Oro\Bundle\UserBundle\DataFixtures\UserUtilityTrait;
use Oro\Bundle\UserBundle\Entity\User;
use Oro\Bundle\UserBundle\Migrations\Data\ORM\LoadAdminUserData;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Adds content block with Image Slider content widget in it.
 */
class LoadHomePageWidget extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;
    use UserUtilityTrait;

    /** @var string */
    private const HOME_PAGE_SLIDER_ALIAS_IMAN = 'home-page-iman-content';
    private const FEATUED_PRODUCTS_ALIAS = 'home-featured-products';
    private const NEW_PRODUCTS_ALIAS = 'home-new-arrival-products';
    private const TOPSELL_PRODUCTS_ALIAS = 'home-top-selling-products';
    private const FEATUED_CATEGORIES_ALIAS = 'home-featured-categories';
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadAdminUserData::class
        ];
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $carouselType = ['featured' => 'featured','new' => 'new','top_selling' => 'top_selling','featured_categories' => 'featured_categories'];
        $this->AddCarouselWidget($manager,$carouselType['featured_categories'],self::FEATUED_CATEGORIES_ALIAS);
        $this->AddCarouselWidget($manager,$carouselType['featured'],self::FEATUED_PRODUCTS_ALIAS);
        $this->AddCarouselWidget($manager,$carouselType['new'],self::NEW_PRODUCTS_ALIAS);
        $this->AddCarouselWidget($manager,$carouselType['top_selling'],self::TOPSELL_PRODUCTS_ALIAS);


      
    }
    private function AddCarouselWidget(ObjectManager $manager,string $carouselType,string $alias): void{
        $user = $this->getFirstUser($manager);
        $widget = new ContentWidget();
        $widget->setWidgetType(ProductsCarouselsContentWidgetType::getName());
        $widget->setName($alias);
        $widget->setOrganization($user->getOrganization());
        $widget->setSettings(
            [
                'carouselType' => $carouselType,
                'useSlider' => true,
                'arrows' => true,
                'dots' => false,
                'draggable' => true,
                'autoplay' => true,
                'rtl' => true,
                'defaultShow' => 5,
                'desktopShow' => 5,
                'tabletShow' => 3,
                'smallTabletShow' => 2,
                'mobileShow' => 1
                
            ]
        );
        $manager->persist($widget);
        $manager->flush();
        $this->updateOrCreateContentBlock(
            $manager,
            $user,
            '<div data-title="'.$alias.'" data-type="default_widget_products_carousels" class="content-widget content-placeholder">
                {{ widget("'.$alias.'") }}
            </div>',
            self::HOME_PAGE_SLIDER_ALIAS_IMAN
        );
    }
    /**
     * @param ObjectManager $manager
     * @param User $user
     * @param string $content
     * @return ContentBlock|null
     */
    private function updateOrCreateContentBlock(ObjectManager $manager, User $user, string $content,string $alias): void
    {
        $contentBlock = $manager->getRepository(ContentBlock::class)
            ->findOneBy(['alias' => $alias]);

        if (!$contentBlock instanceof ContentBlock) {
            $title = new LocalizedFallbackValue();
            $title->setString($alias);
            $manager->persist($title);

            $variant = new TextContentVariant();
            $variant->setDefault(true);
            $variant->setContent($content);
            $manager->persist($variant);
            $slider = new ContentBlock();
            $slider->setOrganization($user->getOrganization());
            $slider->setOwner($user->getOwner());
            $slider->setAlias($alias);
            $slider->addTitle($title);
            $slider->addContentVariant($variant);
            $manager->persist($slider);
           } else {
            foreach ($contentBlock->getContentVariants() as $contentVariant) {
                    $contentVariant->setContent($contentVariant->getContent().$content);
            }
        }

        $manager->flush();
    }

}
