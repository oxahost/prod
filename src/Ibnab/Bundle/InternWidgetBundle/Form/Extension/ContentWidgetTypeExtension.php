<?php

namespace Ibnab\Bundle\InternWidgetBundle\Form\Extension;

use Oro\Bundle\CMSBundle\Form\Type\ContentWidgetType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ContentWidgetTypeExtension extends AbstractTypeExtension {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                   /* $form = $event->getForm();
                    $widgetType = $event->getData()->getWidgetType();
                    if ($widgetType == 'product_carousels') {
                        $config = $form->get('template')->getConfig();
                        //$type = $config->getType()->getInnerType();
                        $options = $config->getOptions();
                        $form->remove('template');
                        $form->add('template', TextType::class, array_replace($options, array(
                            'disabled' => true
                        )));
                    }*/
                });
    }

    public function getExtendedType() {
        return ContentWidgetType::class;
    }

}
