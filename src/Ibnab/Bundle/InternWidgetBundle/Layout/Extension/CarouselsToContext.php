<?php

namespace Ibnab\Bundle\InternWidgetBundle\Layout\Extension;

use Oro\Component\Layout\ContextConfiguratorInterface;
use Oro\Component\Layout\ContextInterface;

class CarouselsToContext implements ContextConfiguratorInterface
{
    /**
     * {@inheritdoc}
     */
    public function configureContext(ContextInterface $context)
    {
        $context->getResolver()
            ->setRequired(['defaultCarouselsCurrentContent'])
             ;
        $context->set('defaultCarouselsCurrentContent', []);
        
    }

}
