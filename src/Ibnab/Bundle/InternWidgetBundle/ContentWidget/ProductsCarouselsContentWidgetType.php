<?php

namespace Ibnab\Bundle\InternWidgetBundle\ContentWidget;

use Oro\Bundle\CMSBundle\ContentWidget\ContentWidgetTypeInterface;
use Oro\Bundle\CMSBundle\ContentWidget\AbstractContentWidgetType;
use Oro\Bundle\CMSBundle\Entity\ContentWidget;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Valid;
use Oro\Bundle\LayoutBundle\Layout\LayoutContextHolder;
use Oro\Bundle\LayoutBundle\Layout\LayoutManager;
use Twig\Environment;

class ProductsCarouselsContentWidgetType implements ContentWidgetTypeInterface
{
    /** @var LayoutManager */
    private $layoutManager;
    /** @var LayoutContextHolder */
    private $contextHolder;

    /**
     * @param LayoutContextHolder $contextHolder
     * @param ConfigManager       $configManager
     * @param bool                $isDebug
     */
    public function __construct(LayoutContextHolder $contextHolder,LayoutManager $layoutManager
        )
    {
        $this->contextHolder = $contextHolder;
        $this->layoutManager = $layoutManager;

    }
    public static function getName(): string
    {
        return 'default_widget_products_carousels';
    }

    public function getLabel(): string
    {
        return 'ibnab.internwidget.products_carousels.label';
    }
    /**
     * {@inheritdoc}
     */
    public function getBackOfficeViewSubBlocks(ContentWidget $contentWidget, Environment $twig): array
    {
        
        $data = $this->getWidgetData($contentWidget);

        return [
            [
                'title' => 'oro.cms.contentwidget.sections.slider_options.label',
                'subblocks' => [
                    [
                        'data' => [
                            $twig->render('@IbnabInternWidget/ContentWidget/view.html.twig', $data),
                        ]
                    ],
                ]
            ]
        ];
    }
    public function getSettingsForm(ContentWidget $contentWidget, FormFactoryInterface $formFactory): ?FormInterface
    {
        //$contentWidget->setLayout('@IbnabInternWidget/ContentWidget/view.html.twig');
        return $formFactory->create(FormType::class)
            ->add('carouselType', ChoiceType::class, array(
                    'label' => 'ibnab.internwidget.carouselsType_label',
                    'multiple' => false,
                    'required' => true,
                    'block' => 'carousels_options',
                    'choices' => array(
                        'ibnab.internwidget.carouselsType.featured_label' => 'featured',
                        'ibnab.internwidget.carouselsType.new_label' => 'new',
                        'ibnab.internwidget.carouselsType.topSelling_label' => 'top_selling',
                        'ibnab.internwidget.carouselsType.featuredCategories_label' => 'featured_categories',
                        'ibnab.internwidget.carouselsType.featuredMenu_label' => 'featured_menu'
                        
                    )))
                ->add('useSlider', CheckboxType::class, array(
                    'label' => 'ibnab.internwidget.useSlider_label',
                    'required' => false,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new Type('boolean'),
                    ]
                ))
                ->add('arrows', CheckboxType::class, array(
                    'label' => 'ibnab.internwidget.arrows_label',
                    'required' => false,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new Type('boolean'),
                    ]
                ))
                ->add('dots', CheckboxType::class, array(
                    'label' => 'ibnab.internwidget.dots_label',
                    'required' => false,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new Type('boolean'),
                    ]
                ))
                ->add('draggable', CheckboxType::class, array(
                    'label' => 'ibnab.internwidget.draggable_label',
                    'required' => false,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new Type('boolean'),
                    ]
                ))                
                ->add('autoplay', CheckboxType::class, array(
                    'label' => 'ibnab.internwidget.autoplay_label',
                    'required' => false,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new Type('boolean'),
                    ]
                ))
                ->add('rtl', CheckboxType::class, array(
                    'label' => 'ibnab.internwidget.rtl_label',
                    'required' => false,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new Type('boolean'),
                    ]
                ))                
                ->add('defaultShow', IntegerType::class, array(
                    'label' => 'ibnab.internwidget.defaultShow_label',
                    'required' => true,
                    'block' => 'carousels_options',
                    'block_config' => [
                        'carousels_options' => [
                            'title' => 'ibnab.internwidget.sections.carousels_options.label'
                        ]
                    ],
                    'constraints' => [
                        new NotBlank(),
                        new Type('integer'),
                        new Range(['min' => 1]),
                    ]
                ))
                ->add('desktopShow', IntegerType::class, array(
                    'label' => 'ibnab.internwidget.desktopShow_label',
                    'required' => true,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new NotBlank(),
                        new Type('integer'),
                        new Range(['min' => 1]),
                    ]                  
                ))
                ->add('tabletShow', IntegerType::class, array(
                    'label' => 'ibnab.internwidget.tabletShow_label',
                    'required' => true,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new NotBlank(),
                        new Type('integer'),
                        new Range(['min' => 1]),
                    ] 
                ))
                ->add('smallTabletShow', IntegerType::class, array(
                    'label' => 'ibnab.internwidget.smallTabletShow_label',
                    'required' => true,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new NotBlank(),
                        new Type('integer'),
                        new Range(['min' => 1]),
                    ] 
                ))
                ->add('mobileShow', IntegerType::class, array(
                    'label' => 'ibnab.internwidget.mobileShow_label',
                    'required' => true,
                    'block' => 'carousels_options',
                    'constraints' => [
                        new NotBlank(),
                        new Type('integer'),
                        new Range(['min' => 1]),
                    ] 
                ));
    }

    /**
     * {@inheritdoc}
     */
    public function getWidgetData(ContentWidget $contentWidget): array
    {
        $context = $this->contextHolder->getContext();
        $settings = $contentWidget->getSettings();
        $vars = null;
        if($context != null){
        $sliderType = ['featured' => 'featured_dynamic_products','new' => 'new_arrival_dynamic_products','top_selling' => 'top_selling_dynamic_products','featured_categories' => 'featured_categories','featured_menu' => 'featured_menu'];
        $widgetSliderType = isset($sliderType[$settings['carouselType']]) ? $sliderType[$settings['carouselType']] : 'featured_products';
        //echo var_dump($context->get('featured_products'));
        $layout = $this->layoutManager->getLayout($context, $widgetSliderType); 
        $view = $layout->getView();
        $vars = $view->vars;
        $vars['use_slider'] = isset($vars['use_slider']) ? $settings['useSlider'] : 0;
        $instancName = $widgetSliderType.'_'.time();
        $vars['id'] = $instancName;        
        $vars['unique_block_prefix'] = '_'.$instancName;
        $vars['attr']['data-page-component-name'] = '_'.$instancName;
        if($vars['use_slider']){
        if(isset($vars['slider_options']['slidesToShow'])){
             $vars['slider_options']['slidesToShow'] = $settings['defaultShow'];
        }

        if(isset($vars['slider_options']['arrows'])){
             $vars['slider_options']['arrows'] = isset($settings['arrows']) ? $settings['arrows'] : false;
        }
        if(isset($vars['slider_options'])){
             $vars['slider_options']['dots'] = isset($settings['dots']) ? $settings['dots'] : true;
             $vars['slider_options']['autoplay'] = isset($settings['autoplay']) ? $settings['autoplay'] : true;
             $vars['slider_options']['draggable'] = isset($settings['draggable']) ? $settings['draggable'] : false;
             $vars['slider_options']['rtl'] = isset($settings['draggable']) ? $settings['rtl'] : false;
             
        }
        if(isset($vars['slider_options']['responsive'])){          
             if(isset($vars['slider_options']['responsive'][0]['settings'])){
                $vars['slider_options']['responsive'][0]['settings']['slidesToShow'] = $settings['desktopShow'];
                $vars['slider_options']['responsive'][0]['settings']['arrows'] = $settings['arrows'];
             }
             if(isset($vars['slider_options']['responsive'][1]['settings'])){
                $vars['slider_options']['responsive'][1]['settings']['slidesToShow'] = $settings['tabletShow'];
                $vars['slider_options']['responsive'][1]['settings']['arrows'] = $settings['arrows'];
             }
             if(isset($vars['slider_options']['responsive'][2]['settings'])){
                $vars['slider_options']['responsive'][2]['settings']['slidesToShow'] = $settings['smallTabletShow'];
                $vars['slider_options']['responsive'][2]['settings']['arrows'] = $settings['arrows'];
             }
             if(isset($vars['slider_options']['responsive'][3]['settings'])){
                $vars['slider_options']['responsive'][3]['settings']['slidesToShow'] = $settings['mobileShow'];
                $vars['slider_options']['responsive'][3]['settings']['arrows'] = $settings['arrows'];
             }
        }
        }
        //$view->vars = $vars;
        //$vars['block']->vars = $vars;
        //$layout2 = $this->layoutManager->getLayout($context, 'featured_products_container');
        //$block = $layout->getView();
        //$response = $layout->render();
       // $vars['use_slider'] = false;
        }else{
          $response = null;  
        }

        $data = $contentWidget->getSettings();
        return array_merge(
            $settings,
            ['blockContext' => $vars['block']]         
        );
    }

    /**
     * {@inheritdoc}
     */
    public function isInline(): bool
    {
        return false;
    }
    /**
     * {@inheritdoc}
     */
    public function getDefaultTemplate(ContentWidget $contentWidget, Environment $twig): string
    {
        //echo ',oo';die();
        return "";
        //return $twig->render('@IbnabInternWidget/ContentWidget/view.html.twig', $contentWidget->getSettings()); ;
    }
}
