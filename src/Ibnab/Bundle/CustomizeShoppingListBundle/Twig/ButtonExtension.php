<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Ibnab\Bundle\CustomizeShoppingListBundle\Layout\DataProvider\Checkout;
use Oro\Bundle\ActionBundle\Provider\ButtonProvider;
use Oro\Bundle\ActionBundle\Provider\ButtonSearchContextProvider;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
class ButtonExtension extends \Twig_Extension
{

    /** @var ContainerInterface */
    protected $checkoutProvider;



    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container,
        ButtonProvider $buttonProvider,
        DoctrineHelper $doctrineHelper,
        ButtonSearchContextProvider $contextProvider) {
        if(!isset($this->checkoutProvider)){
            $this->checkoutProvider = new Checkout($container,$buttonProvider,$doctrineHelper,$contextProvider);
        }
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_top_shoppinglist_button', [$this, 'getTopShoppingListButton'])
        ];
    }

    public function getTopShoppingListButton($shoppinglist)
    {
        return $this->checkoutProvider->getAll($shoppinglist);
        
    }

}
