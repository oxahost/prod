<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\Provider;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;

class ConfigurationProvider
{
    const ENABLE_FIELD = 'ibnab_customize_shopping_list.enable';
    const REDIRECT_FIELD = 'ibnab_customize_shopping_list.redirect';
    const EFP_FIELD = 'ibnab_customize_shopping_list.enablefrompoup';
    const FC_FIELD = 'ibnab_customize_shopping_list.enableflycart';
    const UP_FIELD = 'ibnab_customize_shopping_list.enablepoupupdate';
    const UF_FIELD = 'ibnab_customize_shopping_list.enableflycartupdate';

    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }

    /**
     * @return string
     */
    public function isEnable()
    {       
        return $this->configManager->get(self::ENABLE_FIELD);    
    }
    /**
     * @return string
     */
    public function getRedirect()
    {       
        return $this->configManager->get(self::REDIRECT_FIELD);    
    }
    /**
     * @return string
     */
    public function isEnableFromPopup()
    {       
        return $this->configManager->get(self::EFP_FIELD);    
    }    
     /**
     * @return string
     */
    public function isEnableFlyCart()
    {       
        return $this->configManager->get(self::FC_FIELD);    
    }
    /**
     * @return string
     */
    public function isEnableUpdatePopup()
    {       
        return $this->configManager->get(self::UP_FIELD);    
    }
    /**
     * @return string
     */
    public function isEnableUpdateFlyCart()
    {       
        return $this->configManager->get(self::UF_FIELD);    
    }
}
