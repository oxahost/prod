<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\Controller\Frontend;

use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ShoppingListBundle\Entity\LineItem;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Oro\Bundle\ActionBundle\Model\ActionData;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Oro\Bundle\ProductBundle\Form\Type\FrontendLineItemType;
use Oro\Bundle\ShoppingListBundle\Form\Handler\LineItemHandler;
use Oro\Bundle\ShoppingListBundle\Manager\CurrentShoppingListManager;
use Oro\Bundle\ShoppingListBundle\Manager\ShoppingListManager;
use Oro\Bundle\CheckoutBundle\Entity\Checkout;
use Oro\Bundle\ShoppingListBundle\Controller\Frontend\AjaxLineItemController as ParentAjaxLineItemController;
use Symfony\Component\PropertyAccess\PropertyPath;
use Oro\Bundle\ShoppingListBundle\Entity\ShoppingList;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Ibnab\Bundle\CustomizeShoppingListBundle\Provider\ConfigurationProvider;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Controller that manages products and line items for a shopping list via AJAX requests.
 */
class AjaxLineItemController extends ParentAjaxLineItemController {
    
    private $config;

    public function setConfig(ConfigurationProvider $config)
    {
        $this->config = $config;
    }
    /**
     * Add Product to Shopping List (product view form)
     *
     * @Route(
     *      "/add-product-from-view/{productId}",
     *      name="oro_shopping_list_frontend_add_product",
     *      requirements={"productId"="\d+"},
     *      methods={"POST"}
     * )
     * @AclAncestor("oro_product_frontend_view")
     * @ParamConverter("product", class="OroProductBundle:Product", options={"id" = "productId"})
     *
     * @param Request $request
     * @param Product $product
     *
     * @return JsonResponse
     */
    public function addProductFromViewAction(Request $request, Product $product) {
        $currentShoppingListManager = $this->get(CurrentShoppingListManager::class);
        $shoppingList = $currentShoppingListManager->getForCurrentUser($request->get('shoppingListId'));
        $postParams = $request->request->all();
        $fromQuickPopup = isset($postParams['oro_product_frontend_line_item']['fromquickpopup']) ? $postParams['oro_product_frontend_line_item']['fromquickpopup'] : 0 ;
        $requestProductFromValue = $request->request->get("oro_product_frontend_line_item");
        unset($requestProductFromValue['fromquickpopup']);
        //var_dump($requestProductFromValue);die();
        $request->request->set('oro_product_frontend_line_item',$requestProductFromValue);
        if (!$this->get(AuthorizationCheckerInterface::class)->isGranted('EDIT', $shoppingList)) {
            throw $this->createAccessDeniedException();
        }
        $parentProduct = $this->getParentProduct($request);

        $lineItem = (new LineItem())
                ->setProduct($product)
                ->setShoppingList($shoppingList)
                ->setCustomerUser($shoppingList->getCustomerUser())
                ->setOrganization($shoppingList->getOrganization());

        if ($parentProduct && $parentProduct->isConfigurable()) {
            $lineItem->setParentProduct($parentProduct);
        }

        $form = $this->createForm(FrontendLineItemType::class, $lineItem);
        $handler = new LineItemHandler(
            $form,
            $request,
            $this->getDoctrine(),
            $this->get(ShoppingListManager::class),
            $currentShoppingListManager,
            $this->get(ValidatorInterface::class)
        );
        $isFormHandled = $handler->process($lineItem);

        if (!$isFormHandled) {
            return new JsonResponse(['successful' => false, 'message' => (string) $form->getErrors(true, false)]);
        }


        $successResponse = $this->getSuccessResponse($shoppingList, $product, 'oro.shoppinglist.product.added.label');
        $config = $this->getConfigurationProvider();
        if ($config->isEnable()) {
            $successResponse['formAdd'] = true;
            $typeRedirect = $config->getRedirect();
            $enableFromOtherPopup = $config->isEnableFromPopup();
            if($fromQuickPopup != 1 || $enableFromOtherPopup==1){
            if ($typeRedirect == 0) {
                $successResponse['redirectUrl'] = $this->generateUrl('oro_shopping_list_frontend_view', ['id' => $shoppingList->getId()]);
            } elseif ($typeRedirect == 1) {
                $redirectUrl = $this->findCheckout($shoppingList);
                if (!is_null($redirectUrl)) {
                    $successResponse['redirectUrl'] = $redirectUrl;
                }
            }elseif ($typeRedirect == 2) {
                    $successResponse['redirectUrl'] = $this->generateUrl('ibnab_customizeshoppinglist_list_frontend_view', ['id' => $shoppingList->getId()]);
                    $successResponse['popup'] = true;
            }elseif ($typeRedirect == 3) {
                    $successResponse['redirectUrl'] = $this->generateUrl('ibnab_customizeshoppinglist_item_frontend_view', ['id' => $shoppingList->getId(), 'pid' => $lineItem->getProduct()->getId() ]);
                    $successResponse['popup'] = true;
            }else {
                unset($successResponse['formAdd']);
            }
            return new JsonResponse(
                    $successResponse
            );
            }else{
                
            }
        }
        return new JsonResponse(
                $this->getSuccessResponse($shoppingList, $product, 'oro.shoppinglist.product.added.label')
        );

        //eturn $this->redirectToRoute('oro_shopping_list_frontend_view', [], 307);
    }

    protected function getConfigurationProvider() {
        return $this->config;
    }

    protected function getUser() {
        $token = $this->get('security.token_storage')->getToken();
        return $token->getUser();
    }

    protected function findCheckout($shoppingList) {
        /*$checkoutRepository = $this->get('oro_checkout.repository.checkout');
        $workflowRegistry = $this->get('oro_workflow.registry');
        $workflows = $workflowRegistry->getActiveWorkflowsByEntityClass(Checkout::class);
        $workflowName = 'b2b_flow_checkout';
        $workFlowInstance = null;
        foreach ($workflows as $workflow) {
            $definition = $workflow->getDefinition();
            $workFlowInstance = $workflow;
            $workflowName = $definition->getName();
        }
        if(!is_null($workFlowInstance)){
           $workFlowInstance->start($shoppingList,["entityClass"=>"Oro\\Bundle\\ShoppingListBundle\\Entity\\ShoppingList","entityId" => $shoppingList->getId(),"routeName" => ""],'start_from_shoppinglist');
        }
        $user = $this->getUser();
        if (is_object($user)) {
            $checkoutResult = $checkoutRepository->findCheckoutByCustomerUserAndSourceCriteria($user,['shoppingList' => $shoppingList, 'deleted' => false], $workflowName);
        } else {
            $checkoutResult = $checkoutRepository->findCheckoutBySourceCriteria(['shoppingList' => $shoppingList, 'deleted' => false], $workflowName);
        }*/
        $data = $this->getCheckoutData($shoppingList);
        $action = $this->get('oro_action.action.run_action_group');
        $action->initialize($data['options']);
        $action->execute($data['context']);
        return $data['context']['redirectUrl'];
    }
    /**
     * @param ShoppingList $shoppingList
     * @return array
     */
    protected function getCheckoutData(ShoppingList $shoppingList)
    {
        return [
            'context' => new ActionData(['shoppingList' => $shoppingList]),
            'options' => [
                'action_group' => 'start_shoppinglist_checkout',
                'parameters_mapping' => [
                    'shoppingList' => $shoppingList,
                    'showErrors' => false,
                ],
                'results' => [
                    'redirectUrl' => new PropertyPath('redirectUrl'),
                ]
            ]
        ];
    }

}
