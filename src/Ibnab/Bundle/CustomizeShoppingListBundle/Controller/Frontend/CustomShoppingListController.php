<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\Controller\Frontend;

use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\ShoppingListBundle\Entity\ShoppingList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Oro\Bundle\ShoppingListBundle\Controller\Frontend\ShoppingListController as ParentController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\PricingBundle\SubtotalProcessor\TotalProcessorProvider;
use Oro\Bundle\ShoppingListBundle\Manager\CurrentShoppingListManager;
use Oro\Bundle\ShoppingListBundle\Manager\ShoppingListManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
/**
 * Controller that manages shopping lists.
 */
class CustomShoppingListController extends AbstractController {

    /**
     * @Route("/{id}", name="ibnab_customizeshoppinglist_list_frontend_view", defaults={"id" = null}, requirements={"id"="\d+"}, options= {"expose" = true})
     * @Layout
     * @Acl(
     *      id="oro_shopping_list_frontend_view",
     *      type="entity",
     *      class="OroShoppingListBundle:ShoppingList",
     *      permission="VIEW",
     *      group_name="commerce"
     * )
     *
     * @param ShoppingList $shoppingList
     * @return array
     *
     */
    public function viewAction(ShoppingList $shoppingList = null) {
        $shoppingListController = $this->get(ParentController::class);
        return $shoppingListController->viewAction($shoppingList);
    }

    /**
     * @Route("/{id}/{pid}", name="ibnab_customizeshoppinglist_item_frontend_view", defaults={"id" = null}, requirements={"id"="\d+","pid"="\d+"}, options= {"expose" = true})
     * @Layout
     * @Acl(
     *      id="oro_shopping_list_frontend_view",
     *      type="entity",
     *      class="OroShoppingListBundle:ShoppingList",
     *      permission="VIEW",
     *      group_name="commerce"
     * )
     *
     * @param ShoppingList $shoppingList
     * @return array
     *
     */
    public function viewItemAction(ShoppingList $shoppingList = null, $pid) {
        $product = null;
        if (!$shoppingList) {
            $shoppingList = $this->get(CurrentShoppingListManager::class)->getCurrent();
        }
        $allId = null;
        if ($shoppingList) {
            $this->get(ShoppingListManager::class)->actualizeLineItems($shoppingList);
            $title = $shoppingList->getLabel();
               $totalWithSubtotalsAsArray = $this->get(TotalProcessorProvider::class)
                ->getTotalWithSubtotalsAsArray($shoppingList);
            if ($pid) {
                $lineItems = $shoppingList->getLineItems();
                /** @var Product $parentProduct */
                foreach ($lineItems as $lineItem) {
                    $currentProduct = $lineItem->getProduct();
                    $currentParentProduct = $lineItem->getParentProduct();
                    if ($currentProduct->getId() == $pid || (!is_null($currentParentProduct) && $currentParentProduct->getId() == $pid)) {
                        $product = $lineItem;
                        break;
                    }
                }
            }
        } else {
            $title = null;
            $totalWithSubtotalsAsArray = [];
        }
        return [
            'data' => [
                'title' => $title,
                'entity' => $shoppingList,
                'item' => $product,
                'totals' => [
                    'identifier' => 'totals',
                    'data' => $totalWithSubtotalsAsArray
                ]
            ],
        ];
    }

}
