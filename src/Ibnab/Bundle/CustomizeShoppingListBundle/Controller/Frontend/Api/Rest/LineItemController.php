<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\Controller\Frontend\Api\Rest;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Oro\Bundle\ProductBundle\Form\Type\FrontendLineItemType;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Oro\Bundle\ShoppingListBundle\Entity\LineItem;
use Oro\Bundle\ShoppingListBundle\Form\Handler\LineItemHandler;
use Oro\Bundle\SoapBundle\Controller\Api\Rest\RestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Oro\Bundle\ShoppingListBundle\Controller\Frontend\Api\Rest\LineItemController as ParentLineItemController;
use Oro\Bundle\ActionBundle\Model\ActionData;
use Symfony\Component\PropertyAccess\PropertyPath;

/**
 * Controller for shopping list line item REST API requests.
 * @NamePrefix("oro_api_shopping_list_frontend_")
 */
class LineItemController extends ParentLineItemController {

    /**
     * @ApiDoc(
     *      description="Update Line Item",
     *      resource=true
     * )
     * @AclAncestor("oro_shopping_list_frontend_update")
     *
     * @param int $id
     *
     * @param Request $request
     * @return Response
     */
    public function putAction($id, Request $request) {
        /** @var LineItem $entity */
        $entity = $this->getManager()->find($id);
        if ($entity) {
            $form = $this->createForm(FrontendLineItemType::class, $entity, ['csrf_protection' => false]);
            $postParams = $request->request->all();
            $fromQuickPopup = isset($postParams['oro_product_frontend_line_item']['fromquickpopup']) ? $postParams['oro_product_frontend_line_item']['fromquickpopup'] : 0;
            $requestProductFromValue = $request->request->get("oro_product_frontend_line_item");
            unset($requestProductFromValue['fromquickpopup']);
            $handler = new LineItemHandler(
                    $form, $request, $this->getDoctrine(), $this->get('oro_shopping_list.manager.shopping_list'), $this->get('oro_shopping_list.manager.current_shopping_list'), $this->get('validator')
            );
            $isFormHandled = $handler->process($entity);
            $successResponse = ['unit' => $entity->getUnit()->getCode(), 'quantity' => $entity->getQuantity()];
            $config = $this->getConfigurationProvider();
            if ($config->isEnable()) {
                $shoppingList = $entity->getShoppingList();
                $successResponse['formAdd'] = true;
                $typeRedirect = $config->getRedirect();
                $enableFromOtherPopup = $config->isEnableFromPopup();
                if ($fromQuickPopup != 1 || $enableFromOtherPopup == 1) {
                    
                    if ($typeRedirect == 0) {
                        $successResponse['redirectUrl'] = $this->generateUrl('oro_shopping_list_frontend_view', ['id' => $shoppingList->getId()]);
                    } elseif ($typeRedirect == 1) {
                        $redirectUrl = $this->findCheckout($shoppingList);
                        if (!is_null($redirectUrl)) {
                            $successResponse['redirectUrl'] = $redirectUrl;
                        }
                    } elseif ($typeRedirect == 2) {
                        $successResponse['redirectUrl'] = $this->generateUrl('ibnab_customizeshoppinglist_list_frontend_view', ['id' => $shoppingList->getId()]);
                        $successResponse['popup'] = true;
                    } elseif ($typeRedirect == 3) {
                        $successResponse['redirectUrl'] = $this->generateUrl('ibnab_customizeshoppinglist_item_frontend_view', ['id' => $shoppingList->getId(), 'pid' => $entity->getProduct()->getId()]);
                        $successResponse['popup'] = true;
                    } else {
                        unset($successResponse['formAdd']);
                    }
                    if ($isFormHandled) {
                        $successResponse['successful'] = true;
                        $view = $this->view(
                                $successResponse , Codes::HTTP_OK
                        );
                        return $this->buildResponse($view, self::ACTION_UPDATE, ['id' => $id, 'entity' => $entity, 'successResponse' => $successResponse]);

                    } else {
                        $view = $this->view($form, Codes::HTTP_BAD_REQUEST);
                    }

                    if (!$this->isGranted('EDIT', $entity->getShoppingList())) {
                        $view = $this->view(null, Codes::HTTP_FORBIDDEN);
                    }
                }
            } else {
                
            }
        
        } else {
            $view = $this->view(null, Codes::HTTP_NOT_FOUND);
        }


        return $this->buildResponse($view, self::ACTION_UPDATE, ['id' => $id, 'entity' => $entity]);
    }

    protected function getConfigurationProvider() {
        return $this->get('ibnab.customizeshoppinglist.provider.configuration');
    }

    protected function findCheckout($shoppingList) {
        $data = $this->getCheckoutData($shoppingList);
        $action = $this->get('oro_action.action.run_action_group');
        $action->initialize($data['options']);
        $action->execute($data['context']);
        return $data['context']['redirectUrl'];
    }

    /**
     * @param ShoppingList $shoppingList
     * @return array
     */
    protected function getCheckoutData(ShoppingList $shoppingList) {
        return [
            'context' => new ActionData(['shoppingList' => $shoppingList]),
            'options' => [
                'action_group' => 'start_shoppinglist_checkout',
                'parameters_mapping' => [
                    'shoppingList' => $shoppingList,
                    'showErrors' => false,
                ],
                'results' => [
                    'redirectUrl' => new PropertyPath('redirectUrl'),
                ]
            ]
        ];
    }

}
