define(function (require) {
    'use strict';

    var MatrixGridOrderWidget;
    var routing = require('routing');
    var FrontendDialogWidget = require('orofrontend/js/app/components/frontend-dialog-widget');
    var headerTemplate = require('tpl-loader!oroproduct/templates/product-popup-header.html');
    var mediator = require('oroui/js/mediator');
    var _ = require('underscore');

    MatrixGridOrderWidget = FrontendDialogWidget.extend({
        optionNames: FrontendDialogWidget.prototype.optionNames.concat([
            'shoppingListId'
        ]),
        shoppingListId: null,
        enableFlyCart: 1,
        enableUpdatePopup: 1,
        enableUpdateFly: 0,
        fromUpdate: 0,
        img: null,
        /**
         * @inheritDoc
         */
        constructor: function MatrixGridOrderWidget() {
            MatrixGridOrderWidget.__super__.constructor.apply(this, arguments);
        },
        /**
         * @inheritDoc
         */
        initialize: function (options) {
            this.model = this.model || options.productModel;

            var urlOptions = {
                productId: this.model.get('id'),
                shoppingListId: this.shoppingListId
            };

            if (_.isDesktop()) {
                options.actionsEl = '.product-totals';
                options.moveAdoptedActions = false;
            }
            this.enableFlyCart = options.model.attributes.enableFlyCart;
            this.enableUpdatePopup = options.model.attributes.enableUpdatePopup;
            this.enableUpdateFly = options.model.attributes.enableUpdateFly;
            this.fromUpdate = options.fromUpdate;

            options.url = routing.generate('oro_shopping_list_frontend_matrix_grid_order', urlOptions);
            options.preventModelRemoval = true;
            options.regionEnabled = false;
            options.incrementalPosition = false;
            options.dialogOptions = {
                modal: true,
                title: null,
                resizable: false,
                width: 'auto',
                autoResize: true,
                dialogClass: 'matrix-order-widget--dialog'
            };
            options.initLayoutOptions = {
                productModel: this.model
            };
            options.header = headerTemplate({
                product: this.model.attributes
            });

            MatrixGridOrderWidget.__super__.initialize.apply(this, arguments);
        },
        _onAdoptedFormSubmitClick: function ($form) {
            //var emptyMatrixAllowed = $form.data('empty-matrix-allowed');
            var isQuantity = $form.find('[data-name="field__quantity"]').filter(function () {
                return this.value.length > 0;
            }).length > 0;

            //if (!emptyMatrixAllowed && !isQuantity) {
            if (!isQuantity) {
                var validator = $form.validate();
                validator.errorsFor($form[0]).remove();
                validator.showLabel($form[0], _.__('oro.product.validation.configurable.required'));
                return false;
            }
            var $button = $($form).find(':submit');
            this.img = $button.closest('.matrix-order-widget--dialog').find("img").eq(0);
            var offset = this.img.offset();
            //console.log($(button).closest('.product-item__box'));

            if (offset === undefined || offset === null) {
                this.img = $button.closest('.matrix-order-widget--dialog').find("img").eq(0);
                offset = this.img.offset();
            }
            mediator.execute('showLoading');
            if (this.enableUpdateFly) {
                this._flyShoppingCart();
            }
            return MatrixGridOrderWidget.__super__._onAdoptedFormSubmitClick.apply(this, arguments);
        },
        _onContentLoad: function (content) {
            if (_.isObject(content)) {
                content.enableFlyCart = this.enableFlyCart;
                content.enableUpdatePopup = this.enableUpdatePopup;
                content.enableUpdateFly = this.enableUpdateFly;
                content.fromUpdate = this.fromUpdate;

                /*
                 $.each($(".ui-dialog"), function ()
                 {
                 var $dialog;
                 $dialog = $(this).children(".matrix-order-widget--dialog");
                 if ($dialog.dialog("option", "modal"))
                 {
                 $dialog.dialog('close');
                 //$(this).children(".ui-dialog-titlebar-close").click();
                 //$(this).children(".ui-dialog-titlebar-close").trigger('click');
                 //$(".matrix-order-widget--dialog").children('.ui-dialog-content').dialog('close');
                 //$(".matrix-order-widget--dialog").dialog('close');
                 //$(".ui-widget-overlay").css('display', 'none');
                 //$('input').blur();
                 //$(this).children(".ui-dialog-titlebar-close").trigger('click'); 
                 //$(this).children(".ui-dialog-titlebar-close").click();
                 //self.subview('popup').once('frontend-dialog:close', _.bind(this.changeCheckboxState, this, false));   
                 }
                 });*/

                //this.remove();
                mediator.trigger('shopping-list:line-items:update-response', this.model, content);
                try {
                    //this.remove();
                    //$(".matrix-order-widget--dialog").find('.ui-dialog-titlebar-close').trigger("click");
                    $(".matrix-order-widget--dialog").css('display', 'none');
                    $(".ui-widget-overlay").css('display', 'none');
                }
                catch (err) {

                }
                
                //

            } else {
                return MatrixGridOrderWidget.__super__._onContentLoad.apply(this, arguments);
            }
        },
        _flyShoppingCart: function () {

            if (this.enableFlyCart) {

                var cart = $('.cart-widget__content');
                var imgtodrag = this.img;
                var offset = imgtodrag.offset();
                if (offset !== undefined && offset !== null) {
                    var imgclone = imgtodrag.clone()
                            .offset({
                                top: imgtodrag.offset().top,
                                left: imgtodrag.offset().left
                            })
                            .css({
                                'opacity': '0.5',
                                'position': 'absolute',
                                'height': '150px',
                                'width': '150px',
                                'z-index': '100'
                            })
                            .appendTo($('body'))
                            .animate({
                                'top': cart.offset().top + 10,
                                'left': cart.offset().left + 10,
                                'width': 75,
                                'height': 75
                            }, 1000, 'easeInOutExpo');
                    $("html, body").animate({
                        scrollTop: 0
                    }, 1000, 'easeInOutExpo');
                    setTimeout(function () {

                        cart.effect("shake", {
                            times: 2
                        }, 200);
                    }, 1500);

                    imgclone.animate({
                        'width': 0,
                        'height': 0
                    }, function () {
                        //$(button).detach()
                    });
                }
            }

        }
    });

    return MatrixGridOrderWidget;
});
