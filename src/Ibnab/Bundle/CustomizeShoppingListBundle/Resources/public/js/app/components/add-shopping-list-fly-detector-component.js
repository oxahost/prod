define(function (require) {
    'use strict';

    var AddShoppingListFlyDetectorComponent;
    var BaseComponent = require('oroui/js/app/components/base/component');
    //var mediator = require('oroui/js/mediator');
    var routing = require('routing');
    var _ = require('underscore');

     AddShoppingListFlyDetectorComponent = BaseComponent.extend({
        /**
         * @inheritDoc
         */
        constructor: function AddShoppingListFlyDetectorComponent() {
            AddShoppingListFlyDetectorComponent.__super__.constructor.apply(this, arguments);
        },
        /**
         * @constructor
         * @param {Object} options
         */
        initialize: function (options) {
            this.options = _.defaults(options || {}, this.options);
            /*$('.add-to-shopping-list-button').click(function (e) {
                alert('ok');               
            });*/
            
        }
    });

    return AddShoppingListFlyDetectorComponent;
});
