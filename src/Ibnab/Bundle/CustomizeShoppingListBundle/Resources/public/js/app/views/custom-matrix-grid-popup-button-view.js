define(function(require) {
    'use strict';

    var MatrixGridPopupButtonView;
    var ProductAddToShoppingListView = require('oroshoppinglist/js/app/views/product-add-to-shopping-list-view');
    var MatrixGridOrderWidget = require('ibnabcustomizeshoppinglist/js/app/widget/custom-matrix-grid-order-widget');

    MatrixGridPopupButtonView = ProductAddToShoppingListView.extend({
        /**
         * @inheritDoc
         */
        constructor: function MatrixGridPopupButtonView() {
            MatrixGridPopupButtonView.__super__.constructor.apply(this, arguments);
        },

        _openMatrixGridPopup: function(shoppingListId,fromUpdate) {
            this.subview('popup', new MatrixGridOrderWidget({
                model: this.model,
                shoppingListId: shoppingListId,
                fromUpdate: fromUpdate
            }));
            this.subview('popup').render();
        },

        _saveLineItem: function(url, urlOptions, formData) {
            var fromUpdate = 1;
            this._openMatrixGridPopup(urlOptions.shoppingListId,fromUpdate);
            
        },

        _addLineItem: function(url, urlOptions, formData) {
            var fromUpdate = 0;
            this._openMatrixGridPopup(urlOptions.shoppingListId,fromUpdate);
        }
    });

    return MatrixGridPopupButtonView;
});
