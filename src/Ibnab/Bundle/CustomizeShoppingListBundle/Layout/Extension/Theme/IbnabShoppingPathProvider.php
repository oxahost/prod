<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\Layout\Extension\Theme;

use Oro\Component\Layout\ContextAwareInterface;
use Oro\Component\Layout\ContextInterface;
use Oro\Component\Layout\Extension\Theme\PathProvider\PathProviderInterface;

class IbnabShoppingPathProvider implements PathProviderInterface, ContextAwareInterface {

    /** @var ContextInterface */
    protected $context;

    /**
     * {@inheritdoc}
     */
    public function setContext(ContextInterface $context) {
        $this->context = $context;
    }

    /**
     * {@inheritdoc}
     */
    public function getPaths(array $existingPaths) {

        $routeName = $this->context->getOr('route_name');
        $paths = [];
        if ($routeName && $routeName == "ibnab_customizeshoppinglist_list_frontend_view" || $routeName == "ibnab_customizeshoppinglist_item_frontend_view") {
            $routeName = 'oro_shopping_list_frontend_view';
            foreach ($existingPaths as $path) {
                $paths[] = $path;
                $paths[] = implode(self::DELIMITER, [$path, $routeName]);
            }
            //var_dump($paths);die();
            return $paths;
            
        }
            //var_dump($existingPaths);die();
        return $existingPaths;
    }

}
