<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\Layout\Extension;

use Oro\Component\Layout\ContextConfiguratorInterface;
use Oro\Component\Layout\ContextInterface;
use Ibnab\Bundle\CustomizeShoppingListBundle\Layout\DataProvider\ConfigManager;

class IbnabCustomizeShoppingList implements ContextConfiguratorInterface
{
    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }
    /**
     * {@inheritdoc}
     */
    public function configureContext(ContextInterface $context)
    {
        $context->getResolver()
            ->setRequired(['ibnab_shoppinglistquickview'])
            ->setAllowedTypes('ibnab_shoppinglistquickview', ['numeric']);
        $context->set('ibnab_shoppinglistquickview', $this->configManager->getIsShoppingListQuickView());
        $context->getResolver()
            ->setRequired(['ibnab_isenable'])
            ->setAllowedTypes('ibnab_isenable', ['numeric']);
        $context->set('ibnab_isenable', $this->configManager->isEnable());
    }

}
