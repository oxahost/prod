<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\Layout\DataProvider;

use Ibnab\Bundle\CustomizeShoppingListBundle\Layout\DataProvider\ConfigManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LayoutConfig
{
    /**
     * @var ConfigManager
     */
    protected $configurationProvider;
    /** @var ContainerInterface */
    protected $container;

    
    
    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configurationProvider,ContainerInterface $container)
    {
        $this->configurationProvider = $configurationProvider;
        $this->container = $container;
    }
    /**
     *
     * @return int
     */
    public function isEnable() {
        return $this->configurationProvider->isEnable();
    } 
    /**
     *
     * @return int
     */
    public function isPopup() {
        return $this->configurationProvider->isPopup();
    } 
    /**
     *
     * @return int
     */
    public function isEnableFlyCart()
    {       
      return $this->configurationProvider->isEnableFlyCart();
    }
    /**
     *
     * @return int
     */
    public function isEnableUpdatePopup()
    {       
        return $this->configurationProvider->isEnableUpdatePopup();   
    }
    /**
     *
     * @return int
     */
    public function isEnableUpdateFlyCart()
    {       
        return $this->configurationProvider->isEnableUpdateFlyCart();    
    }
}
