<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\Layout\DataProvider;

use Ibnab\Bundle\CustomizeShoppingListBundle\Provider\ConfigurationProvider;
use Symfony\Component\HttpFoundation\RequestStack;

class ConfigManager
{
    /**
     * @var ConfigManager
     */
    protected $configProvider;
    protected $requestStack;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigurationProvider $configProvider,RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->configProvider = $configProvider;
    }
    public function getIsShoppingListQuickView()
    {       
       $request = $this->requestStack->getCurrentRequest();   
       $type = $request->query->get("type");
       if($type == "shopping-quick-view-popup"){
           return 1;
       }
       return 0;
    }  
    /**
     *
     * @return int
     */
    public function isPopup()
    {       
      $isPopup =  $this->configProvider->getRedirect();
      if($isPopup ==2 || $isPopup ==3){
         return 1; 
      }
      return 0;
    } 
    /**
     *
     * @return int
     */
    public function isEnable()
    {       
      return $this->configProvider->isEnable();
    } 
    /**
     *
     * @return int
     */
    public function isEnableFlyCart()
    {       
      return $this->configProvider->isEnableFlyCart();
    }  
    /**
     *
     * @return int
     */
    public function isEnableUpdatePopup()
    {       
        return $this->configProvider->isEnableUpdatePopup();   
    }
    /**
     *
     * @return int
     */
    public function isEnableUpdateFlyCart()
    {       
        return $this->configProvider->isEnableUpdateFlyCart();    
    }
}
