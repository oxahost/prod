<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ibnab_customize_shopping_list');
        SettingsBuilder::append(
            $rootNode,
            array(
                'enable'   => ['value' => 1],
                'redirect'   => ['value' => 2],
                'enablefrompoup'   => ['value' => 0],
                'enableflycart'   => ['value' => 1],
                'enablepoupupdate'   => ['value' => 1],
                'enableflycartupdate'   => ['value' => 0],
            )
        );
        return $treeBuilder;
    }

    
}
