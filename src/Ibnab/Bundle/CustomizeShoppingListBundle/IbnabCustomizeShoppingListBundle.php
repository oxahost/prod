<?php

namespace Ibnab\Bundle\CustomizeShoppingListBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IbnabCustomizeShoppingListBundle extends Bundle
{
    public function getParent()
    {
        return 'OroShoppingListBundle';
    }
}
