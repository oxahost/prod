<?php

namespace Webkul\POMBundle\Migrations\Schema;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\MigrationBundle\Migration\Installation;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class WebkulPOMBundleInstaller implements Installation
{
    /**
     * {@inheritdoc}
     */
    public function getMigrationVersion()
    {
        return 'v1_0';
    }

    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        /** Tables generation **/
        $this->createPurchaseorderTable($schema);
        $this->createPurchaseorderproductTable($schema);
        $this->createPurchaseordershippingaddressTable($schema);
        $this->createReceiveinvoiceTable($schema);
        $this->createSupplierTable($schema);
        $this->createSupplierproductTable($schema);

        /** Foreign keys generation **/
    }

    /**
     * Create PurchaseOrder table
     *
     * @param Schema $schema
     */
    protected function createPurchaseorderTable(Schema $schema)
    {
        $table = $schema->createTable('PurchaseOrder');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('supplierId', 'integer', []);
        $table->addColumn('totalProducts', 'integer', []);
        $table->addColumn('totalAmount', 'float', ['length' => 0]);
        $table->addColumn('currency', 'string', ['length' => 255]);
        $table->addColumn('status', 'string', ['length' => 20]);
        $table->addColumn('createdAt', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updatedAt', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('invoiceNo', 'string', ['length' => 255]);
        $table->setPrimaryKey(['id']);
    }

    /**
     * Create PurchaseOrderProduct table
     *
     * @param Schema $schema
     */
    protected function createPurchaseorderproductTable(Schema $schema)
    {
        $table = $schema->createTable('PurchaseOrderProduct');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('purchaseOrderId', 'integer', []);
        $table->addColumn('productId', 'integer', []);
        $table->addColumn('quantity', 'integer', []);
        $table->addColumn('price', 'float', ['length' => 0]);
        $table->addColumn('productName', 'string', ['length' => 255]);
        $table->addColumn('sku', 'string', ['length' => 255]);
        $table->addColumn('received', 'integer', []);
        $table->setPrimaryKey(['id']);
    }

    /**
     * Create PurchaseOrderShippingAddress table
     *
     * @param Schema $schema
     */
    protected function createPurchaseordershippingaddressTable(Schema $schema)
    {
        $table = $schema->createTable('PurchaseOrderShippingAddress');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('firstname', 'string', ['length' => 255]);
        $table->addColumn('lastname', 'string', ['length' => 255]);
        $table->addColumn('email', 'string', ['length' => 255]);
        $table->addColumn('contactNo', 'string', ['length' => 32]);
        $table->addColumn('address1', 'string', ['length' => 1000]);
        $table->addColumn('address2', 'string', ['notnull' => false, 'length' => 1000]);
        $table->addColumn('city', 'string', ['length' => 500]);
        $table->addColumn('state', 'string', ['length' => 500]);
        $table->addColumn('country', 'string', ['length' => 500]);
        $table->addColumn('purchaseOrderId', 'integer', []);
        $table->setPrimaryKey(['id']);
    }

    /**
     * Create ReceiveInvoice table
     *
     * @param Schema $schema
     */
    protected function createReceiveinvoiceTable(Schema $schema)
    {
        $table = $schema->createTable('ReceiveInvoice');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('invoiceNo', 'string', ['length' => 255]);
        $table->addColumn('date', 'date', ['notnull' => false, 'length' => 0, 'comment' => '(DC2Type:date)']);
        $table->addColumn('paymentDueDate', 'date', ['notnull' => false, 'length' => 0, 'comment' => '(DC2Type:date)']);
        $table->addColumn('purchaseOrderId', 'integer', []);
        $table->setPrimaryKey(['id']);
    }

    /**
     * Create Supplier table
     *
     * @param Schema $schema
     */
    protected function createSupplierTable(Schema $schema)
    {
        $table = $schema->createTable('Supplier');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('name', 'string', ['length' => 255]);
        $table->addColumn('email', 'string', ['length' => 255]);
        $table->addColumn('website', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('supplierNumber', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('ownerName', 'string', ['length' => 255]);
        $table->addColumn('gender', 'boolean', ['notnull' => false]);
        $table->addColumn('telephone', 'integer', []);
        $table->addColumn('fax', 'integer', ['notnull' => false]);
        $table->addColumn('street', 'string', ['length' => 255]);
        $table->addColumn('city', 'string', ['length' => 255]);
        $table->addColumn('postcode', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('country', 'string', ['length' => 10]);
        $table->addColumn('state', 'string', ['length' => 10]);
        $table->addColumn('status', 'string', ['length' => 15]);
        $table->addColumn('createdAt', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updatedAt', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
    }

    /**
     * Create SupplierProduct table
     *
     * @param Schema $schema
     */
    protected function createSupplierproductTable(Schema $schema)
    {
        $table = $schema->createTable('SupplierProduct');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('productId', 'integer', []);
        $table->addColumn('supplierId', 'integer', []);
        $table->addColumn('price', 'float', ['notnull' => false, 'length' => 0]);
        $table->addColumn('status', 'string', ['notnull' => false, 'length' => 20]);
        $table->setPrimaryKey(['id']);
    }
}
