<?php

namespace Webkul\POMBundle\Provider;

class PurchaseOrderStatusProvider
{
    /**
     * @return array
     */
    public function getAvailablePurchaseOrderStatuses()
    {
        return [
            'Pending PO' => 'pending',
            'Placed with Supplier' => 'placed',
            'Products received' => 'products_received',
            'Invoice received' => 'invoice_received'
        ];
    }
}
