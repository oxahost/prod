<?php

namespace Webkul\POMBundle\Provider;

use Webkul\POMBundle\Entity\Supplier;

class SupplierStatusProvider
{
    /**
     * @return array
     */
    public function getAvailableSupplierStatuses()
    {
        return [
            'oro.pom.general.status.disabled' => Supplier::STATUS_DISABLED,
            'oro.pom.general.status.enabled' => Supplier::STATUS_ENABLED,
        ];
    }
}
