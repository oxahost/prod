<?php

namespace Webkul\POMBundle\Provider;

class InputFieldProvider
{
    /**
     * @return array
     */
    public function getInputField()
    {
        $form = $this->createForm(\Webkul\POMBundle\Form\Type\InputType::class, [], []);
        return ['form' => $form->createView()];
    }
}
