<?php

namespace Webkul\POMBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ConstraintsPurchaseOrderAddressValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ConstraintsPurchaseOrderAddress) {
            throw new UnexpectedTypeException($constraint, ConstraintsPurchaseOrderAddress::class);
        }
        
        foreach ($value as $key => $address) {
            if (is_array($address)) {
                if (!preg_match('/^[a-zA-Z0-9]+$/', $address->getFirstName(), $matches)) {
                    $this->context->buildViolation($constraint->message)->addViolation();
                }
            }
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (!is_array($value)) {
            $this->context->buildViolation($constraint->message)->addViolation();
            // return;
        }

        // if (!is_string($value)) {
        //     throw new UnexpectedTypeException($value, 'string');
        // }

        // if (!preg_match('/^[a-zA-Z0-9]+$/', $value, $matches)) {
        //     $this->context->buildViolation($constraint->message)->addViolation();
        // }
    }
}


?>