<?php

namespace Webkul\POMBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintsPurchaseOrderAddress extends Constraint
{
    public $message = 'Shipping address is not filled properly for purchase order!';
}

?>