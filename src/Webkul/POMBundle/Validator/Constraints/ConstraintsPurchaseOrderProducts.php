<?php

namespace Webkul\POMBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintsPurchaseOrderProducts extends Constraint
{
    public $message = 'There are no products for purchase order';
}

?>