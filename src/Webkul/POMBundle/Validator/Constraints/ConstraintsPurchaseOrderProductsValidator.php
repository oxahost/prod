<?php

namespace Webkul\POMBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ConstraintsPurchaseOrderProductsValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ConstraintsPurchaseOrderProducts) {
            throw new UnexpectedTypeException($constraint, ConstraintsPurchaseOrderProducts::class);
        }
        
        if (!is_array($value->getValues()) || empty($value->getValues())) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

?>