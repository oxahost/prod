<?php

namespace Webkul\POMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\EmailBundle\Entity\Email;
use Webkul\POMBundle\Form\Type\ConfigType;

/**
 * @Route("/config")
 */
class ConfigController extends Controller
{
    /**
     * @Route("/", name="pom_config")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $data = array();
        $form = $this->createForm(ConfigType::class, [], []);
        $configManager = $this->container->get('oro_config.global');
        
        // if ($this->get('oro_config.form.handler.config')->process($form, $request)) {
        //     $this->get('session')->getFlashBag()->add(
        //         'success',
        //         $this->get('translator')->trans('oro.pom.config.success_config_save')
        //     );

        //     $form->setData($this->get('oro_config.global')->getSettingsByForm($form));
        // }

        return [
            'form' => $form->createView(),
        ];
    }
}
