<?php

namespace Webkul\POMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Webkul\POMBundle\Entity\Supplier;

/**
 * @Route("/supplier")
 */
class SupplierController extends Controller
{
    /**
     * @Route("/", name="pom_supplier")
     * @Template()
     */
    public function indexAction()
    {
        return array('gridName' => 'supplier-grid');
    }

    /**
     * @Route("/view/{id}", name="pom_supplier_view", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("pom_supplier_view")
     */
    public function viewAction(Supplier $supplier)
    {
        
        return array('entity' => $supplier);
    }

    /**
     * @Route("/info/{id}", name="pom_supplier_info", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("pom_supplier_view")
     */
    public function infoAction(Supplier $supplier)
    {
        return array('entity' => $supplier);
    }

    /**
     * Create supplier form
     *
     * @Route("/create", name="pom_supplier_create")
     * @Template("WebkulPOMBundle:Supplier:update.html.twig")
     * @Acl(
     *      id="pom_supplier_create",
     *      type="entity",
     *      class="WebkulPOMBundle:Supplier",
     *      permission="CREATE"
     * )
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function createAction(Request $request)
    {
        return $this->update(new Supplier(), $request);
    }

    /**
     * Edit order form
     *
     * @Route("/update/{id}", name="pom_supplier_update", requirements={"id"="\d+"})
     * @Template
     * @Acl(
     *      id="pom_supplier_update",
     *      type="entity",
     *      class="WebkulPOMBundle:Supplier",
     *      permission="EDIT"
     * )
     *
     * @param Order $supplier
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function updateAction(Supplier $supplier, Request $request)
    {
        return $this->update($supplier, $request);
    }

    private function update(Supplier $supplier, Request $request)
    {
        $form = $this->createForm(\Webkul\POMBundle\Form\Type\SupplierType::class, $supplier, ['entityManager' => $this->getDoctrine()->getManager()]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $supplier->setUpdatedAt(new \DateTime());
            if($supplier->getId() == '') {
                $this->get('session')->getFlashBag()->add('success', 'You have successfully added Supplier.');
                $supplier->setCreatedAt(new \DateTime());
            } else {
                $this->get('session')->getFlashBag()->add('success', 'You have successfully updated Supplier.');
                $supplier->setUpdatedAt(new \DateTime());
            }
            
            $entityManager->persist($supplier);
            $entityManager->flush();

            return $this->get('oro_ui.router')->redirectAfterSave(
                array(
                    'route' => 'pom_supplier_update',
                    'parameters' => array('id' => $supplier->getId()),
                ),
                array('route' => 'pom_supplier'),
                $supplier
            );
        }

        return array(
            'entity' => $supplier,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/list", name="pom_supplier_list")
     * @AclAncestor("pom_supplier")
     * @Template
     */
    public function listAction()
    {
        return [];
    }

}
