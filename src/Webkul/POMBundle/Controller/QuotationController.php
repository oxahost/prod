<?php

namespace Webkul\POMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/quotation")
 */
class QuotationController extends Controller
{
    /**
     * @Route("/", name="pom_quotation")
     * @Template()
     */
    public function indexAction()
    {
        return $this->render('WebkulPOMBundle:Quotation:index.html.twig');
    }
}
