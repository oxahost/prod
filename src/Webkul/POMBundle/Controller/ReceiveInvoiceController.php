<?php

namespace Webkul\POMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Webkul\POMBundle\Entity\PurchaseOrder;
use Webkul\POMBundle\Entity\ReceiveInvoice;
use Webkul\POMBundle\Form\Type\ReceiveInvoiceType;

/**
 * @Route("/receiveinvoice")
 */
class ReceiveInvoiceController extends Controller
{
    /**
     * @Route("/create/{id}", name="pom_purchaseorder_receiveinvoice_create", requirements={"id"="\d+"})
     * @Template("WebkulPOMBundle:ReceiveInvoice:update.html.twig")
     * @AclAncestor("pom_purchaseorder")
     */
    public function createAction(Request $request, PurchaseOrder $purchaseOrder)
    {
        $receiveInvoice = new ReceiveInvoice();
        $receiveInvoice->setPurchaseOrderId($purchaseOrder->getId());
        return $this->update($receiveInvoice, $purchaseOrder, $request);
    }

    /**
     * @Route("/update/{id}", name="pom_purchaseorder_receiveinvoice_update", requirements={"id"="\d+"})
     * @Template()
     * @AclAncestor("pom_purchaseorder")
     */
    public function updateAction(Request $request, ReceiveInvoice $receiveInvoice)
    {
        $manager = $this->getDoctrine()->getManager();
        $purchaseOrder = $manager->getRepository(PurchaseOrder::class)->find($receiveInvoice->getPurchaseOrderId());
        return $this->update($receiveInvoice, $purchaseOrder, $request);
    }

    public function update($receiveInvoice, $purchaseOrder, $request) {
        $form = $this->createForm(ReceiveInvoiceType::class, $receiveInvoice);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('session')->getFlashBag()->add('success', 'You have successfully logged invoice receiving.');

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($receiveInvoice);
            $manager->flush();

            $purchaseOrder->setInvoiceNo($receiveInvoice->getId());
            $purchaseOrder->setStatus('invoice_received');
            $manager->persist($purchaseOrder);
            $manager->flush();

            return $this->get('oro_ui.router')->redirectAfterSave(
                array(
                    'route' => 'pom_purchaseorder_view',
                    'parameters' => array('id' => $purchaseOrder->getId()),
                ),
                array(
                    'route' => 'pom_purchaseorder_view',
                    'parameters' => array('id' => $purchaseOrder->getId()),
                ),
                $purchaseOrder
            );
        }

        return [
            'entity' => $purchaseOrder,
            'form' => $form->createView()
        ];
    }

}

?>