<?php

namespace Webkul\POMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Oro\Bundle\EmailBundle\Form\Model\Email;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Webkul\POMBundle\Entity\PurchaseOrder;
use Webkul\POMBundle\Entity\PurchaseOrderProduct;
use Webkul\POMBundle\Entity\PurchaseOrderShippingAddress;
use Webkul\POMBundle\Entity\Supplier;
use Webkul\POMBundle\Entity\ReceiveInvoice;
use Webkul\POMBundle\Form\Type\PurchaseOrderType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("/purchaseorder")
 */
class PurchaseOrderController extends Controller
{
    /**
     * @Route("/", name="pom_purchaseorder")
     * @Template()
     */
    public function indexAction(Request $request)
    {       
        return array('gridName' => 'purchase-order-grid');
    }

    /**
     * @Route("/view/{id}", name="pom_purchaseorder_view", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("pom_purchaseorder_view")
     */
    public function viewAction(PurchaseOrder $purchaseOrder, Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $supplier = $manager->getRepository(Supplier::class)->findById($purchaseOrder->getSupplierId());
        $products = $this->getPurchaseOrderProducts($purchaseOrder->getId());
        $purchaseOrder->setProducts(new ArrayCollection($products));
        // ReceiveInvoice

        return $this->update($supplier[0], $purchaseOrder, $request);
    }

    /**
     * @Route("/info/{id}", name="pom_purchaseorder_info", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("pom_purchaseorder_view")
     */
    public function infoAction(PurchaseOrder $purchaseOrder)
    {
        $purchaseOrderStatusProvider = $this->container->get('pom_supplier.provider.purchase_order_status_provider');
        $purchaseOrderStatuses = $purchaseOrderStatusProvider->getAvailablePurchaseOrderStatuses();
        return array('purchaseOrder' => $purchaseOrder, 'purchaseOrderStatuses' => $purchaseOrderStatuses);
    }

    /**
     * @Route("/list", name="pom_purchaseorder_list")
     * @Template()
     * @AclAncestor("pom_purchaseorder_view")
     */
    public function listAction(Request $request)
    {
        return [
            'supplierId' => $request->get('supplierId'),
        ];
    }

    /**
     * Create purchaseOrder form
     *
     * @Route("/create/{id}", name="pom_purchaseorder_create", requirements={"id"="\d+"})
     * @Template("WebkulPOMBundle:PurchaseOrder:update.html.twig")
     * @AclAncestor("pom_purchaseorder")
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function createAction(Supplier $supplier, Request $request)
    {
        $purchaseOrder = new PurchaseOrder();
        $purchaseOrder->setSupplierId($supplier->getId());
        
        $address = new PurchaseOrderShippingAddress();
        $address->setFirstName('');
        $purchaseOrder->getAddress()->add($address);

        return $this->update($supplier, $purchaseOrder , $request);
    }

    /**
     * Create purchaseOrder form
     *
     * @Route("/update/{id}", name="pom_purchaseorder_update", requirements={"id"="\d+"})
     * @Template
     * @Acl(
     *      id="pom_purchaseorder_update",
     *      type="entity",
     *      class="WebkulPOMBundle:PurchaseOrder",
     *      permission="EDIT"
     * )
     *
     * @param Order $purchaseOrder
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function updateAction(PurchaseOrder $purchaseOrder, Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $supplier = $manager->getRepository(Supplier::class)->find($purchaseOrder->getSupplierId());
        $products = $this->getPurchaseOrderProducts($purchaseOrder->getId());
        $purchaseOrder->setProducts(new ArrayCollection($products));

        $address = $this->getPurchaseOrderAddress($purchaseOrder->getId());        
        $purchaseOrder->setAddress(new ArrayCollection($address));
        return $this->update($supplier, $purchaseOrder, $request);
    }

    public function update($supplier,$purchaseOrder,$request)
    {
        $form = $this->createForm(PurchaseOrderType::class, $purchaseOrder, ['entityManager' => $this->getDoctrine()->getManager()]);
        $form->handleRequest($request);
        $purchaseOrderStatusProvider = $this->container->get('pom_supplier.provider.purchase_order_status_provider');
        $purchaseOrderStatuses = $purchaseOrderStatusProvider->getAvailablePurchaseOrderStatuses();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $purchaseOrder->setInvoiceNo('');
            $purchaseOrder->setCurrency('USD');
            $purchaseOrder->setUpdatedAt(new \DateTime());
            if($purchaseOrder->getId() == '') {
                $this->get('session')->getFlashBag()->add('success', 'You have successfully added Purchase Order.');
                $purchaseOrder->setStatus('pending');
                $purchaseOrder->setCreatedAt(new \DateTime());
            } else {
                $this->get('session')->getFlashBag()->add('success', 'You have successfully updated Purchase Order.');
                $purchaseOrder->setUpdatedAt(new \DateTime());
            }
            
            $entityManager->persist($purchaseOrder);
            $entityManager->flush();
            $purchaseOrderId = $purchaseOrder->getId();
            
            foreach ($purchaseOrder->getProducts() as $key => $product) {
                $product->setPurchaseOrderId($purchaseOrderId);
                $product->setReceived(0);
                $entityManager->persist($product);
                $entityManager->flush();
            }

            foreach ($purchaseOrder->getAddress() as $key => $address) {
                $address->setPurchaseOrderId($purchaseOrderId);
                $entityManager->persist($address);
                $entityManager->flush();
            }

            return $this->get('oro_ui.router')->redirectAfterSave(
                array(
                    'route' => 'pom_purchaseorder_update',
                    'parameters' => array('id' => $purchaseOrder->getId()),
                ),
                array(
                    'route' => 'pom_purchaseorder_view',
                    'parameters' => array('id' => $purchaseOrder->getId()),
                ),
                $purchaseOrder
            );
        }
        
        return array(
            'entity' => $purchaseOrder,
            'supplier' => $supplier,
            'purchaseOrderStatuses' => $purchaseOrderStatuses,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/invoice/{id}", name="pom_purchaseorder_invoice", requirements={"id"="\d+"})
     * @Template()
     * @AclAncestor("pom_purchaseorder")
     */
    public function invoiceAction(PurchaseOrder $purchaseOrder)
    {
        return ['purchaseOrder' => $purchaseOrder];
    }

    /**
     * @Route("/sendinvoice/{id}", name="pom_purchaseorder_send_invoice", requirements={"id"="\d+"})
     * @Template()
     * @AclAncestor("pom_purchaseorder")
     */
    public function sendInvoiceAction(PurchaseOrder $purchaseOrder)
    {
        $purchaseOrder->setProducts = new ArrayCollection($this->productsAction($purchaseOrder));

        $manager = $this->getDoctrine()->getManager();
        $supplier = $manager->getRepository(Supplier::class)->find($purchaseOrder->getSupplierId());

        $html = $this->renderView('WebkulPOMBundle:Emails:invoice.html.twig',array('entity' => $purchaseOrder));

        $template = $this->container->get('oro_config.global')->get('oro_email.signature');
        $template = str_replace('{supplier_name}', $supplier->getOwnerName(), $template);
        $template = str_replace('{company_name}', $supplier->getName(), $template);
        $template = str_replace('{purchase_order}', $html, $template);       

        $email = new Email();
        $email->setFrom($this->container->get('oro_config.global')->get('oro_notification.email_notification_sender_email'));
        $email->setTo(array($supplier->getEmail()));
        $email->setCc(array($this->container->get('oro_config.global')->get('oro_notification.email_notification_sender_email')));
        $email->setSubject('Purchase Order Invoice - #' . $purchaseOrder->getId());
        $email->setType('html');
        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Purchase Order Invoice - #' . $purchaseOrder->getId().'</title></head><body><div class="content">'.html_entity_decode($template, ENT_QUOTES, 'UTF-8').'</div></body></html>';
        $email->setBody($template);
        $processor = $this->get('pom_bundle.service.mailer');
        $processor->sendMail($email);
        
        $purchaseOrder->setStatus('placed');
        $manager->persist($purchaseOrder);
        $manager->flush();

        return new JsonResponse(['status' => true, 'msg' => 'Email sent']);
    }

    /**
     * @Template("WebkulPOMBundle:PurchaseOrder:invoice.html.twig")
     * @AclAncestor("pom_purchaseorder")
     */
    public function getInvoiceView($purchaseOrder)
    {
        return ['purchaseOrder' => $purchaseOrder];
    }

    /**
     * @Route("/products/{id}", name="pom_purchaseorder_products", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("pom_purchaseorder")
     */
    public function productsAction(PurchaseOrder $purchaseOrder)
    {
        $products = $this->getPurchaseOrderProducts($purchaseOrder->getId());
        return array('products' => $products);
    }

    public function getPurchaseOrderProducts($purchaseOrderId) {
        $manager = $this->getDoctrine()->getManager();
        $purchaseOrderProducts = $manager->getRepository(PurchaseOrderProduct::class);
        $query = $purchaseOrderProducts->createQueryBuilder('pop')
            ->where("pop.purchaseOrderId = '".$purchaseOrderId."'")
            ->getQuery();
        return $query->getResult();
    }

    public function getPurchaseOrderAddress($purchaseOrderId) {
        $manager = $this->getDoctrine()->getManager();
        $purchaseOrderAddress = $manager->getRepository(PurchaseOrderShippingAddress::class);
        $query = $purchaseOrderAddress->createQueryBuilder('poa')
            ->where("poa.purchaseOrderId = '".$purchaseOrderId."'")
            ->getQuery();
        return $query->getResult();
    }

}
