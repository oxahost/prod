<?php

namespace Webkul\POMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Webkul\POMBundle\Entity\PurchaseOrder;
use Webkul\POMBundle\Entity\Supplier;
use Oro\Bundle\ProductBundle\Entity\Product;
use Webkul\POMBundle\Form\Type\SupplierType;
use Webkul\POMBundle\Entity\SupplierProduct;

/**
 * @Route("/poproduct")
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="poproduct_product")
     * @Template()
     * @Acl(
     *      id="poproduct_product",
     *      type="entity",
     *      class="WebkulPOMBundle:PurchaseOrder",
     *      permission="EDIT"
     * )
     */
    public function indexAction(Request $request)
    {
        return [];
        // return [
        //     'supplierId' => $request->get('supplierId'),
        // ];
    }
    /**
     * @Route("/list", name="poproduct_product_list")
     * @Template()
     * @AclAncestor("poproduct_product")
     */
    public function listAction(Request $request)
    {
        return [
            'supplierId' => $request->get('supplierId'),
        ];
    }

    /**
     * @Route("/set", name="poproduct_product_set", requirements={"id"="\d+"})
     * @AclAncestor("poproduct_product")
     */
    public function setAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $product = $manager->getRepository(Product::class)->find($request->get('id'));
        $products = $this->get('session')->get('products');
        if(!$products) {
            $products = [];
        }
        $currentProduct[$product->getId()] = [
            'id' => $product->getId(),
            'sku' => $product->getSku(),
            'name' => $product->getDenormalizedDefaultName(),
        ];
        $products = array_merge($products, $currentProduct);
        $this->get('session')->set('products', $products);
        return new JsonResponse(['dataGrid' => 'temp-purchase-order-products-grid', 'msg' => 'Added to the purchase order list']);
    }

    /**
     * @Route("/getProducts", name="poproduct_products_get_api")
     * @AclAncestor("poproduct_product")
     * @return JSON
     */
    public function getProductListAction(Request $request)
    {
        $productSku = $request->get('values');
        $productSkus = [];
        $productSkus = json_decode($productSku, true);
        $term = $request->get('term');
        $action = $request->get('action');
        $purchaseId = $request->get('id');
        $entityManager = $this->getDoctrine()->getManager();
        $supplierId = null;
        if($action == "update") {
            $purchaseOrder = $entityManager->getRepository(PurchaseOrder::class)->findOneById($purchaseId);
            if ($purchaseOrder) {
                $supplierId = $purchaseOrder->getSupplierId();
            }
        } else {
            $supplierId = $purchaseId;
        }
        
        $supplierProducts = $entityManager->getRepository(SupplierProduct::class)->findBy(["supplierId" => $supplierId]);
        
        $productId = [];
        foreach ($supplierProducts as  $supplierProduct) {
            $productId[] = $supplierProduct->getProductId();
        }
        
        $product = $entityManager->getRepository(Product::class);
        if(!empty($productSkus)) {
            $query = $product->createQueryBuilder('product')
                    ->where("product.denormalizedDefaultName like '%".$term."%'")
                    ->andWhere("product.id IN(:ids)")
                    ->andWhere("product.sku NOT IN(:skus)")
                    ->orderBy('product.denormalizedDefaultName', 'ASC')
                    ->setParameter('skus', $productSkus)
                    ->setParameter('ids', $productId)
                    ->setMaxResults(10)
                    ->getQuery(); 
        } else {
            $query = $product->createQueryBuilder('product')
                    ->where("product.denormalizedDefaultName like '%".$term."%'")
                    ->andWhere("product.id IN(:ids)")
                    ->orderBy('product.denormalizedDefaultName', 'ASC')
                    ->setParameter('ids', $productId)
                    ->setMaxResults(10)
                    ->getQuery(); 
        }
        
        $result = $query->getResult();
        $json['products'] = $result;
        if($result) {
            $json['success'] = true;
        } else {
            $json['success'] = false;
        }
        return new JsonResponse($json);
    }

    /**
     * @Route("/getProduct", name="poproduct_product_get_api")
     * @AclAncestor("poproduct_product")
     */
    public function getProductAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $product = $manager->getRepository(Product::class)->find($request->get('id'));
        $json['success'] = false;
        $json['product'] = [];
        if($product) {
            $json['success'] = true;
            $json['product'] = [
                'id' => $product->getId(),
                'sku' => $product->getSku(),
                'name' => $product->getDenormalizedDefaultName(),
            ];
        }
        return new JsonResponse($json);
    }

}
