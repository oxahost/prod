<?php

namespace Webkul\POMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Webkul\POMBundle\Entity\PurchaseOrder;
use Webkul\POMBundle\Entity\PurchaseOrderShippingAddress;

/**
 * @Route("/address")
 */
class AddressController extends Controller
{
    /**
     * @Route("/info/{id}", name="pom_address_info", requirements={"id"="\d+"})
     * @Template
     */
    public function infoAction(PurchaseOrder $purchaseOrder)
    {
        $manager = $this->getDoctrine()->getManager();
        $purchaseOrderAddress = $manager->getRepository(PurchaseOrderShippingAddress::class);
        $result = $purchaseOrderAddress->createQueryBuilder('poa')
            ->where("poa.purchaseOrderId = '".$purchaseOrder->getId()."'")
            ->getQuery()
            ->getOneOrNullResult();
        return array('address' => $result);
    }

}

