<?php

namespace Webkul\POMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Webkul\POMBundle\Entity\SupplierProduct;

/**
 * @Route("/supplierproduct")
 */
class SupplierProductController extends Controller
{
    /**
     * @Route("/", name="pom_supplier_product_list")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        return array('supplierId' => $request->get('id'));
    }

    /**
     * @Route("/list", name="pom_supplier_product_list_widget")
     * @Template()
     */
    public function listAction(Request $request)
    {
        return array('supplierId' => $request->get('id'));
    }

    /**
     * @Route("/add", name="pom_supplier_product_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        if($request->get('sPId')) {
            $supplierProduct = $manager->getRepository(SupplierProduct::class)->find($request->get('sPId'));
        } else {
            $supplierProduct = new SupplierProduct();
        }
        $supplierProduct->setProductId($request->get('id'));
        $supplierProduct->setSupplierId($request->get('supplierId'));
        $supplierProduct->setPrice(0);
        $supplierProduct->setStatus('enabled');
        $manager->persist($supplierProduct);
        $manager->flush();
        return new JsonResponse(['success' => true, 'message' => "Added to the Supplier's inventory"]);
    }

    /**
     * @Route("/remove", name="pom_supplier_product_remove")
     * @Template()
     */
    public function removeAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        if($request->get('sPId')) {
            $supplierProduct = $manager->getRepository(SupplierProduct::class)->find($request->get('sPId'));
            $supplierProduct->setStatus('disabled');
            $manager->persist($supplierProduct);
            $manager->flush();
        }
        return new JsonResponse(['success' => true, 'message' => "Removed from the Supplier's inventory"]);
    }

}
