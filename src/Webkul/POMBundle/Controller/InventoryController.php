<?php

namespace Webkul\POMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Doctrine\Common\Collections\ArrayCollection;
use Webkul\POMBundle\Form\Type\ReceiveInventoryType;
use Webkul\POMBundle\Form\Type\ReceivedPurchaseOrderType;
use Webkul\POMBundle\Entity\PurchaseOrder;
use Webkul\POMBundle\Entity\PurchaseOrderProduct;

/**
 * @Route("/inventory")
 */
class InventoryController extends Controller
{
    /**
     * @Route("/receive/{id}", name="pom_inventory_receive", requirements={"id"="\d+"})
     * @Template()
     * @Acl(
     *      id="pom_inventory_receive",
     *      type="entity",
     *      class="WebkulPOMBundle:PurchaseOrder",
     *      permission="EDIT"
     * )
     * @param Order $purchaseOrder
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function receiveAction(PurchaseOrder $purchaseOrder, Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $purchaseOrderProducts = $manager->getRepository(PurchaseOrderProduct::class);
        $query = $purchaseOrderProducts->createQueryBuilder('pop')
            ->where("pop.purchaseOrderId = '".$purchaseOrder->getId()."'")
            ->getQuery();
        $purchaseOrder->setProducts(new ArrayCollection($query->getResult()));

        $form = $this->createForm(ReceivedPurchaseOrderType::class, $purchaseOrder);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $purchaseOrder->setUpdatedAt(new \DateTime());
            $purchaseOrder->setStatus('products_received');
            $manager->persist($purchaseOrder);
            $manager->flush();
            $purchaseOrderId = $purchaseOrder->getId();
            
            foreach ($purchaseOrder->getProducts() as $key => $product) {
                $product->setPurchaseOrderId($purchaseOrderId);
                if($product->getQuantity() < $product->getReceived()) {
                    $product->setReceived($product->getQuantity());
                }
                
                // Update inventory in core
                // $inventoryLevel = $manager->getRepository(Oro\Bundle\InventoryBundle\Entity\InventoryLevel::class);
                // $inventoryLevelQuery = $inventoryLevel->createQueryBuilder('il')
                //     ->where("il.product_id = '".$product->getProductId()."'")
                //     ->getQuery();
                    
                // echo '<pre>';
                // print_r($inventoryLevelQuery);
                // echo '</pre>';
                // die;
                
                $manager->persist($product);
                $manager->flush();
            }

            $this->get('session')->getFlashBag()->add('success', 'Received inventory is updated');

            return $this->get('oro_ui.router')->redirectAfterSave(
                array(
                    'route' => 'pom_inventory_receive',
                    'parameters' => array('id' => $purchaseOrder->getId()),
                ),
                array(
                    'route' => 'pom_purchaseorder_view',
                    'parameters' => array('id' => $purchaseOrder->getId()),
                ),
                $purchaseOrder
            );
        }

        return [
            'form' => $form->createView(),
            'entity' => $purchaseOrder,
        ];
    }
}
