define(function (require) {
    'use strict';

    var RegionComponent,
        BaseComponent = require('oroui/js/app/components/base/component');
    require('jquery');

    RegionComponent = BaseComponent.extend({
        /**
         * Initializes Select2 component
         *
         * @param {Object} options
         */
        initialize: function (options) {
            console.log(options);
        },

        /**
         * Disposes the component
         */
        dispose: function () {
            if (this.disposed) {
                // component is already removed
                return;
            }
            this.$elem.select2('destroy');
            delete this.$elem;
            RegionComponent.__super__.dispose.call(this);
        }
    });

    return RegionComponent;
});