define(function () {
    return {
        init: function (deferred, options) {
            // gridPromise gets resolved once grid is rendered
            options.gridPromise.done(function (grid) {
                grid.body.on('rowClicked', function(grid, event) {
                    console.log('It\'s a rowClick',  grid, event);
                });
                // this deferred should be resolved when the builder finished its work
                deferred.resolve();
            })
        }
    }
});