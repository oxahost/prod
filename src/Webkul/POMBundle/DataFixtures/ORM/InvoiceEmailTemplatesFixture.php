<?php

namespace Webkul\POMBundle\DataFixtures\ORM;

use Oro\Bundle\EmailBundle\Migrations\Data\ORM\AbstractEmailFixture;

class InvoiceEmailTemplatesFixture extends AbstractEmailFixture
{
    public function getEmailsDir()
    {
        return $this->container
            ->get('kernel')
            ->locateResource('@WebkulPOMBundle/Resources/Emails');
    }
}

?>