<?php

namespace Webkul\POMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ReceiveInventoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('purchaseOrderId', HiddenType::class);
        $builder->add('productId', HiddenType::class);
        $builder->add('productName', TextType::class, [
            'attr' => [
                'readonly' => true,
                'class' => 'small-text-box'
            ]
        ]);
        $builder->add('sku', TextType::class, [
            'attr' => [
                'readonly' => true,
                'class' => 'small-text-box'
            ]
        ]);
        $builder->add('quantity', TextType::class, [
            'attr' => [
                'readonly' => true,
                'class' => 'small-text-box'
            ]
        ]);
        $builder->add('price', TextType::class, [
            'attr' => [
                'readonly' => true,
                'class' => 'small-text-box'
            ]
        ]);
        $builder->add('received', TextType::class, [
            'attr' => [
                'class' => 'small-text-box',
                'value' => '',
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Webkul\POMBundle\Entity\PurchaseOrderProduct',
        ));
    }

    public function getName()
    {
        return 'webkul_pom_purchase_order_product2';
    }
}

?>