<?php

namespace Webkul\POMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class ConfigType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, [
                'label' => 'oro.pom.config.entity_status',
                'choices' => [
                    'Disabled' => 0,
                    'Enabled' => 1,
                ]
            ])
            ->add('orderPrefix', TextType::class, [
                'label' => 'oro.pom.config.entity_order_prefix',
                'attr' => [
                    'value' => "",
                ]
            ])
            ->add('procurementMethod', ChoiceType::class, [
                'label' => 'oro.pom.config.entity_procurement_method',
                'choices' => [
                    'oro.pom.config.text_make_to_stock' => 'stock',
                    'oro.pom.config.text_make_to_order' => 'order',
                ]
            ])
            ->add('quotationQuantity', TextType::class, [
                'label' => 'oro.pom.config.entity_quotation_quantity',
                'attr' => [
                    'value' => "",
                ]
            ])
            ->add('poOnOrder', ChoiceType::class, [
                'label' => 'oro.pom.config.entity_po_on_order',
                'choices' => [
                    'Disabled' => 0,
                    'Enabled' => 1,
                ]
            ])
            ->add('posOrderStatus', ChoiceType::class, [
                'label' => 'oro.pom.config.entity_order_status',
                'choices' => [
                    'Disabled' => 0,
                    'Enabled' => 1,
                ]
            ])
            ->add('poEmailId', EmailType::class, [
                'label' => 'oro.pom.config.entity_po_email_id',
                'attr' => [
                    'value' => "",
                ]
            ])
            ;
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {

    }

}
