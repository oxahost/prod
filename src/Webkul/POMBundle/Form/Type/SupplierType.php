<?php

namespace Webkul\POMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Oro\Bundle\AddressBundle\Entity\Region;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class SupplierType extends AbstractType
{

    protected $entityManager;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->entityManager = $options['entityManager'];
        $builder
            ->add('name', TextType::class, [
                'label' => 'oro.pom.supplier.entry_company_name'
            ])
            ->add('email')
            ->add('website')
            ->add('supplierNumber')
            ->add('ownerName')
            ->add('gender')
            ->add('gender', ChoiceType::class, [
                'label' => 'Gender',
                'expanded' => true,
                'choices' => [
                    'Male' => 1,
                    'Female' => 0,
                ],
                'data' => '1'
            ])
            ->add('telephone')
            ->add('fax')
            ->add('street')
            ->add('city')
            ->add('postcode')
            ->add('country', CountryType::class, [
                'label' => 'Country',
                'placeholder' => 'Select a Country...',
                'attr' => [
                    'class' => 'country'
                ]
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'Status',
                'choices' => [
                    'oro.pom.general.status.disabled' => 'disabled',
                    'oro.pom.general.status.enabled' => 'enabled',
                ]
            ])
        ;
        
        $formModifier = function (FormInterface $form, $country) {
            $states = $this->entityManager->getRepository('OroAddressBundle:Region')->findBycountry($country);
            $stateChoice = [];
            foreach ($states as $key => $state) {
                $stateChoice[$state->getName()] = $state->getCode();
            }

            $required = true;
            if($country && !$stateChoice) {
                $required = false;
            }
            
            $form->add('state', ChoiceType::class, [
                'required' => $required,
                'choices' => $stateChoice,
                'choice_attr' => function($stateChoice, $key, $value) {
                    return [
                        'value' => $stateChoice];
                },
                'attr' => [
                    'class' => 'state'
                ]
            ]);
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
                $formModifier($event->getForm(), $data->getCountry());
            }
        );

        $builder->get('country')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $country = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $country);
            }
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Webkul\POMBundle\Entity\Supplier',
        ));
        $resolver->setRequired('entityManager');
    }

    public function getName()
    {
        return 'webkul_pom_supplier';
    }
}

?>