<?php

namespace Webkul\POMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Webkul\POMBundle\Entity\PurchaseOrderProduct;
use Webkul\POMBundle\Form\Type\ReceiveInventoryType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
// use Symfony\Component\Form\FormEvent;
// use Symfony\Component\Form\FormEvents;

class ReceivedPurchaseOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('supplierId', HiddenType::class, [])
        ->add('products', CollectionType::class, [
            'label' => false,
            'entry_type' => ReceiveInventoryType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
        ]);

        // $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
        //     $purchaseOrder = $event->getData();
        //     $productCount = 0;
        //     $total = 0;
        //     foreach ($purchaseOrder->getProducts() as $key => $product) {
        //         $productCount += $product->getQuantity();
        //         $total += $product->getQuantity()*$product->getPrice();
        //     }
        //     $form = $event->getForm();
        //     $purchaseOrder->setTotalProducts($productCount);
        //     $purchaseOrder->setTotalAmount($total);
        // });

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Webkul\POMBundle\Entity\PurchaseOrder',
        ));
    }

    public function getName()
    {
        return 'webkul_pom_purchase_order';
    }
}

?>