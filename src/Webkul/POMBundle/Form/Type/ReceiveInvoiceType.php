<?php

namespace Webkul\POMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Oro\Bundle\FormBundle\Form\Type\OroDateType;

class ReceiveInvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('purchaseOrderId', HiddenType::class, [])
        ->add('invoiceNo', TextType::class, [
            'label' => "Invoice Number",
            'required' => false,
        ])
        ->add('date', OroDateType::class, [
            'placeholder' => 'Select a value',
        ])
        ->add('paymentDueDate', OroDateType::class, [
            'placeholder' => 'Select a value',
        ])
        ->add('submit', SubmitType::class, [
            'label' => 'Save',
            'attr'  => ['class' => 'btn btn-primary'],
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Webkul\POMBundle\Entity\ReceiveInvoice',
        ));
    }

    public function getName()
    {
        return 'webkul_pom_purchase_order_receive_invoice';
    }
}

?>