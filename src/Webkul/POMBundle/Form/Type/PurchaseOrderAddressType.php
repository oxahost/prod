<?php

namespace Webkul\POMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Oro\Bundle\AddressBundle\Entity\Region;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PurchaseOrderAddressType extends AbstractType
{
    protected $entityManager;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->entityManager = $options['entityManager'];

        $builder
        ->add('purchaseOrderId', HiddenType::class, [])
        ->add('firstname')
        ->add('lastname')
        ->add('email')
        ->add('contactNo')
        ->add('address1')
        ->add('address2')
        ->add('city')
        ->add('stateId', HiddenType::class, [])
        ->add('country', CountryType::class);

        $formModifier = function (FormInterface $form, $country) {
            $states = $this->entityManager->getRepository('OroAddressBundle:Region')->findBycountry($country);
            $stateChoice = [];
            foreach ($states as $key => $state) {
                $stateChoice[$state->getName()] = $state->getCode();
            }

            $required = true;
            if($country && !$stateChoice) {
                $required = false;
            }
            
            $form->add('state', ChoiceType::class, [
                'required' => $required,
                'choices' => $stateChoice,
                'choice_attr' => function($stateChoice, $key, $value) {
                    return [
                        'value' => $stateChoice];
                },
                'attr' => [
                    'class' => 'state'
                ]
            ]);
        };

        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
                if($data != null) {
                    $country = $data->getCountry();
                } else {
                    $country = '';
                }
                $formModifier($event->getForm(), $country);
            }
        );

        $builder->get('country')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $country = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $country);
            }
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Webkul\POMBundle\Entity\PurchaseOrderShippingAddress',
        ));
        $resolver->setRequired('entityManager');
    }

    public function getName()
    {
        return 'webkul_pom_purchase_order_address';
    }
}

?>