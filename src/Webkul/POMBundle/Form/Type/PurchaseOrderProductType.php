<?php

namespace Webkul\POMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
// use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class PurchaseOrderProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('purchaseOrderId', HiddenType::class);
        $builder->add('productId', HiddenType::class);
        $builder->add('productName', TextType::class, [
            'required' => false,
            'attr' => [
                'readonly' => true,
            ]
        ]);
        $builder->add('sku', TextType::class, [
            'required' => false,
            'attr' => [
                'readonly' => true,
                'class' => 'small-text-box'
            ]
        ]);
        $builder->add('quantity', TextType::class, [
            'attr' => [
                'class' => 'small-text-box',
            ]
        ]);
        $builder->add('price', TextType::class, [
            'attr' => [
                'class' => 'small-text-box',
            ]
        ]);
        $builder->add('received', HiddenType::class, []);

        // $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
        //     $purchaseOrderProduct = $event->getData();
        //     $purchaseOrderProduct->setReceived(0);
        // });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Webkul\POMBundle\Entity\PurchaseOrderProduct',
        ));
    }

    public function getName()
    {
        return 'webkul_pom_purchase_order_product';
    }
}

?>