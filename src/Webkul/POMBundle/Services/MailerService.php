<?php

namespace Webkul\POMBundle\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MailerService
{
    private $orocommerceMailer;

    public function __construct(
        $orocommerceMailer
    ) {
        $this->orocommerceMailer = $orocommerceMailer;
    }

    public function sendMail($email)
    {
        $this->orocommerceMailer->process($email);
    }

}
