<?php

namespace Webkul\POMBundle\Datagrid\Datasource;

use Oro\Bundle\DataGridBundle\Datagrid\DatagridInterface;
use Oro\Bundle\DataGridBundle\Datasource\DatasourceInterface;
use Oro\Bundle\DataGridBundle\Datasource\ResultRecord;
use Symfony\Component\HttpFoundation\Session\Session;

class TempPurchaseOrderProductsDataSource implements DatasourceInterface
{
    const TYPE = 'temp_purchase_order_products';

    /** {@inheritdoc} */
    public function process(DatagridInterface $grid, array $config)
    {
        $grid->setDatasource(clone $this);
    }

    /** {@inheritdoc} */
    public function getResults()
    {
        $rows = [];
        $session = new Session();
        $this->arraySource = $session->get('products');
        foreach ($this->arraySource as $result) {
            $rows[] = new ResultRecord($result);
        }

        return $rows;
    }

    /**
     * @return array
     */
    public function getArraySource()
    {
        return $this->arraySource;
    }

    /**
     * @param array $source
     */
    public function setArraySource(array $source)
    {
        $this->arraySource = $source;
    }
}