<?php

namespace Webkul\POMBundle\Datagrid;

use Oro\Bundle\DataGridBundle\Datasource\ResultRecordInterface;
use Oro\Bundle\FeatureToggleBundle\Checker\FeatureChecker;
use Oro\Bundle\WorkflowBundle\Configuration\Checker\ConfigurationChecker;
use Oro\Bundle\WorkflowBundle\Configuration\FeatureConfigurationExtension;
use Oro\Bundle\WorkflowBundle\Datagrid\ActionPermissionProvider as BaseActionPermissionProvider;

class ActionPermissionProvider extends BaseActionPermissionProvider
{
    /**
     * @param FeatureChecker $featureChecker
     * @param ConfigurationChecker $configurationChecker
     */
    public function __construct(FeatureChecker $featureChecker, ConfigurationChecker $configurationChecker)
    {
        parent::__construct($featureChecker, $configurationChecker);
        $this->featureChecker = $featureChecker;
    }

    /**
     * @param ResultRecordInterface $record
     * @return array
     */
    public function getProcessDefinitionPermissions(ResultRecordInterface $record)
    {
        $isEnabled = $record->getValue('status');
        
        $permissions = [];

        if($isEnabled == "placed") {

            $permissions =  [
                                'edit'             => false
                            ];
        } else {
            $permissions = [
                                'edit'             => true
                            ];
        }

        return $permissions;       
        
    }
}
