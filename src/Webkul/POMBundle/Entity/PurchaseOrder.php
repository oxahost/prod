<?php

namespace Webkul\POMBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * PurchaseOrder
 */
class PurchaseOrder
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $supplierId;

    /**
     * @var int
     */
    private $totalProducts;

    /**
     * @var string
     */
    private $invoiceNo;

    /**
     * @var float
     */
    private $totalAmount;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var ArrayCollection
    */
    private $products;

    /**
     * @var ArrayCollection
    */
    private $address;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->address = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set supplierId.
     *
     * @param int $supplierId
     *
     * @return PurchaseOrder
     */
    public function setSupplierId($supplierId)
    {
        $this->supplierId = $supplierId;

        return $this;
    }

    /**
     * Get supplierId.
     *
     * @return int
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * Set totalProducts.
     *
     * @param int $totalProducts
     *
     * @return PurchaseOrder
     */
    public function setTotalProducts($totalProducts)
    {
        $this->totalProducts = $totalProducts;

        return $this;
    }

    /**
     * Get totalProducts.
     *
     * @return int
     */
    public function getTotalProducts()
    {
        return $this->totalProducts;
    }

    /**
     * Set totalAmount.
     *
     * @param float $totalAmount
     *
     * @return PurchaseOrder
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount.
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set invoiceNo.
     *
     * @param float $invoiceNo
     *
     * @return PurchaseOrder
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;

        return $this;
    }

    /**
     * Get invoiceNo.
     *
     * @return float
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * Set currency.
     *
     * @param string $currency
     *
     * @return PurchaseOrder
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency.
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return PurchaseOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return PurchaseOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return PurchaseOrder
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts(ArrayCollection $products)
    {
        $this->products = $products;
    }

    public function removeProducts(ArrayCollection $products)
    {
        $this->products->removeElement($products);
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress(ArrayCollection $address)
    {
        $this->address = $address;
    }

    public function removeAddress(ArrayCollection $address)
    {
        $this->address->removeElement($address);
    }

}
