<?php

namespace Webkul\POMBundle\Entity;

/**
 * PurchaseOrderProduct
 */
class PurchaseOrderProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $purchaseOrderId;

    /**
     * @var int
     */
    private $productId;

    /**
     * @var string
     */
    private $productName;

    /**
     * @var string
     */
    private $sku;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int
     */
    private $received;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set purchaseOrderId.
     *
     * @param int $purchaseOrderId
     *
     * @return PurchaseOrderProduct
     */
    public function setPurchaseOrderId($purchaseOrderId)
    {
        $this->purchaseOrderId = $purchaseOrderId;

        return $this;
    }

    /**
     * Get purchaseOrderId.
     *
     * @return int
     */
    public function getPurchaseOrderId()
    {
        return $this->purchaseOrderId;
    }

    /**
     * Set productId.
     *
     * @param int $productId
     *
     * @return PurchaseOrderProduct
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId.
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set productName.
     *
     * @param int $productName
     *
     * @return PurchaseOrderProduct
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productName.
     *
     * @return int
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set sku.
     *
     * @param int $sku
     *
     * @return PurchaseOrderProduct
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku.
     *
     * @return int
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set quantity.
     *
     * @param int $quantity
     *
     * @return PurchaseOrderProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price.
     *
     * @param float $price
     *
     * @return PurchaseOrderProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set received.
     *
     * @param int $received
     *
     * @return PurchaseOrderProduct
     */
    public function setReceived($received)
    {
        $this->received = $received;

        return $this;
    }

    /**
     * Get received.
     *
     * @return int
     */
    public function getReceived()
    {
        return $this->received;
    }

}
