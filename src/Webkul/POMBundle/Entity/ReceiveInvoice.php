<?php

namespace Webkul\POMBundle\Entity;

/**
 * ReceiveInvoice
 */
class ReceiveInvoice
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $invoiceNo;

    /**
     * @var \DateTime|null
     */
    private $date;

    /**
     * @var \DateTime|null
     */
    private $paymentDueDate;

    /**
     * @var int
     */
    private $purchaseOrderId;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set invoiceNo.
     *
     * @param string $invoiceNo
     *
     * @return ReceiveInvoice
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;

        return $this;
    }

    /**
     * Get invoiceNo.
     *
     * @return string
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * Set date.
     *
     * @param \DateTime|null $date
     *
     * @return ReceiveInvoice
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set paymentDueDate.
     *
     * @param \DateTime|null $paymentDueDate
     *
     * @return ReceiveInvoice
     */
    public function setPaymentDueDate($paymentDueDate = null)
    {
        $this->paymentDueDate = $paymentDueDate;

        return $this;
    }

    /**
     * Get paymentDueDate.
     *
     * @return \DateTime|null
     */
    public function getPaymentDueDate()
    {
        return $this->paymentDueDate;
    }

    /**
     * Set purchaseOrderId.
     *
     * @param int $purchaseOrderId
     *
     * @return ReceiveInvoice
     */
    public function setPurchaseOrderId($purchaseOrderId)
    {
        $this->purchaseOrderId = $purchaseOrderId;

        return $this;
    }

    /**
     * Get purchaseOrderId.
     *
     * @return int
     */
    public function getPurchaseOrderId()
    {
        return $this->purchaseOrderId;
    }
}
