<?php

declare(strict_types=1);

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    public const ROOT_PACKAGE_NAME = 'oro/commerce-crm-application';
    /**
     * Array of all available composer packages.
     * Dont read this array from your calling code, but use the \PackageVersions\Versions::getVersion() method instead.
     *
     * @var array<string, string>
     * @internal
     */
    public const VERSIONS          = array (
  'akeneo/batch-bundle' => '0.4.11@712713ee1e15059ce3aed47ade5b83123ab69ac1',
  'ass/xmlsecurity' => 'v1.1.1@c8976519ebbf6e4d953cd781d09df44b7f65fbb8',
  'behat/transliterator' => 'v1.3.0@3c4ec1d77c3d05caa1f0bf8fb3aae4845005c7fc',
  'besimple/soap-client' => 'v0.2.6@50f186fd3e9fafc4ce2d194d5d18e3987d5fbf41',
  'besimple/soap-common' => 'v0.2.6@08aa78c7f1cae4b23cce5e8928dca7243ca4e4fa',
  'box/spout' => 'v2.7.3@3681a3421a868ab9a65da156c554f756541f452b',
  'brick/math' => '0.8.15@9b08d412b9da9455b210459ff71414de7e6241cd',
  'cboden/ratchet' => 'v0.4.3@466a0ecc83209c75b76645eb823401b5c52e5f21',
  'clue/stream-filter' => 'v1.4.1@5a58cc30a8bd6a4eb8f856adf61dd3e013f53f71',
  'composer/ca-bundle' => '1.2.7@95c63ab2117a72f48f5a55da9740a3273d45b7fd',
  'composer/composer' => '1.6.5@b184a92419cc9a9c4c6a09db555a94d441cb11c9',
  'composer/semver' => '1.5.1@c6bea70230ef4dd483e6bbcab6005f682ed3a8de',
  'composer/spdx-licenses' => '1.5.3@0c3e51e1880ca149682332770e25977c70cf9dae',
  'container-interop/container-interop' => '1.2.0@79cbf1341c22ec75643d841642dd5d6acd83bdb8',
  'defuse/php-encryption' => 'v2.2.1@0f407c43b953d571421e0020ba92082ed5fb7620',
  'doctrine/annotations' => 'v1.7.0@fa4c4e861e809d6a1103bd620cce63ed91aedfeb',
  'doctrine/cache' => '1.10.2@13e3381b25847283a91948d04640543941309727',
  'doctrine/collections' => 'v1.5.0@a01ee38fcd999f34d9bfbcee59dbda5105449cbf',
  'doctrine/common' => '2.12.0@2053eafdf60c2172ee1373d1b9289ba1db7f1fc6',
  'doctrine/data-fixtures' => '1.3.3@f0ee99c64922fc3f863715232b615c478a61b0a3',
  'doctrine/dbal' => '2.10.2@aab745e7b6b2de3b47019da81e7225e14dcfdac8',
  'doctrine/doctrine-bundle' => '1.9.1@703fad32e4c8cbe609caf45a71a1d4266c830f0f',
  'doctrine/doctrine-cache-bundle' => '1.4.0@6bee2f9b339847e8a984427353670bad4e7bdccb',
  'doctrine/doctrine-fixtures-bundle' => 'v2.4.1@74b8cc70a4a25b774628ee59f4cdf3623a146273',
  'doctrine/event-manager' => '1.1.0@629572819973f13486371cb611386eb17851e85c',
  'doctrine/inflector' => '1.4.3@4650c8b30c753a76bf44fb2ed00117d6f367490c',
  'doctrine/instantiator' => '1.3.1@f350df0268e904597e3bd9c4685c53e0e333feea',
  'doctrine/lexer' => '1.2.1@e864bbf5904cb8f5bb334f99209b48018522f042',
  'doctrine/orm' => 'v2.6.4@b52ef5a1002f99ab506a5a2d6dba5a2c236c5f43',
  'doctrine/persistence' => '1.3.1@5fde0b8c1261e5089ece1525e68be2be27c8b2a6',
  'doctrine/reflection' => '1.2.1@55e71912dfcd824b2fdd16f2d9afe15684cfce79',
  'egulias/email-validator' => '2.1.18@cfa3d44471c7f5bfb684ac2b0da7114283d78441',
  'evenement/evenement' => 'v3.0.1@531bfb9d15f8aa57454f5f0285b18bec903b8fb7',
  'ezyang/htmlpurifier' => 'v4.12.0@a617e55bc62a87eec73bd456d146d134ad716f03',
  'friendsofsymfony/jsrouting-bundle' => '2.2.2@be6c7ec335d0f0cf3b6d152d6b64d5772f5919b6',
  'friendsofsymfony/rest-bundle' => '2.7.4@3d8501dbdfa48811ef942f5f93c358c80d5ad8eb',
  'gedmo/doctrine-extensions' => 'v2.4.41@e55a6727052f91834a968937c93b6fb193be8fb6',
  'gos/pnctl-event-loop-emitter' => 'v0.1.7@93bb0f0e60e4e1f4025f77c8a4fd9ae0c3b45fb3',
  'gos/pubsub-router-bundle' => 'v0.3.5@a3f9666455dc42f38a7ce31ca2fc55bd27421ea0',
  'gos/web-socket-bundle' => 'v1.8.12@15174761596ebe9fb58d037ec144822531922f66',
  'gos/websocket-client' => 'v0.1.2@13bb38cb01acee648fea1a6ca4ad3dc6148da7fe',
  'guzzle/guzzle' => 'v3.7.4@b170b028c6bb5799640e46c8803015b0f9a45ed9',
  'guzzlehttp/psr7' => '1.6.1@239400de7a173fe9901b9ac7c06497751f00727a',
  'hoa/compiler' => '3.17.08.08@aa09caf0bf28adae6654ca6ee415ee2f522672de',
  'hoa/consistency' => '1.17.05.02@fd7d0adc82410507f332516faf655b6ed22e4c2f',
  'hoa/event' => '1.17.01.13@6c0060dced212ffa3af0e34bb46624f990b29c54',
  'hoa/exception' => '1.17.01.16@091727d46420a3d7468ef0595651488bfc3a458f',
  'hoa/file' => '1.17.07.11@35cb979b779bc54918d2f9a4e02ed6c7a1fa67ca',
  'hoa/iterator' => '2.17.01.10@d1120ba09cb4ccd049c86d10058ab94af245f0cc',
  'hoa/math' => '1.17.05.16@7150785d30f5d565704912116a462e9f5bc83a0c',
  'hoa/protocol' => '1.17.01.14@5c2cf972151c45f373230da170ea015deecf19e2',
  'hoa/regex' => '1.17.01.13@7e263a61b6fb45c1d03d8e5ef77668518abd5bec',
  'hoa/stream' => '1.17.02.21@3293cfffca2de10525df51436adf88a559151d82',
  'hoa/ustring' => '4.17.01.16@e6326e2739178799b1fe3fdd92029f9517fa17a0',
  'hoa/visitor' => '2.17.01.16@c18fe1cbac98ae449e0d56e87469103ba08f224a',
  'hoa/zformat' => '1.17.01.10@522c381a2a075d4b9dbb42eb4592dd09520e4ac2',
  'hwi/oauth-bundle' => '0.6.3@0963709b04d8ac0d6d6c0c78787f6f59bd0d9a01',
  'imagine/imagine' => 'v0.7.1@a9a702a946073cbca166718f1b02a1e72d742daa',
  'incenteev/composer-parameter-handler' => 'v2.1.4@084befb11ec21faeadcddefb88b66132775ff59b',
  'jdorn/sql-formatter' => 'v1.2.17@64990d96e0959dff8e059dfcdc1af130728d92bc',
  'jms/cg' => '1.2.0@2152ea2c48f746a676debb841644ae64cae27835',
  'jms/metadata' => '2.1.0@8d8958103485c2cbdd9a9684c3869312ebdaf73a',
  'jms/serializer' => '3.4.0@e2d3c49d9322a08ee32221a5623c898160dada79',
  'jms/serializer-bundle' => '3.5.0@5793ec59b2243365a625c0fd78415732097c11e8',
  'justinrainbow/json-schema' => '5.2.10@2ba9c8c862ecd5510ed16c6340aa9f6eadb4f31b',
  'knplabs/gaufrette' => 'v0.9.0@786247eba04d4693e88a80ca9fdabb634675dcac',
  'knplabs/knp-gaufrette-bundle' => 'v0.5.2@2a3d24efda257023e5d3f21866f1ff18f50c60ba',
  'knplabs/knp-menu' => '2.3.0@655630a1db0b72108262d1a844de3b1ba0885be5',
  'knplabs/knp-menu-bundle' => '2.2.2@267027582a1f1e355276f796f8da0e9f82026bf1',
  'kriswallsmith/assetic' => 'v1.0.5@8ab3638325af9cd144242765494a9dc9b53ab430',
  'kriswallsmith/buzz' => '1.0.1@3d436434ab6019a309b8813839a3693997d03774',
  'lcobucci/jwt' => '3.3.2@56f10808089e38623345e28af2f2d5e4eb579455',
  'league/event' => '2.2.0@d2cc124cf9a3fab2bb4ff963307f60361ce4d119',
  'league/oauth2-server' => '7.4.0@2eb1cf79e59d807d89c256e7ac5e2bf8bdbd4acf',
  'lexik/maintenance-bundle' => 'v2.1.5@3a3e916776934a95834235e4a1d71e4595d515f5',
  'liip/imagine-bundle' => '2.0.0@66959e113d1976d0b23a2617771c2c65d62ea44b',
  'michelf/php-markdown' => '1.9.0@c83178d49e372ca967d1a8c77ae4e051b3a3c75c',
  'monolog/monolog' => '1.23.0@fd8c787753b3a2ad11bc60c063cff1358a32a3b4',
  'mtdowling/cron-expression' => 'v1.2.3@9be552eebcc1ceec9776378f7dcc085246cacca6',
  'mustangostang/spyc' => '0.6.3@4627c838b16550b666d15aeae1e5289dd5b77da0',
  'myclabs/deep-copy' => '1.8.1@3e01bdad3e18354c3dce54466b7fbe33a9f9f7f8',
  'nelmio/api-doc-bundle' => '2.13.3@f0a606b6362c363043e01aa079bee2b0b5eb47a2',
  'nelmio/security-bundle' => '2.5.1@fe1d31eb23c13e0918de9a66df9d315648c5d3d1',
  'nesbot/carbon' => '1.29.2@ed6aa898982f441ccc9b2acdec51490f2bc5d337',
  'nyholm/psr7' => '1.2.1@55ff6b76573f5b242554c9775792bd59fb52e11c',
  'ocramius/package-versions' => '1.5.1@1d32342b8c1eb27353c8887c366147b4c2da673c',
  'ocramius/proxy-manager' => '2.1.1@e18ac876b2e4819c76349de8f78ccc8ef1554cd7',
  'oro/calendar-bundle' => '4.1.5@754729be27ef88a0c1a119ab20a7a529e1163df6',
  'oro/commerce' => '4.1.6@6a332cc13b9787bcaeaacbadeac6ccb4b75bfe19',
  'oro/commerce-crm' => '4.1.3@6da265e3e7728ac84ce1a5a5395bc89fd24cc4f3',
  'oro/crm' => '4.1.6@c7c3b13c3c2250f092311f0dc4388c8fcca936ee',
  'oro/crm-call-bundle' => '4.1.5@43e05b9fdd7efe93d97b01de5e4198924bf835ca',
  'oro/crm-dotmailer' => '4.1.5@1777173c2fdb925ffb397425dc0e6afb987654a1',
  'oro/crm-hangouts-call-bundle' => '4.1.1@42d1a1d70cc88ae14d7ce1e4d60c479fcc0d9616',
  'oro/crm-magento-embedded-contact-us' => '4.1.2@c498dc33de933253dc020648aecbd8482b3a933b',
  'oro/crm-task-bundle' => '4.1.4@6577877f8265d24eb3e0bca020d8597385df550f',
  'oro/crm-zendesk' => '4.1.3@1038fdecadf21f4be2fd8e5fcb61d0d49e5d25dd',
  'oro/customer-portal' => '4.1.6@04555ebc996ad97357518d9d73e56e41425d11e9',
  'oro/doctrine-extensions' => '1.2.2@71b38bd772d68723b3999843d710b039b667426e',
  'oro/marketing' => '4.1.4@39ec188da6474aafced579bc7c8093f2f2022c86',
  'oro/oauth2-server' => '4.1.4@517861414ba01e535b9eb2ce2993bc316edc77a4',
  'oro/platform' => '4.1.6@d15f76e7691967f5fefd9a66d234861dc3ccc8d8',
  'oro/platform-serialised-fields' => '4.1.2@7e41bbda72ee4b441254ac61d774fc4ce2b79960',
  'oro/redis-config' => '4.1.2@b2b5b5613981c85c83aac2ad5e0dcc2c54af04af',
  'paragonie/random_compat' => 'v2.0.18@0a58ef6e3146256cc3dc7cc393927bcc7d1b72db',
  'php-http/client-common' => '1.10.0@c0390ae3c8f2ae9d50901feef0127fb9e396f6b4',
  'php-http/discovery' => '1.9.0@9ab7668fee74a5ad61996095e50917bd50ee8bfe',
  'php-http/httplug' => 'v1.1.0@1c6381726c18579c4ca2ef1ec1498fdae8bdf018',
  'php-http/httplug-bundle' => '1.15.2@35d281804a90f0359aa9da5b5b1f55c18aeafaf8',
  'php-http/logger-plugin' => '1.1.0@c1c6e90717ce350319b7b8bc489f1db35bb523fd',
  'php-http/message' => '1.8.0@ce8f43ac1e294b54aabf5808515c3554a19c1e1c',
  'php-http/message-factory' => 'v1.0.2@a478cb11f66a6ac48d8954216cfed9aa06a501a1',
  'php-http/promise' => '1.1.0@4c4c1f9b7289a2ec57cde7f1e9762a5789506f88',
  'php-http/stopwatch-plugin' => '1.3.0@de6f39c96f57c60a43d535e27122de505e683f98',
  'phpdocumentor/reflection-common' => '2.2.0@1d01c49d4ed62f25aa84a747ad35d5a16924662b',
  'phpdocumentor/reflection-docblock' => '4.3.4@da3fd972d6bafd628114f7e7e036f45944b62e9c',
  'phpdocumentor/type-resolver' => '1.3.0@e878a14a65245fbe78f8080eba03b47c3b705651',
  'piwik/device-detector' => '3.10.2@67e96595cd7649b7967533053fcbfbe02d5c55a3',
  'predis/predis' => 'v1.1.1@f0210e38881631afeafb56ab43405a92cafd9fd1',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/container' => '1.0.0@b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
  'psr/http-client' => '1.0.1@2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
  'psr/http-factory' => '1.0.1@12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
  'psr/http-message' => '1.0.1@f6561bf28d520154e4b0ec72be95418abe6d9363',
  'psr/link' => '1.0.0@eea8e8662d5cd3ae4517c9b864493f59fca95562',
  'psr/log' => '1.1.3@0f73288fd15629204f9d42b7055f72dacbe811fc',
  'psr/simple-cache' => '1.0.1@408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
  'ralouphie/getallheaders' => '3.0.3@120b605dfeb996808c31b6477290a714d356e822',
  'ratchet/rfc6455' => 'v0.3@c8651c7938651c2d55f5d8c2422ac5e57a183341',
  'react/cache' => 'v1.0.0@aa10d63a1b40a36a486bdf527f28bac607ee6466',
  'react/dns' => 'v1.2.0@a214d90c2884dac18d0cac6176202f247b66d762',
  'react/event-loop' => 'v0.5.3@228178a947de1f7cd9296d691878569628288c6f',
  'react/promise' => 'v2.8.0@f3cff96a19736714524ca0dd1d4130de73dbbbc4',
  'react/promise-timer' => 'v1.5.1@35fb910604fd86b00023fc5cda477c8074ad0abc',
  'react/socket' => 'v1.5.0@842dcd71df86671ee9491734035b3d2cf4a80ece',
  'react/stream' => 'v1.1.1@7c02b510ee3f582c810aeccd3a197b9c2f52ff1a',
  'robloach/component-installer' => '0.2.3@908a859aa7c4949ba9ad67091e67bac10b66d3d7',
  'romanpitak/dotmailer-api-v2-client' => '3.0.0@40a8603a8c7be29afff2a8f0b816f88085f27064',
  'romanpitak/php-rest-client' => 'v1.2.1@728b6c44040a13daeb8033953f5f3cdd3dde1980',
  'salsify/json-streaming-parser' => 'v8.0.1@7d3bc67f495ba161de40f033d6bda5959542bbd0',
  'seld/cli-prompt' => '1.0.3@a19a7376a4689d4d94cab66ab4f3c816019ba8dd',
  'seld/jsonlint' => '1.8.0@ff2aa5420bfbc296cf6a0bc785fa5b35736de7c1',
  'seld/phar-utils' => '1.1.1@8674b1d84ffb47cc59a101f5d5a3b61e87d23796',
  'sensio/framework-extra-bundle' => 'v5.2.4@1fdf591c4b388e62dbb2579de89c1560b33f865d',
  'snc/redis-bundle' => '3.2.3@cd47f699eeb6cffa72eaa505d3dcf907b157339b',
  'stof/doctrine-extensions-bundle' => 'v1.3.0@46db71ec7ffee9122eca3cdddd4ef8d84bae269c',
  'swiftmailer/swiftmailer' => 'v6.2.3@149cfdf118b169f7840bbe3ef0d4bc795d1780c9',
  'symfony/acl-bundle' => 'v1.0.1@5b32179c5319105cfc58884c279d76497f8a63b4',
  'symfony/contracts' => 'v1.1.8@f51bca9de06b7a25b19a4155da7bebad099a5def',
  'symfony/monolog-bundle' => 'v3.3.1@572e143afc03419a75ab002c80a2fd99299195ff',
  'symfony/polyfill-ctype' => 'v1.17.1@2edd75b8b35d62fd3eeabba73b26b8f1f60ce13d',
  'symfony/polyfill-iconv' => 'v1.17.1@ba6c9c18db36235b859cc29b8372d1c01298c035',
  'symfony/polyfill-intl-icu' => 'v1.17.1@7b5e03aeb88cc8b4b2b136e34b0fc0830e86cd4d',
  'symfony/polyfill-intl-idn' => 'v1.17.1@a57f8161502549a742a63c09f0a604997bf47027',
  'symfony/polyfill-mbstring' => 'v1.17.1@7110338d81ce1cbc3e273136e4574663627037a7',
  'symfony/polyfill-php70' => 'v1.17.1@471b096aede7025bace8eb356b9ac801aaba7e2d',
  'symfony/polyfill-php72' => 'v1.17.0@f048e612a3905f34931127360bdd2def19a5e582',
  'symfony/polyfill-php73' => 'v1.17.1@fa0837fe02d617d31fbb25f990655861bb27bd1a',
  'symfony/psr-http-message-bridge' => 'v1.3.0@9d3e80d54d9ae747ad573cad796e8e247df7b796',
  'symfony/security-acl' => 'v3.0.4@dc8f10b3bda34e9ddcad49edc7accf61f31fce43',
  'symfony/swiftmailer-bundle' => 'v3.1.2@c0807512fb174cf16ad4c6d3c0beffc28f78f4cb',
  'symfony/symfony' => 'v4.4.2@5973dacbd9587d9bf1234dfcb546c3cbdfbc4c9e',
  'tinymce/tinymce' => '4.6.7@9f9cf10d009892009a296dff48970b079ec78c7a',
  'true/punycode' => 'v2.1.1@a4d0c11a36dd7f4e7cd7096076cab6d3378a071e',
  'twig/extensions' => 'v1.5.4@57873c8b0c1be51caa47df2cdb824490beb16202',
  'twig/twig' => 'v2.12.5@18772e0190734944277ee97a02a9a6c6555fcd94',
  'ua-parser/uap-php' => 'v3.9.8@fde0bd76ebd21cebfabc90a3a0d927754cb4f739',
  'webmozart/assert' => '1.9.1@bafc69caeb4d49c39fd0779086c03a3738cbb389',
  'willdurand/jsonp-callback-validator' => 'v1.1.0@1a7d388bb521959e612ef50c5c7b1691b097e909',
  'willdurand/negotiation' => 'v2.3.1@03436ededa67c6e83b9b12defac15384cb399dc9',
  'xemlock/htmlpurifier-html5' => 'v0.1.10@32cef47500fb77c2be0f160372095bec15bf0052',
  'zendframework/zend-code' => '3.4.1@268040548f92c2bfcba164421c1add2ba43abaaa',
  'zendframework/zend-diactoros' => '1.8.7@a85e67b86e9b8520d07e6415fcbcb8391b44a75b',
  'zendframework/zend-eventmanager' => '3.2.1@a5e2583a211f73604691586b8406ff7296a946dd',
  'zendframework/zend-loader' => '2.6.1@91da574d29b58547385b2298c020b257310898c6',
  'zendframework/zend-mail' => '2.10.0@d7beb63d5f7144a21ac100072c453e63860cdab8',
  'zendframework/zend-mime' => '2.7.2@c91e0350be53cc9d29be15563445eec3b269d7c1',
  'zendframework/zend-stdlib' => '3.2.1@66536006722aff9e62d1b331025089b7ec71c065',
  'zendframework/zend-validator' => '2.13.0@b54acef1f407741c5347f2a97f899ab21f2229ef',
  'behat/behat' => 'v3.4.3@d60b161bff1b95ec4bb80bb8cb210ccf890314c2',
  'behat/gherkin' => 'v4.6.0@ab0a02ea14893860bca00f225f5621d351a3ad07',
  'behat/mink' => 'dev-master@07c6a9fe3fa98c2de074b25d9ed26c22904e3887',
  'behat/mink-extension' => '2.3.1@80f7849ba53867181b7e412df9210e12fba50177',
  'behat/mink-selenium2-driver' => 'v1.3.1@473a9f3ebe0c134ee1e623ce8a9c852832020288',
  'behat/symfony2-extension' => '2.1.5@d7c834487426a784665f9c1e61132274dbf2ea26',
  'composer/xdebug-handler' => '1.4.2@fa2aaf99e2087f013a14f7432c1cd2dd7d8f1f51',
  'friendsofphp/php-cs-fixer' => 'v2.16.1@c8afb599858876e95e8ebfcd97812d383fa23f02',
  'fzaninotto/faker' => 'v1.9.1@fc10d778e4b84d5bd315dad194661e091d307c6f',
  'instaclick/php-webdriver' => '1.4.7@b5f330e900e9b3edfc18024a5ec8c07136075712',
  'johnkary/phpunit-speedtrap' => 'v3.0.0@5f1ede99bd53fd0e5ff72669877420e801256d90',
  'mybuilder/phpunit-accelerator' => 'dev-master@91dae70fbeb7b81b9502a9d3ea80d1184c1134b1',
  'nelmio/alice' => 'v3.5.8@4a8d15ea1b70869a8b5eaaf3bf6d68b228322bc4',
  'oro/twig-inspector' => '1.0.3@a6bfa95e3909fba2f7ee411b4a5ac6b373490e2f',
  'pdepend/pdepend' => '2.8.0@c64472f8e76ca858c79ad9a4cf1e2734b3f8cc38',
  'phar-io/manifest' => '1.0.3@7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
  'phar-io/version' => '2.0.1@45a2ec53a73c70ce41d55cedef9063630abaf1b6',
  'php-cs-fixer/diff' => 'v1.3.0@78bb099e9c16361126c86ce82ec4405ebab8e756',
  'phpmd/phpmd' => '2.6.1@7425e155cf22cdd2b4dd3458a7da4cf6c0201562',
  'phpspec/prophecy' => 'v1.10.3@451c3cd1418cf640de218914901e51b064abb093',
  'phpunit/php-code-coverage' => '6.1.4@807e6013b00af69b6c5d9ceb4282d0393dbb9d8d',
  'phpunit/php-file-iterator' => '2.0.2@050bedf145a257b1ff02746c31894800e5122946',
  'phpunit/php-text-template' => '1.2.1@31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
  'phpunit/php-timer' => '2.1.2@1038454804406b0b5f5f520358e78c1c2f71501e',
  'phpunit/php-token-stream' => '3.1.1@995192df77f63a59e47f025390d2d1fdf8f425ff',
  'phpunit/phpcov' => '5.0.0@72fb974e6fe9b39d7e0b0d44061d2ba4c49ee0b8',
  'phpunit/phpunit' => '7.5.20@9467db479d1b0487c99733bb1e7944d32deded2c',
  'sebastian/code-unit-reverse-lookup' => '1.0.1@4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
  'sebastian/comparator' => '3.0.2@5de4fc177adf9bce8df98d8d141a7559d7ccf6da',
  'sebastian/diff' => '3.0.2@720fcc7e9b5cf384ea68d9d930d480907a0c1a29',
  'sebastian/environment' => '4.2.3@464c90d7bdf5ad4e8a6aea15c091fec0603d4368',
  'sebastian/exporter' => '3.1.2@68609e1261d215ea5b21b7987539cbfbe156ec3e',
  'sebastian/finder-facade' => '1.2.3@167c45d131f7fc3d159f56f191a0a22228765e16',
  'sebastian/global-state' => '2.0.0@e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
  'sebastian/object-enumerator' => '3.0.3@7cfd9e65d11ffb5af41198476395774d4c8a84c5',
  'sebastian/object-reflector' => '1.1.1@773f97c67f28de00d397be301821b06708fca0be',
  'sebastian/phpcpd' => '4.0.0@bb7953b81fb28e55964d76d5fe2dbe725d43fab3',
  'sebastian/recursion-context' => '3.0.0@5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
  'sebastian/resource-operations' => '2.0.1@4d7a795d35b889bf80a0cc04e08d77cedfa917a9',
  'sebastian/version' => '2.0.1@99732be0ddb3361e16ad77b68ba41efc8e979019',
  'squizlabs/php_codesniffer' => '3.5.5@73e2e7f57d958e7228fce50dc0c61f58f017f9f6',
  'symfony/class-loader' => 'v3.4.42@e4636a4f23f157278a19e5db160c63de0da297d8',
  'symfony/phpunit-bridge' => 'v4.3.11@692a73a2e0b76123150709f5eb7e2ea9bf2bad43',
  'theofidry/alice-data-fixtures' => 'v1.0.1@5752bbf979a012bb804c00641478d4d3f879e51d',
  'theseer/fdomdocument' => '1.6.6@6e8203e40a32a9c770bcb62fe37e68b948da6dca',
  'theseer/tokenizer' => '1.1.3@11336f6f84e16a720dae9d8e6ed5019efa85a0f9',
  'oro/commerce-crm-application' => '4.1.6@',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException If a version cannot be located.
     *
     * @psalm-param key-of<self::VERSIONS> $packageName
     */
    public static function getVersion(string $packageName) : string
    {
        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new \OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: check your ./vendor/composer/installed.json and/or ./composer.lock files'
        );
    }
}
