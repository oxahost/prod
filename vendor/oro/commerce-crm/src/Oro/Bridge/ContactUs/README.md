# OroContactUsBridgeBundle

OroContactUsBridgeBundle integrates features of the [OroContactUsBundle](https://github.com/oroinc/crm/tree/4.1/src/Oro/Bundle/ContactUsBundle) into **OroCommerce** applications.

The bundle allows admin users to enable the Contact Us form feature on [System configuration](https://github.com/oroinc/platform/tree/4.1/src/Oro/Bundle/ConfigBundle) UI and adds links to the Contact Us form page to the OroCommerce storefront navigation menus.
