# OroCacheBundle

OroCacheBundle introduces the configuration of the application data cache storage used by application bundles for different cache types.

Please see [documentation](https://doc.oroinc.com/backend/bundles/platform/CacheBundle/) for more details.
