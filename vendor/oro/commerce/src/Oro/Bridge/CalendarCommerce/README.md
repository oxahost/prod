# OroCalendarCommerceBridgeBundle

OroCalendarCommerceBridgeBundle is a bridge that integrates [OroCalendarBundle](https://github.com/oroinc/OroCalendarBundle) into OroCommerce applications.

The bundle enables the Calendar Event [activity](https://github.com/oroinc/platform/tree/4.1/src/Oro/Bundle/ActivityBundle) for Customer users, Orders, RFP requests, and Quotes.
