# OroFrontendTestFrameworkBundle

OroFrontendTestFrameworkBundle provides customer fixtures and extends the [OroTestFrameworkBundle](https://github.com/oroinc/platform/tree/4.1/src/Oro/Bundle/TestFrameworkBundle) test client to enable testing of the OroCommerce storefront website(s).
