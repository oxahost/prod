# OroFrontendLocalizationBundle

OroFrontendLocalizationBundle adds the [localization](https://github.com/oroinc/platform/tree/4.1/src/Oro/Bundle/LocaleBundle) feature to the OroCommerce storefront and enables customers to switch between storefront localizations using the language switcher widget.
