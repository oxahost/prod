CallCRM
-------
* The `UpdateCallAccessLevels::processPermission`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/CallCRM/Migrations/Data/ORM/UpdateCallAccessLevels.php#L75 "Oro\Bridge\CallCRM\Migrations\Data\ORM\UpdateCallAccessLevels::processPermission")</sup> method was removed.

MarketingCRM
------------
* The following methods in class `LoadTrackingRolesData`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/MarketingCRM/Migrations/Data/ORM/LoadTrackingRolesData.php#L36 "Oro\Bridge\MarketingCRM\Migrations\Migrations\Data\ORM\LoadTrackingRolesData")</sup> were removed:
   - `setContainer`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/MarketingCRM/Migrations/Data/ORM/LoadTrackingRolesData.php#L36 "Oro\Bridge\MarketingCRM\Migrations\Migrations\Data\ORM\LoadTrackingRolesData::setContainer")</sup>
   - `processPermission`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/MarketingCRM/Migrations/Data/ORM/LoadTrackingRolesData.php#L86 "Oro\Bridge\MarketingCRM\Migrations\Migrations\Data\ORM\LoadTrackingRolesData::processPermission")</sup>
* The `LoadTrackingRolesData::$container`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/MarketingCRM/Migrations/Data/ORM/LoadTrackingRolesData.php#L20 "Oro\Bridge\MarketingCRM\Migrations\Migrations\Data\ORM\LoadTrackingRolesData::$container")</sup> property was removed.

TaskCRM
-------
* The following methods in class `UpdateTaskAccessLevels`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/TaskCRM/Migrations/Data/Demo/ORM/UpdateTaskAccessLevels.php#L36 "Oro\Bridge\TaskCRM\Migrations\Data\Demo\ORM\UpdateTaskAccessLevels")</sup> were removed:
   - `setContainer`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/TaskCRM/Migrations/Data/Demo/ORM/UpdateTaskAccessLevels.php#L36 "Oro\Bridge\TaskCRM\Migrations\Data\Demo\ORM\UpdateTaskAccessLevels::setContainer")</sup>
   - `updateUserRole`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/TaskCRM/Migrations/Data/Demo/ORM/UpdateTaskAccessLevels.php#L59 "Oro\Bridge\TaskCRM\Migrations\Data\Demo\ORM\UpdateTaskAccessLevels::updateUserRole")</sup>
   - `getRole`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/TaskCRM/Migrations/Data/Demo/ORM/UpdateTaskAccessLevels.php#L102 "Oro\Bridge\TaskCRM\Migrations\Data\Demo\ORM\UpdateTaskAccessLevels::getRole")</sup>
* The following properties in class `UpdateTaskAccessLevels`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/TaskCRM/Migrations/Data/Demo/ORM/UpdateTaskAccessLevels.php#L18 "Oro\Bridge\TaskCRM\Migrations\Data\Demo\ORM\UpdateTaskAccessLevels")</sup> were removed:
   - `$container`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/TaskCRM/Migrations/Data/Demo/ORM/UpdateTaskAccessLevels.php#L18 "Oro\Bridge\TaskCRM\Migrations\Data\Demo\ORM\UpdateTaskAccessLevels::$container")</sup>
   - `$objectManager`<sup>[[?]](https://github.com/oroinc/crm/tree/4.1.0/src/Oro/Bridge/TaskCRM/Migrations/Data/Demo/ORM/UpdateTaskAccessLevels.php#L23 "Oro\Bridge\TaskCRM\Migrations\Data\Demo\ORM\UpdateTaskAccessLevels::$objectManager")</sup>

