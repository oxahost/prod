# OroTaskCRMBridgeBundle

OroTaskCRMBridgeBundle enables the task [activity](https://github.com/oroinc/platform/tree/4.1/src/Oro/Bundle/ActivityBundle) for Account, Contact, Sales Lead, Sales Opportunity, Sales B2BCustomer, Case, Magento Customer, Magento Order entities. The bundle enables users to create Tasks from the view pages of these entities with correct prefilled context.
