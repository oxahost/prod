# OroContactUsBundle

OroContactUsBundle adds the Contact Us [embedded form type](https://github.com/oroinc/platform/tree/4.1/src/Oro/Bundle/EmbeddedFormBundle) to Oro applications, provides UI for admin users to configure possible contact reasons available in the form and enables UI that allows to view and manage collected customer requests.
