WebsiteBundle
-------------
* The `DefaultWebsiteSubscriber`<sup>[[?]](https://github.com/oroinc/customer-portal/tree/4.1.0/src/Oro/Bundle/WebsiteBundle/Form/EventSubscriber/DefaultWebsiteSubscriber.php#L11 "Oro\Bundle\WebsiteBundle\Form\EventSubscriber\DefaultWebsiteSubscriber")</sup> class was removed.
* The `WebsiteManager::__construct(ManagerRegistry $managerRegistry, FrontendHelper $frontendHelper)`<sup>[[?]](https://github.com/oroinc/customer-portal/tree/4.1.0/src/Oro/Bundle/WebsiteBundle/Manager/WebsiteManager.php#L35 "Oro\Bundle\WebsiteBundle\Manager\WebsiteManager")</sup> method was changed to `WebsiteManager::__construct(ManagerRegistry $managerRegistry, FrontendHelper $frontendHelper, Mode $maintenance)`<sup>[[?]](https://github.com/oroinc/customer-portal/tree/4.1.1/src/Oro/Bundle/WebsiteBundle/Manager/WebsiteManager.php#L42 "Oro\Bundle\WebsiteBundle\Manager\WebsiteManager")</sup>

