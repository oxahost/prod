CalendarBundle
--------------
* The following methods in class `UpdateAclRoles`<sup>[[?]](https://github.com/oroinc/OroCalendarBundle/tree/4.1.0/Migrations/Data/ORM/UpdateAclRoles.php#L37 "Oro\Bundle\CalendarBundle\Migrations\Data\ORM\UpdateAclRoles")</sup> were removed:
   - `setContainer`<sup>[[?]](https://github.com/oroinc/OroCalendarBundle/tree/4.1.0/Migrations/Data/ORM/UpdateAclRoles.php#L37 "Oro\Bundle\CalendarBundle\Migrations\Data\ORM\UpdateAclRoles::setContainer")</sup>
   - `updateUserRole`<sup>[[?]](https://github.com/oroinc/OroCalendarBundle/tree/4.1.0/Migrations/Data/ORM/UpdateAclRoles.php#L61 "Oro\Bundle\CalendarBundle\Migrations\Data\ORM\UpdateAclRoles::updateUserRole")</sup>
   - `updateManagerRole`<sup>[[?]](https://github.com/oroinc/OroCalendarBundle/tree/4.1.0/Migrations/Data/ORM/UpdateAclRoles.php#L90 "Oro\Bundle\CalendarBundle\Migrations\Data\ORM\UpdateAclRoles::updateManagerRole")</sup>
   - `getRole`<sup>[[?]](https://github.com/oroinc/OroCalendarBundle/tree/4.1.0/Migrations/Data/ORM/UpdateAclRoles.php#L123 "Oro\Bundle\CalendarBundle\Migrations\Data\ORM\UpdateAclRoles::getRole")</sup>
* The following properties in class `UpdateAclRoles`<sup>[[?]](https://github.com/oroinc/OroCalendarBundle/tree/4.1.0/Migrations/Data/ORM/UpdateAclRoles.php#L19 "Oro\Bundle\CalendarBundle\Migrations\Data\ORM\UpdateAclRoles")</sup> were removed:
   - `$container`<sup>[[?]](https://github.com/oroinc/OroCalendarBundle/tree/4.1.0/Migrations/Data/ORM/UpdateAclRoles.php#L19 "Oro\Bundle\CalendarBundle\Migrations\Data\ORM\UpdateAclRoles::$container")</sup>
   - `$objectManager`<sup>[[?]](https://github.com/oroinc/OroCalendarBundle/tree/4.1.0/Migrations/Data/ORM/UpdateAclRoles.php#L24 "Oro\Bundle\CalendarBundle\Migrations\Data\ORM\UpdateAclRoles::$objectManager")</sup>

