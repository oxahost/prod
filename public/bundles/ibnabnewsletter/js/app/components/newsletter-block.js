define(function(require) {
    'use strict';

    const BaseComponent = require('oroui/js/app/components/base/component');
    const mediator = require('oroui/js/mediator');
    const $ = require('jquery');
    const routing = require('routing');
    const _ = require('underscore');
    
    const NewsletterBlock = BaseComponent.extend({
        /**
         * @property {Object}
         */
        options: {
            inputId: '#subscribe-form__input',
            buttonId: '#subscribe-form__button',
            successLabel: '.subscribe-container__success-label',
            ajaxMode: true,
            ajaxMethod: 'GET',
            newsletterRoute: 'ibnab_newsletter_get_email',
            registerRoute: 'oro_customer_frontend_customer_user_register',
        },

        /**
         * @inheritDoc
         */
        constructor: function NewsletterBlock(options) {
            NewsletterBlock.__super__.constructor.call(this, options);
        },

        /**
         * @inheritDoc
         */
        initialize: function(options) {
            
            
            $(this.options.inputId).attr("autocomplete","off");
            
            this.watchEmailInput();
            
        },
        
        watchEmailInput: function() {
            
            var self = this;
            var emailInput = self.options.inputId;
            var emailButton = self.options.buttonId;
            var successLabel = self.options.successLabel;
            $(emailButton).unbind("click");
            $(emailButton).bind("click", function(event){              
                self.getEmail($(emailInput).val());
                //$(successLabel).show();
                return false;
            });
        },
                    
        getEmail: function(query) {
            
                var self = this;
                var emailInput = self.options.inputId;
                if(!this.isValidEmailAddress(query) || query.length === 0 )
                {
                  $(emailInput).addClass('newsletter-error');  
                }else{
                $.ajax({
                url: routing.generate(self.options.newsletterRoute),
                method: self.options.ajaxMethod,
                data: {"email" : query},
                
                beforeSend:  function(){
                   mediator.execute('showLoading');
                   
                },
                success:  function(data){ 
                   if(data.status == 0){
                      mediator.execute('showFlashMessage', 'error', _.__('ibnab.newsletter.footer_error_label'));
                   }  
                   if(data.status == 1){
                       mediator.execute('showFlashMessage', 'success',_.__('ibnab.newsletter.footer_success_label'));
                   } 
                   if(data.status == 2){
                       mediator.execute('showFlashMessage', 'success',_.__('ibnab.newsletter.footer_already_label'));
                   }   
                   if(data.status == 3){
                       mediator.execute('showFlashMessage', 'success',_.__('ibnab.newsletter.footer_signin_label'));
                       window.location.replace(routing.generate(self.options.registerRoute));
                   } 
                   mediator.execute('showLoading');

                },
                complete:  function(){
                   mediator.execute('hideLoading'); 
                }
                }); 
             }
            
        },
        isValidEmailAddress: function(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        }
    });

    return NewsletterBlock;
});