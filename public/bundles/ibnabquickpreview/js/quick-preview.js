define(function (require) {
    'use strict';

    var PopupQuickViewWidget;
    var FrontendDialogWidget = require('orofrontend/js/app/components/frontend-dialog-widget');
    var $ = require('jquery');
    var _ = require('underscore');
    var mediator = require('oroui/js/mediator');
    var routing = require('routing');
    var BaseView = require('oroui/js/app/views/base/view');

    var BROWSER_SCROLL_SIZE = mediator.execute('layout:scrollbarWidth');

    PopupQuickViewWidget = BaseView.extend({
        /**
         * @property {Object}
         */
        template: require('tpl-loader!ibnabquickpreview/templates/quick-preview.html'),
        content: "",
        /**
         * @property {Object}
         */
        options: {
            ajaxMode: true,
            //ajaxRoute: 'oro_product_frontend_product_view',
            ajaxRoute: 'ibnab_quickpreview_frontend_product_view',
            ajaxMethod: 'GET'
        },
        /**
         * @inheritDoc
         */
        constructor: function PopupQuickViewWidget() {
            PopupQuickViewWidget.__super__.constructor.apply(this, arguments);
        },
        /**
         * @constructor
         * @param {Object} options
         */
        initialize: function (options) {
            this.options = _.defaults(options || {}, this.options);
            this.$el = options._sourceElement;
            this.$quickViewWidgetOpen = this.$el.find('[data-trigger-quickview-open]');

            this.bindEvents();
        },
        bindEvents: function () {
            if (this.options.ajaxMode) {
                this.$quickViewWidgetOpen.on('click', _.bind(this.ajaxOpenDecorator, this));
            } else {
                this.$quickViewWidgetOpen.on('click', _.bind(this.onOpen, this));
            }
        },
        unbindEvents: function () {
            this.$quickViewWidgetOpen.off('click');
        },
        onOpen: function (e) {
            e.preventDefault();
            var self = this;

            this.render();
        },
        ajaxOpenDecorator: function (e) {
            e.preventDefault();
            if (this.content !== "") {
                this.onOpen(e);
                return;
            }
            var data = {
                id: this.options.id
            };
            $.ajax({
                url: routing.generate(this.options.ajaxRoute, data) + "?type=quick-view-popup",
                method: this.options.ajaxMethod,
                dataType: 'html',
                beforeSend: _.bind(function () {
                    mediator.execute('showLoading');
                }, this),
                success: _.bind(function (data) {
                    var parsed = $.parseHTML(data);
                    this.$content = $(parsed).find('.page-content').html();
                    //this.$content = data;
                    this.onOpen(e);
                }, this),
                complete: _.bind(function () {
                    mediator.execute('hideLoading');
                }, this)
            });
        },
        render: function () {
            $('body').css("width", $(window).width() + "px");
            var self = this;
            new FrontendDialogWidget({
                autoRender: true,
                title: '',                
                simpleActionTemplate: false,
                renderActionsFromTemplate: true,
                staticPage: false,
                fullscreenMode: false,
                dialogOptions: {
                    modal: true,
                    resizable: false,
                    closeOnEscape: true,
                    autoResize: true,
                    width: 1000,
                    draggable: false,
                    dialogClass: 'quick-view-dialog-widget',
                    open: function () {
                        $('.ui-widget-overlay').bind('click', function () {
                            $.each($(".ui-dialog"), function ()
                            {
                                var $dialog;
                                $dialog = $(this).children(".ui-dialog-content");
                                if ($dialog.dialog("option", "modal"))
                                {
                                    $('.ui-dialog-titlebar-close:visible').click()
		                    //$dialog.dialog('close');
									//$(this).empty();
                                    //$(this).children(".ui-dialog-titlebar-close").trigger('click'); 
                                    //$(this).children(".ui-dialog-titlebar-close").click();
                                    //self.subview('popup').once('frontend-dialog:close', _.bind(this.changeCheckboxState, this, false));   
                                }
                            });
                        })
                    }
                }
            }).setContent("<div class='widget-content'>" + this.$content + "</div>");
            $(window).on('resize', function () {
                $('.mobile-version').css("width", $(window).width() + "px");
            });
            $(".quick-view-dialog-widget").css("z-index", "11");
            $(".ui-widget-overlay.ui-front").css("z-index", "10");
            $(".zoomLens").css("z-index", "10000");
            $(".sticky-panel").css("z-index", "9");

            //this.subview('popup')
            // .once('frontend-dialog:close', _.bind(this.changeCheckboxState, this, false));

            //this.$quickViewWidget.hide().show();
        },
        dispose: function () {

            PopupQuickViewWidget.__super__.dispose.call(this);
        }
    });

    return PopupQuickViewWidget;
});