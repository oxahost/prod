define(function(require) {
    'use strict';

    const BaseComponent = require('oroui/js/app/components/base/component');
    const $ = require('jquery');
    const __ = require('orotranslation/js/translator');

    const TestComponent = BaseComponent.extend({
        /**
         * @property
         */
        $element: null,

        /**
         * @inheritDoc
         */
        constructor: function TestComponent(options) {
            TestComponent.__super__.constructor.call(this, options);
        },



        /**
         * @inheritDoc
         */
        initialize: function(options) {
            this.$element = options._sourceElement;
            //alert('ok');
            TestComponent.__super__.initialize.call(this, options);
        }
    });

    return TestComponent;
});
