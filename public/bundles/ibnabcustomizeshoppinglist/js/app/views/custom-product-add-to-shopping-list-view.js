define(function (require) {
    'use strict';

    var CustomProductAddToShoppingListView;
    var BaseView = require('oroui/js/app/views/base/view');
    var ProductAddToShoppingListView = require('oroshoppinglist/js/app/views/product-add-to-shopping-list-view');
    var ElementsHelper = require('orofrontend/js/app/elements-helper');
    var ShoppingListCreateWidget = require('oro/shopping-list-create-widget');
    var ApiAccessor = require('oroui/js/tools/api-accessor');
    var routing = require('routing');
    var mediator = require('oroui/js/mediator');
    var $ = require('jquery');
    var _ = require('underscore');
    var ShoppingListCollectionService = require('oroshoppinglist/js/shoppinglist-collection-service');

    CustomProductAddToShoppingListView = ProductAddToShoppingListView.extend(_.extend({}, ElementsHelper, {
        /**
         * @inheritDoc
         */
        enableFlyCart: 1,
        enableUpdatePopup: 1,
        enableUpdateFly: 0,
        img: null,
        
        constructor: function CustomProductAddToShoppingListView() {
            CustomProductAddToShoppingListView.__super__.constructor.apply(this, arguments);
        },
        /**
         * @inheritDoc
         */
        initialize: function (options) {
            CustomProductAddToShoppingListView.__super__.initialize.apply(this, arguments);
            this.deferredInitializeCheck(options, ['productModel', 'dropdownWidget']);
            this.enableFlyCart = options.modelAttr.enableFlyCart;
            this.enableUpdatePopup = options.modelAttr.enableUpdatePopup;
            this.enableUpdateFly = options.modelAttr.enableUpdateFly;
        },
        deferredInitialize: function (options) {
            this.options = $.extend(true, {}, this.options, _.pick(options, _.keys(this.options)));

            this.dropdownWidget = options.dropdownWidget;
            this.$form = this.dropdownWidget.element.closest('form');

            this.initModel(options);

            if (this.options.buttonTemplate) {
                this.options.buttonTemplate = _.template(this.options.buttonTemplate);
            }
            if (this.options.removeButtonTemplate) {
                this.options.removeButtonTemplate = _.template(this.options.removeButtonTemplate);
            }
            if (this.options.createNewButtonTemplate) {
                this.options.createNewButtonTemplate = _.template(this.options.createNewButtonTemplate);
            }

            this.saveApiAccessor = new ApiAccessor(this.options.save_api_accessor);

            if (this.model) {
                this.model.on('change:unit', this._onUnitChanged, this);
                this.model.on('editLineItem', this._onEditLineItem, this);
            }

            this.$form.find(this.options.quantityField).on('keydown', _.bind(this._onQuantityEnter, this));

            ShoppingListCollectionService.shoppingListCollection.done(_.bind(function (collection) {
                this.shoppingListCollection = collection;
                this.listenTo(collection, 'change', this._onCollectionChange);
                this.render();
            }, this));
        },
        onClick: function (e) {
            var $button = $(e.currentTarget);
            if ($button.data('disabled')) {
                return false;
            }
            var url = $button.data('url');
            var intention = $button.data('intention');
            var formData = this.$form.serialize();

            var urlOptions = {
                shoppingListId: $button.data('shoppinglist').id
            };
            if (this.model) {
                urlOptions.productId = this.model.get('id');
                if (this.model.has('parentProduct')) {
                    urlOptions.parentProductId = this.model.get('parentProduct');
                }
            }
            if (!this.validate(intention, url, urlOptions, formData)) {
                return;
            }
            
            this.img = $button.closest('.product-item__box').find("img").eq(0);
            var offset = this.img.offset();
            //console.log($(button).closest('.product-item__box'));

            if (offset === undefined || offset === null) {
                this.img = $button.closest('.product-view').find("img").eq(0);
                offset = this.img.offset();
            }
            
            if (offset === undefined || offset === null) {
                this.img = $(this.$form).closest('.product-item__box').find("img").eq(0); 
                offset = this.img .offset();
            }
            if (offset === undefined || offset === null) {
                this.img = $(this.$form).closest('.product-view').find("img").eq(0); 
            }            
            if (intention === 'new') {
                this._createNewShoppingList(url, urlOptions, formData);
            } else if (intention === 'update') {               
                this._customSaveLineItem(url, urlOptions, formData);
            } else if (intention === 'remove') {
                this._removeLineItem(url, urlOptions, formData);
            } else {       
                this._addCustomLineItem(url, urlOptions, formData);

            }
        },
        _addCustomLineItem: function (url, urlOptions, formData) {
            this._addCustomProductToShoppingList(url, urlOptions, formData);
        },
        _addCustomProductToShoppingList: function (url, urlOptions, formData) {
            if (this.model && !this.model.get('line_item_form_enable')) {
                return;
            }
            var self = this;

            mediator.execute('showLoading');
            $.ajax({
                type: 'POST',
                url: routing.generate(url, urlOptions),
                data: formData,
                success: function (response) {
                    self._flyShoppingCart();
                    mediator.trigger('shopping-list:line-items:update-response', self.model, response);
                },
                complete: function () {
                    mediator.execute('hideLoading');
                }
            });
        },
        _flyShoppingCart: function () {
            if(this.enableFlyCart){
            var cart = $('.cart-widget__content');
            var imgtodrag = this.img;
            var offset = imgtodrag.offset();
            if (offset !== undefined && offset !== null) {
                
                var imgclone = imgtodrag.clone()
                        .offset({
                            top: imgtodrag.offset().top,
                            left: imgtodrag.offset().left
                        })
                        .css({
                            'opacity': '0.5',
                            'position': 'absolute',
                            'height': '150px',
                            'width': '150px',
                            'z-index': '100'
                        })
                        .appendTo($('body'))
                        .animate({
                            'top': cart.offset().top + 10,
                            'left': cart.offset().left + 10,
                            'width': 75,
                            'height': 75
                        }, 1000, 'easeInOutExpo');
                $("html, body").animate({
                    scrollTop: 0
                }, 1000, 'easeInOutExpo');
                setTimeout(function () {

                    cart.effect("shake", {
                        times: 2
                    }, 200);
                }, 1500);

                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    //$(button).detach()
                });
            }
          }

        },
        _customSaveLineItem: function (url, urlOptions, formData) {
            if (this.model && !this.model.get('line_item_form_enable')) {
                return;
            }
            mediator.execute('showLoading');
            var self = this;
            var shoppingList = this.findShoppingList(urlOptions.shoppingListId);
            var lineItem = this.findShoppingListLineItemByUnit(shoppingList, this.model.get('unit'));

            var savePromise = this.saveApiAccessor.send({
                id: lineItem.id
            }, {
                quantity: this.model.get('quantity'),
                unit: this.model.get('unit')
            });

            savePromise
                    .done(_.bind(function (response) {
                        lineItem.quantity = response.quantity;
                        lineItem.unit = response.unit;
                        this.shoppingListCollection.trigger('change', {
                            refresh: true
                        });
                        if(this.enableUpdateFly){
                            self._flyShoppingCart();
                        }
                        response.enableUpdatePopup = self.enableUpdatePopup;
                        response.fromUpdate = true;
                        mediator.execute('showFlashMessage', 'success', _.__(this.options.messages.success));
                        mediator.trigger('shopping-list:line-items:update-response', self.model, response);
                        mediator.execute('hideLoading');

                    }, this))
                    .always(function () {
                       // mediator.execute('hideLoading');
                    });
        },
    }));

    return CustomProductAddToShoppingListView;
});
