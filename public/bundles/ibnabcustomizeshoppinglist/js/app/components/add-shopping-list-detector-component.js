define(function (require) {
    'use strict';

    var AddShoppingListDetectorComponent;
    var BaseComponent = require('oroui/js/app/components/base/component');
    var mediator = require('oroui/js/mediator');
    var _ = require('underscore');
    var FrontendDialogWidget = require('orofrontend/js/app/components/frontend-dialog-widget');
    var LoadingBarView = require('oroui/js/app/views/loading-bar-view');
    var BaseView = require('oroui/js/app/views/base/view');
    AddShoppingListDetectorComponent = BaseView.extend({
        /**
         * @inheritDoc
         */
        listen: {
            'shopping-list:line-items:update-response mediator': '_onLineItemAdd'
        },
        generalRedirectUrl: "",
        content: "",
        options: {
            ajaxMode: true,
            ajaxMethod: 'GET'
        },
        /**
         * @inheritDoc
         */
        constructor: function AddShoppingListDetectorComponent() {
            AddShoppingListDetectorComponent.__super__.constructor.apply(this, arguments);
        },
        _onLineItemAdd: function (model, response) {
            
            if (!model || !response) {
                return;
            }
            var isFromAdd = response.hasOwnProperty('formAdd') && response.formAdd;
            if (response.message || isFromAdd) {
                var isSuccessful = response.hasOwnProperty('successful') && response.successful;
                var Popup = response.hasOwnProperty('popup') && response.formAdd;
                
                if (isSuccessful && isFromAdd) {
                    if (response.hasOwnProperty('redirectUrl')) {
                        var loading = "<div class='custom loader-mask'><div class='loader-overlay'></div><div class='loader-frame well'><div class='loader-content'>.</div></div></div>";
                        if (Popup) {
                            var forUpdate = response.hasOwnProperty('fromUpdate') && response.fromUpdate;
                            var enableUpdatePopup = response.hasOwnProperty('enableUpdatePopup') && response.enableUpdatePopup;
                            if(!forUpdate){
                            $("body").append(loading);
                            this.generalRedirectUrl = response.redirectUrl;
                            this.ajaxOpenDecorator();
                            }else{
                                if(enableUpdatePopup){
                                   $("body").append(loading);
                                   $(".custom.loader-mask").addClass('shown');
                                   this.generalRedirectUrl = response.redirectUrl;
                                   this.ajaxOpenDecorator();                                    
                                }
                            }

                        } else {
                            $("body").append(loading);
                            $(".custom.loader-mask").addClass('shown');
                            mediator.execute('redirectTo', {url: response.redirectUrl}, {redirect: true});
                            mediator.execute('hideLoading');
                        }

                    }
                }
            }
        },
        onOpen: function () {

            this.render();
        },
        ajaxOpenDecorator: function () {

            $.ajax({
                url: this.generalRedirectUrl + "?type=shopping-quick-view-popup",
                method: this.options.ajaxMethod,
                dataType: 'html',
                beforeSend: _.bind(function () {
                    $(".custom.loader-mask").addClass('shown');
                }, this),
                success: _.bind(function (data) {
                    var parsed = $.parseHTML(data.replace("orofrontend/js/app/components/delete-item-component", "ibnabcustomizeshoppinglist/js/app/components/delete-item-component"));
                    this.$content = $(parsed).find('.page-content').html();
                    this.onOpen();
                    mediator.execute('hideLoading');
                }, this),
                complete: _.bind(function () {
                    $(".custom.loader-mask").remove();
                    
                    //$('.custom.loader-mask').find(".loader-overlay").hide();
                }, this)
            });
        },
        render: function () {
            $('body').css("width", $(window).width() + "px");

            this.subview('popup', new FrontendDialogWidget({
                autoRender: true,
                title: '',
                simpleActionTemplate: false,
                renderActionsFromTemplate: true,
                staticPage: false,
                fullscreenMode: false,
                dialogOptions: {
                    modal: true,
                    resizable: false,
                    closeOnEscape: true,
                    autoResize: true,
                    draggable: false,
                    width: 1000,
                    dialogClass: 'cart-view-dialog-widget',
                    open: function () {
                        $('.ui-widget-overlay').bind('click', function () {
                            $.each($(".ui-dialog"), function ()
                            {
                                var $dialog;
                                $dialog = $(this).children(".ui-dialog-content");
                                if ($dialog.dialog("option", "modal"))
                                {
                                     $('.ui-dialog-titlebar-close:visible').click()
                                    //$(this).children(".ui-dialog-titlebar-close").trigger('click'); 
                                    //$(this).children(".ui-dialog-titlebar-close").click();
                                    //$dialog.dialog('close');
                                    //$(this).empty();
                                    //$('input').blur();
                                    //$(this).children(".ui-dialog-titlebar-close").trigger('click'); 
                                    //$(this).children(".ui-dialog-titlebar-close").click();
                                    //self.subview('popup').once('frontend-dialog:close', _.bind(this.changeCheckboxState, this, false));   
                                }
                            });
                        })
                    }
                }
            }).setContent("<div class='widget-content'>" + this.$content + "</div>"));


            $(window).on('resize', function () {
                $('.mobile-version').css("width", $(window).width() + "px");
            });
            $(".cart-view-dialog-widget").css("z-index", "11");
            $(".ui-widget-overlay.ui-front").css("z-index", "10");
            $(".sticky-panel").css("z-index", "9");
            //this.subview('popup')
            // .once('frontend-dialog:close', _.bind(this.changeCheckboxState, this, false));

            //this.$quickViewWidget.hide().show();
        }
    });

    return AddShoppingListDetectorComponent;
});
